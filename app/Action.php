<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'actions';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	public function type(){
		return $this->belongsTo('App\ActionType','action_type_id');
	}
}
