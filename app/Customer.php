<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customers';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	public $true = "<i class='fa fa-check text-success'></i>";
    public $false = "<i class='fa fa-times text-danger'></i>";

    public function getEnabled(){
        // reverse of above , to understand it on the front end
        return $this->is_activated ? $this->true : $this->false;
    }

    public function address(){
        return $this->hasMany('App\CustomerAddress', 'customer_id');
    }

    public function contact(){
        return $this->hasMany('App\CustomerContact', 'customer_id');
    }

}
