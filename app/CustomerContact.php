<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerContact extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customer_contact';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	public $true = "<i class='fa fa-check text-success'></i>";
    public $false = "<i class='fa fa-times text-danger'></i>";

    public function isPrimary(){
    	return $this->is_primary ? $this->true : $this->false;
    }

}
