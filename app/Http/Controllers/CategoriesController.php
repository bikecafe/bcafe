<?php namespace App\Http\Controllers;

use App\Category;
use App\ProductCategory;
use Illuminate\Auth\Guard;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Validator;
use DB;

class CategoriesController extends Controller {

	public function __construct(Guard $auth)
    {
        $this->middleware('auth');
        $this->auth = $auth;
    }

    public function index(){
    	$categories = Category::where('is_deleted',0)->get();
        $mode = "active";
    	return view('categories.index', compact('categories', 'mode'));
    }


    public function add(){
        $mode = 'add';
        $enabled = true;
        return view('categories.add_edit',compact('mode','enabled'));
    }

    public function postAdd(){
        $name = Input::get('name');
        $machineName = str_replace(' ', '_', strtolower(trim($name)));
        $rank = 0;
        $isHidden = Input::get('is_hidden');
        
        $category = new Category;
        $category->name = trim($name);
        $category->machine_name = $machineName;
        $category->rank = $rank;
        $category->is_shown = empty($isHidden) ? true : false;

        $input = Input::all();

        $rules = array(
            'name' => 'required'
        );

        $messages = array(
            'name.required' => 'This category name field is required',
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){
            $idx = 2;
            //if category name exists append index to machine name
            while(Category::where('machine_name',trim($machineName))->first()){
                $machineName = $machineName.$idx;
                $category->machine_name = $machineName;
                $idx++;
            }
            //save category
            $category->save();

            if($category->id){

                $categorylink = "<a href='/categories/$category->id/edit'>$category->name</a>";

                return redirect("/categories/")
                    ->with('success','Category '.$categorylink.' successfully added');
            }
        }

        return redirect("/categories/add")->withInput()->withErrors($validation);
    }


    public function edit($id){
        $category = Category::find($id);
        $enabled = $category->is_shown;
        $mode = 'edit';
        return view('categories.add_edit',compact('category', 'mode', 'enabled'));
    }



    public function postEdit($id){
        $name = Input::get('name');
        $machineName = str_replace(' ', '_', strtolower($name));
        $rank = 0;
        $isHidden = Input::get('is_hidden');
        
        $category = Category::find($id);
        $category->name = $name;
        $category->machine_name = $machineName;
        $category->rank = $rank;
        $category->is_shown = empty($isHidden) ? true : false;

        $rules = array(
            'name' => 'required|unique:categories,name,'.$id.''
        );

        $messages = array(
            'name.required' => 'This category name field is required',
            'name.unique' => 'This category name already exists.'
        );

        $input = Input::all();

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){
            //save category
            $category->save();

            if($category->id){

                $categorylink = "<a href='/categories/$id/edit'>$category->name</a>";

                return redirect("/categories/")
                    ->with('success',"Category $categorylink successfully updated");
            }
        }

        return redirect("/categories/$id/edit")->withInput()->withErrors($validation);
    }


    public function delete($id){
        $category = Category::find($id);
        $category->is_deleted = 1;
        $categoryName = $category->name;

        try {
            $category->save();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/categories/')->with("error","<b>$categoryName</b> cannot be deleted it is being used by the system");
        }

        return redirect('/categories/')->with("success","<b>$categoryName</b> has been deleted successfully");

    }

    public function deleted(){
        $categories = Category::where('is_deleted',1)->get();
        $mode = "deleted";
        return view('categories.index', compact('categories', 'mode'));
    }

    public function restore($id){
        $category = Category::find($id);
        $category->is_deleted = 0;
        $categoryName = $category->name;

        try {
            $category->save();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/categories/deleted')->with("error","<b>$categoryName</b> cannot be restored due to a conflict");
        }

        return redirect('/categories/')->with("success","<b>$categoryName</b> has been restored successfully");

    }

    public function rank(){
        $categories = Category::where('is_deleted',0)->where('is_shown',1)->orderBy('rank')->get();
        return view('categories.rank', compact('categories'));
    }

    public function postRank(){
        $ranking = Input::get('ranking');
        $categories = explode(',', $ranking);

        for($cid = 0; $cid < count($categories); $cid++){
            //update rank for all categories
            $category = Category::find($categories[$cid]);
            $category->rank = $cid;
            $category->save();
        }
        return redirect("/categories/rank")
                    ->with('success',"Category ranking successfully updated");
    }

    public function products($id){
        $category = Category::find($id);
        $category_products = DB::table('product_categories')
                                ->join('products', 'product_categories.product_id', '=', 'products.id')
                                ->where('product_categories.category_id', '=', $id)
                                ->where('products.is_deleted', '=', 0)
                                ->select('products.name','product_categories.id')
                                ->orderBy('product_categories.rank')
                                ->get();
        return view('categories.products', compact('category','category_products'));
    }

    public function postProducts($id){
        $ranking = Input::get('ranking');
        $product_categories = explode(',', $ranking);

        for($pid = 0; $pid < count($product_categories); $pid++){
            //update rank for all products under category
            $product_category = ProductCategory::find($product_categories[$pid]);
            $product_category->rank = $pid;
            $product_category->save();
        }
        return redirect("/categories/$id/products")
                    ->with('success',"Product ranking successfully updated");
    }

}
