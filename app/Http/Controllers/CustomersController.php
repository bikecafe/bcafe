<?php namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Database\QueryException;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Validator;

class CustomersController extends Controller {

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function index(){
        $customers = Customer::where('id','<>',1)->get();
        return view('customers.index',compact('customers'));
    }

    public function view($id){
        $customers = Customer::where('id','<>',1)->get();
        $customer = Customer::find($id);
        if(count($customer)>0){
            return view ('customers.details',compact('customer'));
        }
        return redirect('/customers')->with("error","Customer account does not exist");
    }

    
}