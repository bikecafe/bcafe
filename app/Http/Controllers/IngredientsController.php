<?php namespace App\Http\Controllers;

use App\Ingredient;
use Illuminate\Auth\Guard;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\Facades\Form;
use DB;
use App\Action;
use Auth;
use Validator;

class IngredientsController extends Controller {

	public function __construct(Guard $auth)
    {
        $this->middleware('auth');
        $this->auth = $auth;
    }

    public function index(){
    	$ingredients = Ingredient::where('is_deleted',0)->get();
        $mode = "active";
    	return view('inventory.ingredients.index', compact('ingredients', 'mode'));
    }

    public function add(){
        $mode = 'add';
        return view('inventory.ingredients.add_edit',compact('mode'));
    }

    public function postAdd(){
        $name = Input::get('name');
        $supplier = Input::get('supplier');
        $brand = Input::get('brand');
        $quantity = Input::get('quantity');
        $unit = Input::get('unit');
        $min_normal = Input::get('min_normal');
        $max_normal = Input::get('max_normal');

        $ingredient = new Ingredient;
        $ingredient->name = $name;
        $ingredient->supplier = $supplier;
        $ingredient->brand = $brand;
        $ingredient->quantity = $quantity;
        $ingredient->unit = $unit;
        $ingredient->min_normal = $min_normal;
        $ingredient->max_normal = $max_normal;

        $input = Input::all();

        $rules = array(
            'name' => 'required',
            'supplier' => 'required',
            'quantity' => 'required|numeric'
        );

        $messages = array(
            'name.required' => 'The name field is required',
            'supplier.required' => 'The supplier field is required',
            'quantity.required' => 'The quantity field is required',
            'quantity.numeric' => 'The quantity field must be numeric'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){

            //save raw material to DB
            $ingredient->save();

            if($ingredient->id){

                //log initial stock
                $stock = new Action;
                $stock->action_type_id = 1; //update ingredient stock
                $stock->user_id = Auth::user()->id; //who updated the stock
                $stock->reference_id = $ingredient->id;
                $stock->old_value = $ingredient->quantity;
                $stock->new_value = $ingredient->quantity;
                $stock->description = "initial stock";
                $stock->save();

                $ingredientlink = "<a href='/inventory/ingredients/$ingredient->id/edit'>$ingredient->name</a>";

                return redirect("/inventory/ingredients")
                    ->with('success','Ingredient '.$ingredientlink.' successfully added');
            }
        }

        return redirect("/inventory/ingredients/add")->withInput()->withErrors($validation);
    }

    public function edit($id){
        $ingredient = Ingredient::find($id);
        $history = DB::table('actions')->join('users', 'users.id', '=', 'actions.user_id')->select('users.id', 'users.firstname', 'users.lastname', 'actions.created_at', 'actions.updated_at','actions.old_value','actions.new_value')->where('action_type_id',1)->where('reference_id',$id)->orderby('actions.updated_at','DESC')->get();
        $mode = 'edit';
        return view('inventory.ingredients.add_edit',compact('ingredient', 'mode','history'));
    }

    public function postEdit($id){
        $name = Input::get('name');
        $supplier = Input::get('supplier');
        $brand = Input::get('brand');
        $quantity = Input::get('quantity');
        $unit = Input::get('unit');
        $min_normal = Input::get('min_normal');
        $max_normal = Input::get('max_normal');

        $ingredient = Ingredient::find($id);
        $oldStockValue = $ingredient->quantity;
        $ingredient->name = $name;
        $ingredient->supplier = $supplier;
        $ingredient->brand = $brand;
        $ingredient->quantity = $quantity;
        $ingredient->unit = $unit;
        $ingredient->min_normal = $min_normal;
        $ingredient->max_normal = $max_normal;

        $input = Input::all();

        $rules = array(
            'name' => 'required',
            'supplier' => 'required',
            'quantity' => 'required|numeric'
        );

        $messages = array(
            'name.required' => 'Thie name field is required',
            'supplier.required' => 'The supplier field is required',
            'quantity.required' => 'The quantity field is required',
            'quantity.numeric' => 'The quantity field must be numeric'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){

            //save ingredient
            $ingredient->save();

            if($ingredient->id){
                if($oldStockValue != $ingredient->quantity){
                    //log updated stock
                    $stock = new Action;
                    $stock->action_type_id = 1; //update raw material stock
                    $stock->user_id = Auth::user()->id; //who updated the stock
                    $stock->reference_id = $ingredient->id;
                    $stock->old_value = $oldStockValue;
                    $stock->new_value = $ingredient->quantity;
                    $stock->description = "updated stock";
                    $stock->save();
                }

                $ingredientlink = "<a href='/inventory/ingredients/$id/edit'>$ingredient->name</a>";

                return redirect("/inventory/ingredients/")
                    ->with('success',"Ingredient $ingredientlink successfully updated");
            }
        }

        return redirect("/inventory/ingredients/$id/edit")->withInput()->withErrors($validation);
    }

    public function delete($id){
        $ingredient = Ingredient::find($id);
        $ingredient->is_deleted = 1;
        $ingredientName = $ingredient->name;

        try {
            $ingredient->save();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/inventory/ingredients/')->with("error","<b>$ingredientName</b> cannot be deleted it is being used by the system");
        }

        return redirect('/inventory/ingredients/')->with("success","<b>$ingredientName</b> has been deleted successfully");

    }

    public function deleted(){
        $ingredients = Ingredient::where('is_deleted',1)->get();
        $mode = "deleted";
        return view('inventory.ingredients.index', compact('ingredients', 'mode'));
    }

    public function restore($id){
        $ingredient = Ingredient::find($id);
        $ingredient->is_deleted = 0;
        $ingredientName = $ingredient->name;

        try {
            $ingredient->save();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/inventory/ingredients/deleted')->with("error","<b>$ingredientName</b> cannot be restored due to a conflict");
        }

        return redirect('/inventory/ingredients/')->with("success","<b>$ingredientName</b> has been deleted successfully");

    }
}
