<?php namespace App\Http\Controllers;

class LoginController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	public function __construct()
	{
		$this->middleware('guest');
	}

	public function index()
	{
		return view('auth.login');
	}

}
