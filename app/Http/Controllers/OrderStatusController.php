<?php namespace App\Http\Controllers;

use App\OrderStatus;
use Illuminate\Database\QueryException;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Validator;

class OrderStatusController extends Controller {

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function index(){
		$mode = 'add';
        $orderStatuses = OrderStatus::all();
        return view('orders.status.index',compact('orderStatuses', 'mode'));
    }

    public function add(){
        $mode = 'add';
        $orderStatuses = OrderStatus::all;
        return view('orders.status.index',compact('orderStatuses','mode'));
    }

    public function postAdd(){
        $name = Input::get('name');

        $orderStatus = new OrderStatus;
        $orderStatus->name = $name;

        $input = Input::all();

        $rules = array(
            'name' => 'required',
        );

        $messages = array(
            'name.required' => 'This name field is required'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){


            $orderStatus->save();

            if($orderStatus->id){
                return redirect("/orders/status/")
                    ->with('success',"Order Status $orderStatus->name successfully added");
            }
        }

        return redirect("/orders/status")->withInput()->withErrors($validation);

    }

    public function edit($id){
        $orderStatus = OrderStatus::find($id);
		$orderStatuses = OrderStatus::all();
        $mode = 'edit';
        return view('orders.status.index',compact('orderStatus', 'orderStatuses', 'mode'));
    }

    public function postEdit($id){

        $name = Input::get('name');

        $orderStatus = OrderStatus::find($id);
        $orderStatus->name = $name;

        $input = Input::all();

        $rules = array(
            'name' => 'required',
        );

        $messages = array(
            'name.required' => 'This name field is required'
        );


        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){


            $orderStatus->save();

            if($orderStatus->id){
                return redirect("/orders/status")
                    ->with('success',"Order Status $orderStatus->name successfully updated");
            }
        }

        return redirect("/orders/status")->withInput()->withErrors($validation);
    }

    public function delete($id){
        $orderStatus = OrderStatus::find($id);
        $name = $orderStatus->name;

        try {
            $orderStatus->delete();
        } catch (QueryException $e) {
            return redirect('/orders/status')->with("error","<b>$name</b> cannot be deleted it is being used by the system");
        }

        return redirect('/orders/status')->with("success","<b>$name</b> has been deleted successfully");

    }
}