<?php namespace App\Http\Controllers;

use App\Order;
use App\OrderStatus;
use App\OrderLine;
use App\OrderWorkflow;
use App\OrderProductIngredient;
use App\OrderProductRawMaterial;
use App\Product;
use App\Ingredient;
use App\IngredientLock;
use App\IngredientLockTemp;
use App\RawMaterial;
use App\RawMaterialLock;
use App\RawMaterialLockTemp;
use Illuminate\Database\QueryException;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;

class OrdersController extends Controller {

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function index(){
        $onsite_processing_orders = Order::where(['order_type_id' => 1, 'order_status_id' => 2])->get();
        $online_active_orders = Order::where(function($query){
            $query->where(['order_status_id'=>1]);
            $query->orWhere(['order_status_id'=>2]);
        })
        ->where(['order_type_id' => 2])->get();
        return view('orders.index',compact('onsite_processing_orders','online_active_orders'));
    }

    public function add(){
        $products = Product::where('is_deleted',0)->get();
        $orderStatuses = OrderStatus::where(['name'=>'processing'])->orWhere(['name'=>'delivered'])->get();
        return view('orders.add',compact('orderStatuses', 'products'));
    }

    public function postAdd(){
        $orderItems = Input::get('order-items');
        $changeFor = Input::get('amount-tendered');
        $grandTotal = Input::get('grand-total');
        $orderStatus = Input::get('order-status');
        $referenceNo = Input::get('reference_no');

        $products = json_decode($orderItems, true);
        $orderlines = array();
        foreach($products as $product){
            $orderline = new OrderLine;
            $orderline->product_id = $product['product_id'];
            $orderline->price = $product['price'];
            $orderline->quantity = $product['quantity'];
            $orderlines[] = $orderline;
        }

        $order = new Order;
        $order->reference_no = $referenceNo;
        $order->order_status_id = $orderStatus;
        $order->customer_id = 1;
        $order->total_price = $grandTotal;
        $order->change_for = $changeFor;
        $order->customer_address_id = 1;
        $order->customer_contact_id = 1;
        $order->order_type_id = 1; //offline/onsite
        $order->order_acquisition_id = 1; //pickup

        $input = Input::all();

        $rules = array(
            'order-items' => 'required',
        );

        $messages = array(
            'order-items.required' => 'No order items found'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){

            $order->save();

            if($order->id){
                //save order lines
                $order->orderlines()->saveMany($orderlines);

                //log order workflow (status + logged in user who tendered the order)
                if(Auth::check()){
                    $workflow = new OrderWorkflow;
                    $workflow->user_id = Auth::user()->id;
                    $workflow->notes = "Onsite order tendered";
                    $workflow->order_status_id = $orderStatus;
                    $order->workflow()->save($workflow);
                }

                //remove for_release since reference_no is now associated to an order
                IngredientLock::where(['reference_no'=>$referenceNo,'for_release'=>1])->update(['for_release'=>0]);
                RawMaterialLock::where(['reference_no'=>$referenceNo,'for_release'=>1])->update(['for_release'=>0]);
                
                //if order status was changed to 'delivered'
                if($orderStatus == 3){
                    //store ingredient deduction
                    $locked_data = IngredientLock::where(['reference_no'=>$referenceNo])->get();
                    if(count($locked_data)>0){
                        foreach($locked_data as $ingredient){
                            $order_ingredient = new OrderProductIngredient;
                            $order_ingredient->order_id = $order->id;
                            $order_ingredient->reference_no = $referenceNo;
                            $order_ingredient->product_id = $ingredient->product_id;
                            $order_ingredient->ingredient_id = $ingredient->ingredient_id;
                            $order_ingredient->quantity = $ingredient->quantity;
                            $order_ingredient->save();
                            IngredientLock::where(['id'=>$ingredient->id])->delete();
                        }
                    }
                }

                return redirect("/orders/")
                    ->with('success',"Order $order->reference_no successfully added");
            }
        }

        return redirect("/orders/add")->withInput()->withErrors($validation);

    }

    public function manage($id){
        $order = Order::find($id);
        //if order id exists
        if(!empty($order)){
            $workflow = $order->getLastWorkflow();
            $ordertype = $order->type->name == 'offline' ? 'onsite' : 'online';
            if(!empty($workflow)){
                //if latest order workflow status is 'processing'
                if($workflow->status->name == "processing"){
                    $products = Product::where('is_deleted',0)->get();
                    $orderStatuses = OrderStatus::where(['name'=>'processing'])->orWhere(['name'=>'delivered'])->orWhere(['name'=>'cancelled'])->get();
                    return view('orders.processing', compact('order', 'ordertype', 'orderStatuses', 'products'));
                } 
                //if latest order workflow status is 'delivered' or 'cancelled'
                elseif($workflow->status->name == "delivered" || $workflow->status->name == "cancelled"){
                    return view('orders.closed', compact('order','ordertype'));
                }
                //if latest order workflow status is 'pending' and order type is online
                elseif($workflow->status->name == "pending" && $ordertype == "online"){
                    $reference_no = $order->reference_no;
                    $for_release = 1;
                    $insufficient_stock = [];
                    //check stock status for current order items and lock ingredients/raw materials for available ones
                    foreach($order->orderlines as $orderline){
                        $quantity = $orderline->quantity;
                        $product = Product::find($orderline->product_id);
                        if(intval($product->stocks()) >= intval($orderline->quantity)){
                            $ingredients = $product->ingredients;
                            if(count($ingredients)>0){
                                foreach($ingredients as $ingredient){
                                    $ingredientLock = new IngredientLock;
                                    $ingredientLock->reference_no = $reference_no;
                                    $ingredientLock->product_id = $product->id;
                                    $ingredientLock->ingredient_id = $ingredient->id;
                                    $ingredientLock->quantity = floatval($ingredient->pivot->quantity * $quantity);
                                    $ingredientLock->for_release = !empty($for_release) ? $for_release : 0;
                                    $ingredientLock->save();

                                    $ingredentObj = Ingredient::find($ingredient->id);
                                    $ingredentObj->quantity = floatval($ingredentObj->quantity - ($ingredient->pivot->quantity * $quantity));
                                    $ingredentObj->save();
                                }
                            }

                            $rawmaterials = $product->rawmaterials;
                            if(count($rawmaterials) > 0){
                                foreach($rawmaterials as $ingredient){
                                    $rawmaterialLock = new RawMaterialLock;
                                    $rawmaterialLock->reference_no = $reference_no;
                                    $rawmaterialLock->product_id = $product->id;
                                    $rawmaterialLock->raw_material_id = $ingredient->id;
                                    $rawmaterialLock->quantity = floatval($ingredient->pivot->quantity * $quantity);
                                    $rawmaterialLock->for_release = !empty($for_release) ? $for_release : 0;
                                    $rawmaterialLock->save();

                                    $rawObj = RawMaterial::find($ingredient->id);
                                    $rawObj->quantity = floatval($rawObj->quantity - ($ingredient->pivot->quantity * $quantity));
                                    $rawObj->save();
                                }
                            }
                        } else {
                            $insufficient_stock[$product->id] = $product->name;
                        }
                    }
                    $products = Product::where('is_deleted',0)->get();
                    $orderStatuses = OrderStatus::where(['name'=>'processing'])->orWhere(['name'=>'delivered'])->orWhere(['name'=>'cancelled'])->get();
                    return view('orders.pending', compact('order', 'ordertype', 'orderStatuses', 'products', 'insufficient_stock'));
                }
            }
            //if order has no existing order workflow
            return redirect('/orders')->with("error","Order update is not permitted.");
        } 
        //if order id does not exist
        else {
            return redirect('/orders')->with("error","Order does not exist");
        }
    }

    public function postManage($id){
        $order = Order::find($id);
        if(!empty($order)){
            $orderStatus = Input::get('order-status');
            //if order status was changed to 'cancelled'
            if($orderStatus == 4){
                $notes = Input::get('notes');
                $notes = !empty($notes) ? $notes : "returned";
                $remarks = Input::get('remarks');
                $remarks = !empty($remarks) ? $remarks : "none";
                $msgs = array('stocks'=>$notes,'remarks'=>$remarks);
                $reference_no = $order->reference_no;

                //log order workflow (status + logged in user who updated the order)
                if(Auth::check()){
                    $workflow = new OrderWorkflow;
                    $workflow->user_id = Auth::user()->id;
                    $workflow->notes = json_encode($msgs);
                    $workflow->order_status_id = $orderStatus;
                    $order->workflow()->save($workflow);
                }
                //update status
                $order->order_status_id = $orderStatus;
                $order->save();

                if($notes == "wasted"){
                    //store ingredient deduction
                    $locked_data = IngredientLock::where(['reference_no'=>$reference_no])->get();
                    if(count($locked_data)>0){
                        foreach($locked_data as $ingredient){
                            $order_ingredient = new OrderProductIngredient;
                            $order_ingredient->order_id = $id;
                            $order_ingredient->reference_no = $reference_no;
                            $order_ingredient->product_id = $ingredient->product_id;
                            $order_ingredient->ingredient_id = $ingredient->ingredient_id;
                            $order_ingredient->quantity = $ingredient->quantity;
                            $order_ingredient->save();
                            IngredientLock::where(['id'=>$ingredient->id])->delete();
                        }
                    }

                    //store raw_material deduction
                    $locked_data = RawMaterialLock::where(['reference_no'=>$reference_no])->get();
                    if(count($locked_data)>0){
                        foreach($locked_data as $ingredient){
                            $order_ingredient = new OrderProductRawMaterial;
                            $order_ingredient->order_id = $id;
                            $order_ingredient->reference_no = $reference_no;
                            $order_ingredient->product_id = $ingredient->product_id;
                            $order_ingredient->raw_material_id = $ingredient->raw_material_id;
                            $order_ingredient->quantity = $ingredient->quantity;
                            $order_ingredient->save();
                            IngredientLock::where(['id'=>$ingredient->id])->delete();
                        }
                    }
                } elseif($notes == "returned" || $notes == "insufficient"){
                    foreach($order->orderlines as $orderline){
                        $product_id = $orderline->product_id;
                        $quantity = $orderline->quantity;
                        //get product details
                        $product = Product::find($product_id);
                        //check if product exists
                        if(count($product)>0){
                            $ingredients = $product->ingredients;
                            if(count($ingredients)>0){
                                foreach($ingredients as $ingredient){
                                    //get total ingredient quantity to unlock
                                    $total_ingredient = floatval($ingredient->pivot->quantity * $quantity);
                                    
                                    //decrease total ingredient from locked ingredients
                                    while($total_ingredient > 0.00){
                                        $ingredientUnlock = IngredientLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'ingredient_id'=>$ingredient->id])->first();
                                        if(count($ingredientUnlock) > 0){
                                            
                                            $new_quantity = floatval($ingredientUnlock->quantity - $ingredient->pivot->quantity);
                                            //if quantity is sufficient to be deducted from
                                            if(floatval($new_quantity) >= 0.00){
                                                //decrease quantity from locked ingredient
                                                $ingredientUnlock->quantity = $new_quantity;
                                                $ingredientUnlock->save();

                                                //return quantity to inventory
                                                $ingredient_inventory = Ingredient::find($ingredient->id);
                                                $ingredient_inventory->quantity = floatval($ingredient_inventory->quantity + $ingredient->pivot->quantity);
                                                $ingredient_inventory->save();

                                                //save deducted value
                                                $total_ingredient = floatval($total_ingredient - $ingredient->pivot->quantity);
                                            }
                                        } else {
                                            $total_ingredient = 0.00;
                                        }
                                        unset($ingredientUnlock);
                                    }
                                    //delete lines with 0 quantity
                                    IngredientLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'ingredient_id'=>$ingredient->id, 'quantity'=>'0.00'])->delete();
                                }
                            }

                            $rawmaterials = $product->rawmaterials;
                            if(count($rawmaterials)>0){
                                foreach($rawmaterials as $ingredient){
                                    //get total ingredient quantity to unlock
                                    $total_ingredient = floatval($ingredient->pivot->quantity * $quantity);
                                    
                                    //decrease total ingredient from locked ingredients
                                    while($total_ingredient > 0.00){
                                        $ingredientUnlock = RawMaterialLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'raw_material_id'=>$ingredient->id])->first();
                                        if(count($ingredientUnlock) > 0){
                                            
                                            $new_quantity = floatval($ingredientUnlock->quantity - $ingredient->pivot->quantity);
                                            //if quantity is sufficient to be deducted from
                                            if(floatval($new_quantity) >= 0.00){
                                                //decrease quantity from locked ingredient
                                                $ingredientUnlock->quantity = $new_quantity;
                                                $ingredientUnlock->save();

                                                //return quantity to inventory
                                                $ingredient_inventory = RawMaterial::find($ingredient->id);
                                                $ingredient_inventory->quantity = floatval($ingredient_inventory->quantity + $ingredient->pivot->quantity);
                                                $ingredient_inventory->save();

                                                //save deducted value
                                                $total_ingredient = floatval($total_ingredient - $ingredient->pivot->quantity);
                                            }
                                        } else {
                                            $total_ingredient = 0.00;
                                        }
                                        unset($ingredientUnlock);
                                    }
                                    //delete lines with 0 quantity
                                    RawMaterialLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'raw_material_id'=>$ingredient->id, 'quantity'=>'0.00'])->delete();
                                }
                            }
                        }
                    }
                }

            } else {
                //save order details
                $orderItems = Input::get('order-items');
                $changeFor = Input::get('amount-tendered');
                $grandTotal = Input::get('grand-total');
                $orderStatus = Input::get('order-status');
                $reference_no = Input::get('reference_no');

                $products = json_decode($orderItems, true);
                $orderlines = array();
                foreach($products as $product){
                    $orderline = new OrderLine;
                    $orderline->product_id = $product['product_id'];
                    $orderline->price = $product['price'];
                    $orderline->quantity = $product['quantity'];
                    $orderlines[] = $orderline;
                }

                
                $order->order_status_id = $orderStatus;
                $order->total_price = $grandTotal;
                $order->change_for = $changeFor;
                $order->save();

                //delete current order lines
                $order->orderlines()->delete();

                //save new orderlines
                $order->orderlines()->saveMany($orderlines);

                //log order workflow (status + logged in user who updated the order)
                if(Auth::check()){
                    $workflow = new OrderWorkflow;
                    $workflow->user_id = Auth::user()->id;
                    $workflow->order_status_id = $orderStatus;
                    $order->workflow()->save($workflow);
                }
                if($order->type->name == "online"){
                    if($orderStatus == 2){ //processing
                        //remove for_release since online order has been confirmed and currently for processing
                        IngredientLock::where(['reference_no'=>$reference_no,'for_release'=>1])->update(['for_release'=>0]);
                        RawMaterialLock::where(['reference_no'=>$reference_no,'for_release'=>1])->update(['for_release'=>0]);
                    }
                } 
                //if order status was changed to 'delivered'
                if($orderStatus == 3){
                    //store ingredient deduction
                    $locked_data = IngredientLock::where(['reference_no'=>$reference_no])->get();
                    if(count($locked_data)>0){
                        foreach($locked_data as $ingredient){
                            $order_ingredient = new OrderProductIngredient;
                            $order_ingredient->order_id = $id;
                            $order_ingredient->reference_no = $reference_no;
                            $order_ingredient->product_id = $ingredient->product_id;
                            $order_ingredient->ingredient_id = $ingredient->ingredient_id;
                            $order_ingredient->quantity = $ingredient->quantity;
                            $order_ingredient->save();
                            IngredientLock::where(['id'=>$ingredient->id])->delete();
                        }
                    }

                    //store raw_material deduction
                    $locked_data = RawMaterialLock::where(['reference_no'=>$reference_no])->get();
                    if(count($locked_data)>0){
                        foreach($locked_data as $ingredient){
                            $order_ingredient = new OrderProductRawMaterial;
                            $order_ingredient->order_id = $id;
                            $order_ingredient->reference_no = $reference_no;
                            $order_ingredient->product_id = $ingredient->product_id;
                            $order_ingredient->raw_material_id = $ingredient->raw_material_id;
                            $order_ingredient->quantity = $ingredient->quantity;
                            $order_ingredient->save();
                            IngredientLock::where(['id'=>$ingredient->id])->delete();
                        }
                    }
                }
            }
            return redirect('/orders')->with("success","Order Reference: <b>$order->reference_no</b> has been updated successfully");
        }
        
    }

    public function onsite(){
        $orders = Order::where(['order_type_id' => 1])->get();
        $order_type = "Onsite";
        return view('orders.list',compact('orders','order_type'));
    }

    public function online(){
        $orders = Order::where(['order_type_id' => 2])->get();
        $order_type = "Online";
        return view('orders.list',compact('orders','order_type'));
    }

    public function ingredientLock(){
        $product_id = Input::get('product_id');
        $quantity = Input::get('product_qty');
        $reference_no = Input::get('reference_no');
        $for_release = Input::get('for_release');

        //get product details
        $product = Product::find($product_id);
        //check if product exists
        if(count($product)>0){
            $ingredients = $product->ingredients;
            if(count($ingredients)>0){
                foreach($ingredients as $ingredient){
                    $ingredientLock = new IngredientLock;
                    $ingredientLock->reference_no = $reference_no;
                    $ingredientLock->product_id = $product->id;
                    $ingredientLock->ingredient_id = $ingredient->id;
                    $ingredientLock->quantity = floatval($ingredient->pivot->quantity * $quantity);
                    $ingredientLock->for_release = !empty($for_release) ? $for_release : 0;
                    $ingredientLock->save();

                    $ingredentObj = Ingredient::find($ingredient->id);
                    $ingredentObj->quantity = floatval($ingredentObj->quantity - ($ingredient->pivot->quantity * $quantity));
                    $ingredentObj->save();
                }
            }

            $rawmaterials = $product->rawmaterials;
            if(count($rawmaterials) > 0){
                foreach($rawmaterials as $ingredient){
                    $rawmaterialLock = new RawMaterialLock;
                    $rawmaterialLock->reference_no = $reference_no;
                    $rawmaterialLock->product_id = $product->id;
                    $rawmaterialLock->raw_material_id = $ingredient->id;
                    $rawmaterialLock->quantity = floatval($ingredient->pivot->quantity * $quantity);
                    $rawmaterialLock->for_release = !empty($for_release) ? $for_release : 0;
                    $rawmaterialLock->save();

                    $rawObj = RawMaterial::find($ingredient->id);
                    $rawObj->quantity = floatval($rawObj->quantity - ($ingredient->pivot->quantity * $quantity));
                    $rawObj->save();
                }
            }
        } else {
            echo "no product";
        }
    }

    public function ingredientUnlock(){
        $product_id = Input::get('product_id');
        $quantity = Input::get('product_qty');
        $reference_no = Input::get('reference_no');

        //get product details
        $product = Product::find($product_id);
        //check if product exists
        if(count($product)>0){
            $ingredients = $product->ingredients;
            if(count($ingredients)>0){
                foreach($ingredients as $ingredient){
                    //get total ingredient quantity to unlock
                    $total_ingredient = floatval($ingredient->pivot->quantity * $quantity);
                    
                    //decrease total ingredient from locked ingredients
                    while($total_ingredient > 0.00){
                        $ingredientUnlock = IngredientLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'ingredient_id'=>$ingredient->id])->first();
                        if(count($ingredientUnlock) > 0){
                            
                            $new_quantity = floatval($ingredientUnlock->quantity - $ingredient->pivot->quantity);
                            //if quantity is sufficient to be deducted from
                            if(floatval($new_quantity) >= 0.00){
                                //decrease quantity from locked ingredient
                                $ingredientUnlock->quantity = $new_quantity;
                                $ingredientUnlock->save();

                                //return quantity to inventory
                                $ingredient_inventory = Ingredient::find($ingredient->id);
                                $ingredient_inventory->quantity = floatval($ingredient_inventory->quantity + $ingredient->pivot->quantity);
                                $ingredient_inventory->save();

                                //save deducted value
                                $total_ingredient = floatval($total_ingredient - $ingredient->pivot->quantity);
                            }
                        } else {
                            $total_ingredient = 0.00;
                        }
                        unset($ingredientUnlock);
                    }
                    //delete lines with 0 quantity
                    IngredientLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'ingredient_id'=>$ingredient->id, 'quantity'=>'0.00'])->delete();
                }
            }

            $rawmaterials = $product->rawmaterials;
            if(count($rawmaterials)>0){
                foreach($rawmaterials as $ingredient){
                    //get total ingredient quantity to unlock
                    $total_ingredient = floatval($ingredient->pivot->quantity * $quantity);
                    
                    //decrease total ingredient from locked ingredients
                    while($total_ingredient > 0.00){
                        $ingredientUnlock = RawMaterialLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'raw_material_id'=>$ingredient->id])->first();
                        if(count($ingredientUnlock) > 0){
                            
                            $new_quantity = floatval($ingredientUnlock->quantity - $ingredient->pivot->quantity);
                            //if quantity is sufficient to be deducted from
                            if(floatval($new_quantity) >= 0.00){
                                //decrease quantity from locked ingredient
                                $ingredientUnlock->quantity = $new_quantity;
                                $ingredientUnlock->save();

                                //return quantity to inventory
                                $ingredient_inventory = RawMaterial::find($ingredient->id);
                                $ingredient_inventory->quantity = floatval($ingredient_inventory->quantity + $ingredient->pivot->quantity);
                                $ingredient_inventory->save();

                                //save deducted value
                                $total_ingredient = floatval($total_ingredient - $ingredient->pivot->quantity);
                            }
                        } else {
                            $total_ingredient = 0.00;
                        }
                        unset($ingredientUnlock);
                    }
                    //delete lines with 0 quantity
                    RawMaterialLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'raw_material_id'=>$ingredient->id, 'quantity'=>'0.00'])->delete();
                }
            }
        } else {
            echo "no product";
        }
    }

    public function ingredientUnlockTemp(){
        $product_id = Input::get('product_id');
        $quantity = Input::get('product_qty');
        $reference_no = Input::get('reference_no');

        //get product details
        $product = Product::find($product_id);
        //check if product exists
        if(count($product)>0){
            $ingredients = $product->ingredients;
            if(count($ingredients)>0){
                foreach($ingredients as $ingredient){
                    $row_quantity = floatval($ingredient->pivot->quantity * $quantity);
                    $row = IngredientLock::where(['reference_no'=>$reference_no,'product_id'=>$product_id,'ingredient_id'=>$ingredient->id, 'quantity'=>$row_quantity])->first();
                    if(count($row)>0){
                        //transfer record from ingredients_locked to ingredients_locked_temp table
                        $ingredientLock = new IngredientLockTemp;
                        $ingredientLock->reference_no = $row->reference_no;
                        $ingredientLock->product_id = $product_id;
                        $ingredientLock->ingredient_id = $ingredient->id;
                        $ingredientLock->quantity = $row_quantity;
                        $ingredientLock->created_at = $row->created_at;
                        $ingredientLock->save();
                        $row->delete();
                    }
                    
                }
            }

            $rawmaterials = $product->rawmaterials;
            if(count($ingredients)>0){
                foreach($ingredients as $ingredient){
                    $row_quantity = floatval($ingredient->pivot->quantity * $quantity);
                    $row = RawMaterialLock::where(['reference_no'=>$reference_no,'product_id'=>$product_id,'raw_material_id'=>$ingredient->id, 'quantity'=>$row_quantity])->first();
                    if(count($row)>0){
                        //transfer record from ingredients_locked to ingredients_locked_temp table
                        $ingredientLock = new RawMaterialLockTemp;
                        $ingredientLock->reference_no = $row->reference_no;
                        $ingredientLock->product_id = $product_id;
                        $ingredientLock->raw_material_id = $ingredient->id;
                        $ingredientLock->quantity = $row_quantity;
                        $ingredientLock->created_at = $row->created_at;
                        $ingredientLock->save();
                        $row->delete();
                    }
                    
                }
            }
        } else {
            echo "no product";
        }
    }

    public function ingredientRelease(){
        $reference_no = Input::get('reference_no');
        $rows = IngredientLock::where(['reference_no'=>$reference_no, 'for_release'=>1])->get();
        
        if(count($rows)>0){
            foreach($rows as $row){
                $product_id = $row->product_id;
                $quantity = $row->quantity;
                $ingredient_id = $row->ingredient_id;

                //get product details
                $product = Product::find($product_id);
                //check if product exists
                if(count($product)>0){

                    //return quantity to inventory
                    $ingredient_inventory = Ingredient::find($ingredient_id);
                    $ingredient_inventory->quantity = floatval($ingredient_inventory->quantity + $quantity);
                    $ingredient_inventory->save();

                    //delete lines with 0 quantity
                    IngredientLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'ingredient_id'=>$ingredient_id, 'quantity'=>'0.00'])->delete();
                    $row->delete();
                } 
            }
        } 

        $raws = RawMaterialLock::where(['reference_no'=>$reference_no, 'for_release'=>1])->get();
        if(count($raws)>0){
            
            foreach($raws as $row){
                $product_id = $row->product_id;
                $quantity = $row->quantity;
                $ingredient_id = $row->raw_material_id;

                //get product details
                $product = Product::find($product_id);
                //check if product exists
                if(count($product)>0){

                    //return quantity to inventory
                    $ingredient_inventory = RawMaterial::find($ingredient_id);
                    $ingredient_inventory->quantity = floatval($ingredient_inventory->quantity + $quantity);
                    echo $ingredient_inventory->quantity;
                    $ingredient_inventory->save();

                    //delete lines with 0 quantity
                    RawMaterialLock::where(['reference_no'=>$reference_no, 'product_id'=>$product_id, 'raw_material_id'=>$ingredient_id, 'quantity'=>'0.00'])->delete();
                    $row->delete();

                } else {
                    echo "no product";
                }
            }
        } 

    }

    public function ingredientReturn(){
        $reference_no = Input::get('reference_no');
        
        $rows = IngredientLockTemp::where(['reference_no'=>$reference_no])->get();
        if(count($rows)>0){
            foreach($rows as $row){
                $ingredientLock = new IngredientLock;
                $ingredientLock->reference_no = $reference_no;
                $ingredientLock->product_id = $row->product_id;
                $ingredientLock->ingredient_id = $row->ingredient_id;
                $ingredientLock->quantity = $row->quantity;
                $ingredientLock->save();
                $row->delete();
            }
        } 

        $raws = RawMaterialLockTemp::where(['reference_no'=>$reference_no])->get();
        if(count($raws)>0){
            foreach($raws as $row){
                $ingredientLock = new RawMaterialLock;
                $ingredientLock->reference_no = $reference_no;
                $ingredientLock->product_id = $row->product_id;
                $ingredientLock->raw_material_id = $row->raw_material_id;
                $ingredientLock->quantity = $row->quantity;
                $ingredientLock->save();
                $row->delete();
            }
        } 
    }

}