<?php namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use App\ProductIngredient;
use App\Category;
use App\Ingredient;
use App\RawMaterial;
use Illuminate\Auth\Guard;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\Facades\Form;
use Validator;

class ProductsController extends Controller {

	public function __construct(Guard $auth)
    {
        $this->middleware('auth');
        $this->auth = $auth;
    }

    public function index(){
    	$products = Product::where(['is_deleted'=>0])->get();
        $mode = "active";
    	return view('products.index', compact('products', 'mode'));
    }


    public function add(){
        $mode = 'add';
        $categories = Category::where('is_deleted',0)->get();
        $ingredientsList = Ingredient::where('is_deleted',0)->get(['id','name','unit'])->toArray();
        $ingredients = array();
        foreach($ingredientsList as $key => $val){
            $ingredients[$key] = array('id'=>$val['id'],'name'=>$val['name'],'unit'=>$val['unit']);
        }
        $rawmaterialsList = RawMaterial::where('is_deleted',0)->get(['id','name','unit'])->toArray();
        $rawmaterials = array();
        foreach($rawmaterialsList as $key => $val){
            $rawmaterials[$key] = array('id'=>$val['id'],'name'=>$val['name'],'unit'=>$val['unit']);
        }
        return view('products.add_edit',compact('mode','categories', 'ingredients', 'rawmaterials'));
    }

    public function postAdd(){
        $name = Input::get('name');
        $description = Input::get('description');
        $imageFile = Input::file('image');
        $price = Input::get('price');
        $categories = Input::get('categories');

        $ingredientName = Input::get('ingredientname');
        $ingredientQuantity = Input::get('ingredientqty');

        $ingredients = array();
        $len = count($ingredientName);
        for($x = 0; $x < $len; $x++){
            $iqty = isset($ingredientQuantity[$x]) ? floatval($ingredientQuantity[$x]) : 0;
            if(!empty($ingredientName[$x]) && !empty($iqty) && $iqty != 0){
                $ingredients[] = array('ingredient_id' => $ingredientName[$x], 'quantity' => $iqty);
            }
        }

        $rawmaterialName = Input::get('rawmaterialname');
        $rawmaterialQuantity = Input::get('rawmaterialqty');

        $rawmaterials = array();
        $len = count($rawmaterialName);
        for($x = 0; $x < $len; $x++){
            $iqty = isset($rawmaterialQuantity[$x]) ? floatval($rawmaterialQuantity[$x]) : 0;
            if(!empty($rawmaterialName[$x]) && !empty($iqty) && $iqty != 0){
                $rawmaterials[] = array('raw_material_id' => $rawmaterialName[$x], 'quantity' => $iqty);
            }
        }

        $product = new Product;
        $product->name = $name;
        $product->description = $description;
        $product->image = ($imageFile) ? time(). '_' . $imageFile->getClientOriginalName() : "";
        $product->price = $price;

        $input = Input::all();

        $rules = array(
            'name' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png,gif',
            'price' => 'required|numeric'
        );

        $messages = array(
            'image.required' => 'The product image field is required',
            'name.required' => 'This product name field is required',
            'price.required' => 'The price field is required',
            'price.numeric' => 'The price field must be numeric'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){
            //save image

            $path = 'uploads/'. $product->image;
            $img = Image::make($imageFile->getRealPath())->save($path);

            //save product
            $product->save();

            if($product->id){

                if(!empty($categories)){
                    $product->categories()->attach($categories);
                }

                if(!empty($ingredients)){
                    $product->ingredients()->attach($ingredients);
                }

                if(!empty($rawmaterials)){
                    $product->rawmaterials()->attach($rawmaterials);
                }

                $productlink = "<a href='/products/$product->id/edit'>$product->name</a>";

                return redirect("/products/")
                    ->with('success','Product '.$productlink.' successfully added');
            }
        }

        return redirect("/products/add")->withInput()->withErrors($validation);
    }


    public function edit($id){
        $product = Product::find($id);
        $categories = Category::where('is_deleted',0)->get();
        $pc = ProductCategory::where('product_id',$id)->get(['category_id'])->toArray();
        foreach($pc as $key => $val){
            $product_categories[] = $val['category_id'];
        }
        //get all (not deleted) ingredients
        $ingredientsList = Ingredient::where('is_deleted',0)->get(['id','name','unit'])->toArray();
        $ingredients = array();
        foreach($ingredientsList as $key => $val){
            $ingredients[$key] = array('id'=>$val['id'],'name'=>$val['name'],'unit'=>$val['unit']);
        }
        //get all ingredients associated to product
        $pi = $product->ingredients()->where('is_deleted',0)->get()->toArray();
        foreach($pi as $key => $val){
            $product_ingredients[] = array("id"=>$val['id'], "qty"=>$val['pivot']['quantity']);
        }
        //get all (not deleted) raw materials
        $rawmaterialsList = RawMaterial::where('is_deleted',0)->get(['id','name','unit'])->toArray();
        $rawmaterials = array();
        foreach($rawmaterialsList as $key => $val){
            $rawmaterials[$key] = array('id'=>$val['id'],'name'=>$val['name'],'unit'=>$val['unit']);
        }
        //get all raw materials associated to product
        $pi = $product->rawmaterials()->where('is_deleted',0)->get()->toArray();
        foreach($pi as $key => $val){
            $product_rawmaterials[] = array("id"=>$val['id'], "qty"=>$val['pivot']['quantity']);
        }
        $mode = 'edit';
        return view('products.add_edit',compact('product', 'mode', 'categories', 'product_categories', 'ingredients', 'product_ingredients', 'rawmaterials', 'product_rawmaterials'));
    }



    public function postEdit($id){
        $name = Input::get('name');
        $description = Input::get('description');
        $imageFile = Input::file('image');
        $price = Input::get('price');
        $categories = Input::get('categories');

        $ingredientName = Input::get('ingredientname');
        $ingredientQuantity = Input::get('ingredientqty');

        $ingredients = array();
        $len = count($ingredientName);
        for($x = 0; $x < $len; $x++){
            if(!empty($ingredientName[$x]) && !empty($ingredientQuantity[$x])){
                $ingredients[] = array('ingredient_id' => $ingredientName[$x], 'quantity' => $ingredientQuantity[$x]);
            }
        }

        $rawmaterialName = Input::get('rawmaterialname');
        $rawmaterialQuantity = Input::get('rawmaterialqty');

        $rawmaterials = array();
        $len = count($rawmaterialName);
        for($x = 0; $x < $len; $x++){
            $iqty = isset($rawmaterialQuantity[$x]) ? floatval($rawmaterialQuantity[$x]) : 0;
            if(!empty($rawmaterialName[$x]) && !empty($iqty) && $iqty != 0){
                $rawmaterials[] = array('raw_material_id' => $rawmaterialName[$x], 'quantity' => $iqty);
            }
        }

        $product = Product::find($id);
        $product->name = $name;
        $product->description = $description;
        if($imageFile){
            $product->image = time(). '_' . $imageFile->getClientOriginalName();
        }
        $product->price = $price;

        $input = Input::all();

        $rules = array(
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif',
            'price' => 'required|numeric'
        );

        $messages = array(
            'image.required' => 'The product image field is required',
            'price.required' => 'The price field is required',
            'price.numeric' => 'The price field must be numeric'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){
            //save image if replaced
            if($imageFile){
                $path = 'uploads/'. $product->image;
                $img = Image::make($imageFile->getRealPath())->save($path);
            }

            //save product
            $product->save();

            if($product->id){
                $product->categories()->sync($categories);
                $product->ingredients()->sync($ingredients);
                $product->rawmaterials()->sync($rawmaterials);

                $productlink = "<a href='/products/$id/edit'>$product->name</a>";

                return redirect("/products/")
                    ->with('success',"Product $productlink successfully updated");
            }
        }

        return redirect("/products/$id/edit")->withInput()->withErrors($validation)
                    ->with('data',$categories);
    }


    public function delete($id){
        $product = Product::find($id);
        $product->is_deleted = 1;
        $productName = $product->name;

        try {
            $product->save();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/products/')->with("error","<b>$productName</b> cannot be deleted it is being used by the system");
        }

        return redirect('/products/')->with("success","<b>$productName</b> has been deleted successfully");

    }

    public function deleted(){
        $products = Product::where('is_deleted',1)->get();
        $mode = "deleted";
        return view('products.index', compact('products', 'mode'));
    }

    public function restore($id){
        $product = Product::find($id);
        $product->is_deleted = 0;
        $productName = $product->name;

        try {
            $product->save();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/products/deleted')->with("error","<b>$productName</b> cannot be restored due to a conflict with other products");
        }

        return redirect('/products/')->with("success","<b>$productName</b> has been restored successfully");

    }

    public function getStocks($id){
        $product = Product::find($id);
        return $product->stocks();
    }

}
