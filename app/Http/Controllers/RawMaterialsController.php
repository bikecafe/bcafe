<?php namespace App\Http\Controllers;

use App\RawMaterial;
use Illuminate\Auth\Guard;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Intervention\Image\Facades\Form;
use Validator;
use DB;
use App\Action;
use Auth;

class RawMaterialsController extends Controller {

	public function __construct(Guard $auth)
    {
        $this->middleware('auth');
        $this->auth = $auth;
    }

    public function index(){
    	$rawMaterials = RawMaterial::where('is_deleted',0)->get();
        $mode = "active";
    	return view('inventory.raw.index', compact('rawMaterials', 'mode'));
    }

    public function add(){
        $mode = 'add';
        return view('inventory.raw.add_edit',compact('mode'));
    }

    public function postAdd(){
        $name = Input::get('name');
        $supplier = Input::get('supplier');
        $brand = Input::get('brand');
        $quantity = Input::get('quantity');
        $unit = Input::get('unit');
        $min_normal = Input::get('min_normal');
        $max_normal = Input::get('max_normal');

        $rawMaterial = new RawMaterial;
        $rawMaterial->name = $name;
        $rawMaterial->supplier = $supplier;
        $rawMaterial->brand = $brand;
        $rawMaterial->quantity = $quantity;
        $rawMaterial->unit = $unit;
        $rawMaterial->min_normal = $min_normal;
        $rawMaterial->max_normal = $max_normal;

        $input = Input::all();

        $rules = array(
            'name' => 'required',
            'supplier' => 'required',
            'quantity' => 'required|numeric'
        );

        $messages = array(
            'name.required' => 'The name field is required',
            'supplier.required' => 'The supplier field is required',
            'quantity.required' => 'The quantity field is required',
            'quantity.numeric' => 'The quantity field must be numeric'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){

            //save raw material to DB
            $rawMaterial->save();

            if($rawMaterial->id){

                //log initial stock
                $stock = new Action;
                $stock->action_type_id = 2; //update raw material stock
                $stock->user_id = Auth::user()->id; //who updated the stock
                $stock->reference_id = $rawMaterial->id;
                $stock->old_value = $rawMaterial->quantity;
                $stock->new_value = $rawMaterial->quantity;
                $stock->description = "initial stock";
                $stock->save();

                $rawlink = "<a href='/inventory/raw/$rawMaterial->id/edit'>$rawMaterial->name</a>";

                return redirect("/inventory/raw")
                    ->with('success','Raw Material '.$rawlink.' successfully added');
            }
        }

        return redirect("/inventory/raw/add")->withInput()->withErrors($validation);
    }

    public function edit($id){
        $raw = RawMaterial::find($id);
        $history = DB::table('actions')->join('users', 'users.id', '=', 'actions.user_id')->select('users.id', 'users.firstname', 'users.lastname', 'actions.created_at', 'actions.updated_at','actions.old_value','actions.new_value')->where('action_type_id',2)->where('reference_id',$id)->orderby('actions.updated_at','DESC')->get();
        $mode = 'edit';
        return view('inventory.raw.add_edit',compact('raw', 'mode','history'));
    }

    public function postEdit($id){
        $name = Input::get('name');
        $supplier = Input::get('supplier');
        $brand = Input::get('brand');
        $quantity = Input::get('quantity');
        $unit = Input::get('unit');
        $min_normal = Input::get('min_normal');
        $max_normal = Input::get('max_normal');

        $rawMaterial = RawMaterial::find($id);
        $oldStockValue = $rawMaterial->quantity; 
        $rawMaterial->name = $name;
        $rawMaterial->supplier = $supplier;
        $rawMaterial->brand = $brand;
        $rawMaterial->quantity = $quantity;
        $rawMaterial->unit = $unit;
        $rawMaterial->min_normal = $min_normal;
        $rawMaterial->max_normal = $max_normal;


        $input = Input::all();

        $rules = array(
            'name' => 'required',
            'supplier' => 'required',
            'quantity' => 'required|numeric'
        );

        $messages = array(
            'name.required' => 'Thie name field is required',
            'supplier.required' => 'The supplier field is required',
            'quantity.required' => 'The quantity field is required',
            'quantity.numeric' => 'The quantity field must be numeric'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){

            //save raw material
            $rawMaterial->save();

            if($rawMaterial->id){
                if($oldStockValue != $rawMaterial->quantity){
                    //log updated stock
                    $stock = new Action;
                    $stock->action_type_id = 2; //update raw material stock
                    $stock->user_id = Auth::user()->id; //who updated the stock
                    $stock->reference_id = $rawMaterial->id;
                    $stock->old_value = $oldStockValue;
                    $stock->new_value = $rawMaterial->quantity;
                    $stock->description = "updated stock";
                    $stock->save();
                }
                $rawlink = "<a href='/inventory/raw/$id/edit'>$rawMaterial->name</a>";

                return redirect("/inventory/raw/")
                    ->with('success',"Raw Material $rawlink successfully updated");
            }
        }

        return redirect("/inventory/raw/$id/edit")->withInput()->withErrors($validation);
    }

    public function delete($id){
        $rawMaterial = RawMaterial::find($id);
        $rawMaterial->is_deleted = 1;
        $rawMaterialName = $rawMaterial->name;

        try {
            $rawMaterial->save();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/inventory/raw/')->with("error","<b>$rawMaterialName</b> cannot be deleted it is being used by the system");
        }

        return redirect('/inventory/raw/')->with("success","<b>$rawMaterialName</b> has been deleted successfully");

    }

    public function deleted(){
        $rawMaterials = RawMaterial::where('is_deleted',1)->get();
        $mode = "deleted";
        return view('inventory.raw.index', compact('rawMaterials', 'mode'));
    }

    public function restore($id){
        $rawMaterial = RawMaterial::find($id);
        $rawMaterial->is_deleted = 0;
        $rawMaterialName = $rawMaterial->name;

        try {
            $rawMaterial->save();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/inventory/raw/deleted')->with("error","<b>$rawMaterialName</b> cannot be restored due to a conflict");
        }

        return redirect('/inventory/raw/')->with("success","<b>$rawMaterialName</b> has been deleted successfully");

    }
}
