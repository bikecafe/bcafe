<?php namespace App\Http\Controllers;

use App\Order;
use App\Category;
use App\Product;
use App\Setting;
use App\Action;
use App\ActionType;
use App\RawMaterial;
use App\Ingredient;
use Illuminate\Database\QueryException;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Libraries\Helpers;
use DB;
use Auth;

class ReportsController extends Controller {

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function sales($date_from = null, $date_to = null){
        //set default date to 5 days from today if none given
        $date_from = !empty($date_from) ? $date_from : date('Y-m-d', strtotime('-5 days'));
        $date_to = !empty($date_to) ? $date_to : date("Y-m-d");
       
        //query sales for date today
        $sales = $this->getSales($date_from,$date_to);
        $format = $this->formatSales($sales);
        $wasted = $this->getLoss($date_from,$date_to);
        $loss = $this->formatSales($wasted);
        $categorySales = $this->categorySales($sales);
        return view('reports.sales',compact('date_from','date_to','sales','format', 'categorySales','wasted', 'loss'));
    }

    public function getSales($date_from,$date_to){
        //get orders on given date
        $sales = [];
        $orders = DB::table('orders')->whereBetween(DB::raw('DATE(created_at)'), array($date_from,$date_to))->where('order_status_id',3)->get();
        
        if(!empty($orders)){
            foreach($orders as $order){
                $products = DB::table('order_lines')->where('order_id',$order->id)->get();
                if(!empty($products)){
                    foreach($products as $product){
                        $sales[$product->product_id] = isset($sales[$product->product_id]) ? $sales[$product->product_id] + $product->quantity : $product->quantity;
                    }
                }
            }
        }
        return $sales;
    }

    public function formatSales($sales){
        $categories = Category::where(['is_deleted'=>0])->get();
        $prod_sales = [];
        $total = 0.00;
        $category_sales = 0;
        foreach($categories as $category){
            $products = $category->products;
            $prod_sales[$category->id]['category_id'] = $category->id;
            $prod_sales[$category->id]['category_name'] = $category->name;
            $quantity = 0;
            foreach($products as $product){
                if(isset($sales[$product->id])){
                    $prod_sales[$category->id]['products'][] = array(
                        'id' => $product->id,
                        'name' => $product->name,
                        'price' => $product->price,
                        'quantity' => $sales[$product->id]
                        );
                    $total = $total + ($product->price*$sales[$product->id]);
                    $quantity += $sales[$product->id];
                } 
            }
            $prod_sales[$category->id]['total_sales'] = $quantity;
            $category_sales += $quantity;
            if(!isset($prod_sales[$category->id]['products'])){
                unset($prod_sales[$category->id]);
            }
        }
        if(!empty($prod_sales)){
            $prod_sales['total'] = $total;
            $prod_sales['total_category'] = $category_sales;
        }
        
        return $prod_sales;
    }

    public function getLoss($date_from,$date_to){
        //get orders on given date
        $loss = [];
        $orders = DB::table('orders')->whereBetween(DB::raw('DATE(created_at)'), array($date_from,$date_to))->where('order_status_id',4)->get();
        
        if(!empty($orders)){
            foreach($orders as $order){
                
                $cancelled = DB::table('order_workflow')->where('order_id',$order->id)->where('order_status_id',4)->orderBy('created_at','DESC')->first();
                if($cancelled){
                    //check if order stocks where wasted
                    $notes = $cancelled->notes ? json_decode($cancelled->notes) : array();
                    if(isset($notes->stocks) && $notes->stocks == "wasted"){
                        $products = DB::table('order_lines')->where('order_id',$order->id)->get();
                        if(!empty($products)){
                            foreach($products as $product){
                                $loss[$product->product_id] = isset($loss[$product->product_id]) ? $loss[$product->product_id] + $product->quantity : $product->quantity;
                            }
                        }
                    }
                }
                
            }
        }
        return $loss;
    }

    public function categorySales($sales){
        $categories = Category::where(['is_deleted'=>0])->get();
        $category_sales = [];
        $category_sales[] = array("Category", "Sales");
        foreach($categories as $category){
            $products = $category->products;
            $quantity = 0;
            foreach($products as $product){
                if(isset($sales[$product->id])){
                    $quantity += $sales[$product->id];
                } 
            }
            if($quantity > 0){
                $category_sales[] = array($category->name, $quantity);
            }
            
        }
        return $category_sales;
    }

    public function demand($date_from = null, $date_to = null){
        //set default date to 5 days from today if none given
        $date_from = !empty($date_from) ? $date_from : date('Y-m-d', strtotime('-5 days'));
        $date_to = !empty($date_to) ? $date_to : date("Y-m-d");
        
        //query top products
        $productGross = $this->topProductGross($date_from,$date_to);
        $topProducts = $this->topProducts($date_from,$date_to);
        return view('reports.demand',compact('date_from', 'date_to', 'productGross', 'topProducts'));
    }

    public function topProducts($date_from,$date_to){
        //get orders on date range
        $sales = [];
        $orders = DB::table('orders')->whereBetween(DB::raw('DATE(created_at)'), array($date_from,$date_to))->where('order_status_id',3)->get();
        
        $top_products = [];
        if(!empty($orders)){
            foreach($orders as $order){
                $products = DB::table('order_lines')->where('order_id',$order->id)->get();
                if(!empty($products)){
                    foreach($products as $product){
                        $top_products[$product->product_id] = isset($top_products[$product->product_id]) ? $top_products[$product->product_id] + ($product->quantity*$product->price) : $product->quantity*$product->price;
                    }
                }
            }
        }
        $top5 = [];
        if(!empty($top_products)){
            arsort($top_products);
            $top_products = array_slice($top_products, 0, 5, true);
            $colors = array('red','orange','yellow','green','blue');
            $x = 0;
            $top5[] = array("Product", "Sales", array('role'=>'style'));
            foreach($top_products as $id => $val){
                $prod_info = DB::table('products')->where('id',$id)->first();
                if(!empty($prod_info)){
                    $top5[] = array($prod_info->name, $val, $colors[$x]);
                }
                $x++;
            }
        }
        return $top5;
    }

    public function topProductGross($date_from,$date_to){
        //get orders on date range
        $sales = [];
        $orders = DB::table('orders')->whereBetween(DB::raw('DATE(created_at)'), array($date_from,$date_to))->where('order_status_id',3)->get();
        
        $top_products = [];
        $product_qty = [];
        if(!empty($orders)){
            foreach($orders as $order){
                $products = DB::table('order_lines')->where('order_id',$order->id)->get();
                if(!empty($products)){
                    foreach($products as $product){
                        $top_products[$product->product_id] = isset($top_products[$product->product_id]) ? $top_products[$product->product_id] + ($product->quantity*$product->price) : $product->quantity*$product->price;
                        $product_qty[$product->product_id] = isset($product_qty[$product->product_id]) ? $product_qty[$product->product_id] + $product->quantity : $product->quantity;
                    }
                }
            }
        }
        $top5 = [];
        if(!empty($top_products)){
            arsort($top_products);
            $top_products = array_slice($top_products, 0, 5, true);
            $x = 0;

            foreach($top_products as $id => $val){
                $prod_info = DB::table('products')->where('id',$id)->first();
                if(!empty($prod_info)){
                    $top5[] = array(
                        'name' => $prod_info->name, 
                        'quantity' => $product_qty[$id],
                        'amount' => $val
                    );
                }
                $x++;
            }
        }
        return $top5;
    }

    public function inventory($date_from = null, $date_to = null){
        //set default date to 5 days from today if none given
        $date_from = !empty($date_from) ? $date_from : date('Y-m-d', strtotime('-5 days'));
        $date_to = !empty($date_to) ? $date_to : date("Y-m-d");
       
        //query inventory stocks by quantity
        $inventory_quantity = $this->inventoryHistoryQuantity($date_from,$date_to);
        //query inventory stocks by percentage
        $inventory_percentage = $this->percentage($inventory_quantity);
        
        return view('reports.inventory',compact('date_from','date_to','inventory_quantity', 'inventory_percentage'));
    }

    public function inventoryHistoryQuantity($date_from = null, $date_to = null){
        //set default date to 5 days from today if none given
        $date_from = !empty($date_from) ? $date_from : date('Y-m-d', strtotime('-5 days'));
        $date_to = !empty($date_to) ? $date_to : date("Y-m-d");

        $raw_history = [];
        $raw_materials = RawMaterial::where('is_deleted',0)->get();
        if(!empty($raw_materials)){
            foreach($raw_materials as $raw){
                //get latest restock quantity
                $action = DB::table('actions')->where('action_type_id',2)->orderBy('created_at','DESC')->where('reference_id',$raw->id)->first();
                $original_quantity = !empty($action) ? floatval($action->new_value) : $raw->quantity;
                //get used quantity from completed orders
                $raw_used = DB::table('order_product_raw_materials')->whereBetween(DB::raw('DATE(created_at)'), array($date_from,$date_to))->where('raw_material_id',$raw->id)->get();
                $total_used = 0;
                if(!empty($raw_used)){
                    foreach($raw_used as $used){
                        $total_used += $used->quantity;
                    }
                }
                $raw_history[] = array(
                    'name' => $raw->name,
                    'original_quantity' => $original_quantity,
                    'used_quantity' => $total_used
                    );
            }
        }
        return $raw_history;
    }

    public function percentage($history){
        $percentage = [];
        if(!empty($history)){
            foreach($history as $idx => $item){
                if($item['used_quantity'] <= 0){
                    unset($history[$idx]);
                }
            }
            $sumArray = array();
            foreach ($history as $k=>$subArray) {
              foreach ($subArray as $id=>$value) {
                array_key_exists( $id, $sumArray ) ? $sumArray[$id] += $value : $sumArray[$id] = $value;
              }
            }
            foreach($history as $item){
                $percent = ($item['used_quantity'] / ($sumArray['used_quantity']+$sumArray['original_quantity']))*100;
                $total = ($item['original_quantity'] / ($sumArray['used_quantity']+$sumArray['original_quantity']))*100;
                $percentage[] = array(
                    'name' => $item['name'],
                    'used' => $percent,
                    'onhand' => $total,
                    );
            }
        }
        return $percentage;
    }
}