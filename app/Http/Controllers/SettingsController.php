<?php namespace App\Http\Controllers;

use App\Customer;
use App\Setting;
use Illuminate\Database\QueryException;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Libraries\Helpers;
use Auth;

class SettingsController extends Controller {

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function index(){
        $settings = Setting::all();
        return view('settings.index',compact('settings'));
    }

    public function postEdit(){
        //$delivery_fee = Input::get('delivery_fee');
        $contact_email = Input::get('contact_email');
        $delivery_places = Input::get('delivery_places');

        $input = Input::all();
        $rules = array(
            //'delivery_fee' => 'required',
            'contact_email' => 'required',
            'delivery_places' => 'required'
        );

        $messages = array(
            //'delivery_fee.required' => 'The delivery fee is required',
            'contact_email.required' => 'The contact email is required',
            'delivery_places.required' => 'Please enter atleast one delivery place'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){
            // $setting = Setting::where(['name'=>'delivery_fee'])->first();
            // $setting->value = $delivery_fee;
            // $setting->save();

            $setting = Setting::where(['name'=>'contact_email'])->first();
            $setting->value = $contact_email;
            $setting->save();

            $setting = Setting::where(['name'=>'delivery_places'])->first();
            $setting->value = $delivery_places;
            $setting->save();

            return redirect("/settings/")
                    ->with('success','Settings successfully updated');
        }
        return redirect("/settings")->withInput()->withErrors($validation);
    }

    
}