<?php namespace App\Http\Controllers;

use App\User;
use App\UserType;
use Illuminate\Auth\Guard;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Validator;

class UsersController extends Controller {

	public function __construct(Guard $auth)
	{
		$this->middleware('auth');
		$this->auth = $auth;
	}

	public function index(){
        $users = User::all();
        return view('users.index',compact('users'));
    }
	
	public function add(){
        $userTypes = UserType::all();
        $mode = 'add';

        $enabled = Input::old('disabled_flag_value', true);

        return view('users.add_edit',compact('userTypes','mode' , 'enabled'));
    }
	
	public function postAdd(){
        $password = Input::get('password');
        $username = Input::get('username');
        $firstName = Input::get('first_name');
        $lastName = Input::get('last_name');
        $email = Input::get('email');
        $userTypeId = Input::get('user_type_id');

        $user = new User;
        $user->username = strtolower($username);
        $user->password = Hash::make($password);
        $user->firstname = $firstName;
        $user->lastname = $lastName;
        $user->email = $email;
        $user->user_type_id = $userTypeId;

        $language_ids = Input::get('languages');

        $input = Input::all();

        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required|unique:users',
            'password' => 'required|min:8|strong_password'
        );

        $messages = array(
            'user_name.unique' => 'This username is already used',
            'user_name.required' => 'This username field is required',
            'first_name.required' => 'The first name field is required',
            'last_name.required' => 'The last name field is required',
            'email.unique' => 'This email is already used',
            'password.required' => 'The password field is required' ,
            'password.min' => 'The password should be 8 characters or more',
            'password.strong_password' => 'The password should have a number, a letter and a symbol'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){


            $user->save();

            if($user->id){

                $userlink = "<a href='/users/'"."$user->id"."'/edit'>$user->username</a>";

                return redirect("/users/")
                    ->with('success','User successfully added');
            }
        }

        return redirect("/users/add")->withInput()->withErrors($validation);

    }

    public function edit($id){
        $user = User::find($id);
        $userTypes = UserType::all();

        $mode = 'edit';
        return view('users.add_edit',compact('user','userTypes','mode'));
    }

    public function postEdit($id){

        $password = Input::get('password');
        $firstName = Input::get('first_name');
        $lastName = Input::get('last_name');
        $email = Input::get('email');
		$userTypeId = Input::get('user_type_id');

        $user = User::find($id);
        if(!empty($password)){
            $user->password = Hash::make($password);
        }
        $user->firstname = $firstName;
        $user->lastname = $lastName;
        $user->email = $email;
		$user->user_type_id = $userTypeId;

        $input = Input::all();

        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'min:8|strong_password'
        );

        $messages = array(
            'first_name.required' => 'The first name field is required',
            'last_name.required' => 'The last name field is required',
            'email.unique' => 'This email is already used',
            'password.required' => 'The password field is required' ,
            'password.min' => 'The password should be 8 characters or more',
            'password.strong_password' => 'The password should have a number, a letter and a symbol'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){


            $user->save();

            if($user->id){

                $userlink = "<a href='/users/$id/edit'>$user->username</a>";

                return redirect("/users/")
                    ->with('success',"User $userlink successfully updated");
            }
        }

        return redirect("/users/$id/edit")->withInput()->withErrors($validation);
    }

    public function delete($id){
        $user = User::find($id);
        $userName = $user->username;
        $currUserName = $this->auth->user()->username;

        if($userName == $currUserName){
            return redirect('/users')->with("error","Your account cannot be deleted while you are logged in");
        }

        try {
            $user->delete();
        } catch (QueryException $e) {
            dd($e);
            return redirect('/users')->with("error","<b>$userName</b> cannot be deleted it is being used by the system");
        }

        return redirect('/users')->with("success","<b>$userName</b> has been deleted successfully");

    }

}
