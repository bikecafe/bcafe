<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/', 'LoginController@index');
Route::get('/unauthorized', function()
{
    return view('permission');
});

Route::group(['middleware' => 'auth'],function(){
	Route::get('home', 'HomeController@index');

	//Products//
	Route::get('products','ProductsController@index');
	Route::get('products/add','ProductsController@add');
	Route::post('products/add','ProductsController@postAdd');
	Route::get('products/{id}/edit','ProductsController@edit');
	Route::post('products/{id}/edit','ProductsController@postEdit');
	Route::get('products/{id}/delete','ProductsController@delete');
	Route::post('products/{id}/delete','ProductsController@delete');
	Route::get('products/deleted','ProductsController@deleted');
	Route::post('products/{id}/restore','ProductsController@restore');
	Route::get('products/{id}/stocks','ProductsController@getStocks');

	//Categories
	Route::get('categories','CategoriesController@index');
	Route::get('categories/add','CategoriesController@add');
	Route::post('categories/add','CategoriesController@postAdd');
	Route::get('categories/{id}/edit','CategoriesController@edit');
	Route::post('categories/{id}/edit','CategoriesController@postEdit');
	Route::get('categories/{id}/delete','CategoriesController@delete');
	Route::post('categories/{id}/delete','CategoriesController@delete');
	Route::get('categories/deleted','CategoriesController@deleted');
	Route::post('categories/{id}/restore','CategoriesController@restore');
	Route::get('categories/rank','CategoriesController@rank');
	Route::post('categories/rank','CategoriesController@postRank');
	Route::get('categories/{id}/products','CategoriesController@products');
	Route::post('categories/{id}/products','CategoriesController@postProducts');

	//Orders
	Route::get('orders','OrdersController@index');
	Route::get('orders/add','OrdersController@add');
	Route::post('orders/add','OrdersController@postAdd');
	Route::get('orders/{id}/manage','OrdersController@manage');
	Route::post('orders/{id}/manage','OrdersController@postManage');
	Route::get('orders/all/onsite','OrdersController@onsite');
	Route::get('orders/all/online','OrdersController@online');

	//Order Ingredients
	Route::post('orders/ingredient/lock', 'OrdersController@ingredientLock');
	Route::post('orders/ingredient/unlock', 'OrdersController@ingredientUnlock');
	Route::post('orders/ingredient/unlocktemp', 'OrdersController@ingredientUnlockTemp');
	Route::post('orders/ingredient/release', 'OrdersController@ingredientRelease');
	Route::post('orders/ingredient/return', 'OrdersController@ingredientReturn');

	//Order Status//
	Route::get('orders/status','OrderStatusController@index');
    Route::get('orders/status/add','OrderStatusController@add');
    Route::post('orders/status/add','OrderStatusController@postAdd');
    Route::get('orders/status/{id}/edit', 'OrderStatusController@edit');
    Route::post('orders/status/{id}/edit', 'OrderStatusController@postEdit');
    Route::post('orders/status/{id}/delete','OrderStatusController@delete');

    //Raw Materials//
    Route::get('inventory/raw','RawMaterialsController@index');
    Route::get('inventory/raw/add','RawMaterialsController@add');
    Route::post('inventory/raw/add','RawMaterialsController@postAdd');
    Route::get('inventory/raw/{id}/edit', 'RawMaterialsController@edit');
    Route::post('inventory/raw/{id}/edit', 'RawMaterialsController@postEdit');
    Route::get('inventory/raw/{id}/delete', 'RawMaterialsController@delete');
    Route::post('inventory/raw/{id}/delete', 'RawMaterialsController@delete');
    Route::get('inventory/raw/deleted', 'RawMaterialsController@deleted');
    Route::post('inventory/raw/{id}/restore', 'RawMaterialsController@restore');

    //Ingredients//
    Route::get('inventory/ingredients','IngredientsController@index');
    Route::get('inventory/ingredients/add','IngredientsController@add');
    Route::post('inventory/ingredients/add','IngredientsController@postAdd');
    Route::get('inventory/ingredients/{id}/edit', 'IngredientsController@edit');
    Route::post('inventory/ingredients/{id}/edit', 'IngredientsController@postEdit');
    Route::get('inventory/ingredients/{id}/delete', 'IngredientsController@delete');
    Route::post('inventory/ingredients/{id}/delete', 'IngredientsController@delete');
    Route::get('inventory/ingredients/deleted', 'IngredientsController@deleted');
    Route::post('inventory/ingredients/{id}/restore', 'IngredientsController@restore');

	//Users//
	Route::get('users','UsersController@index');
    Route::get('users/add','UsersController@add');
    Route::post('users/add','UsersController@postAdd');
    Route::get('users/{id}/edit', 'UsersController@edit');
    Route::post('users/{id}/edit', 'UsersController@postEdit');
    Route::post('users/{id}/delete','UsersController@delete');
	
	//User Types//
	Route::get('users/types', 'UserTypesController@index');
	Route::get('users/types/add', 'UserTypesController@add');
	Route::post('users/types/add','UserTypesController@postAdd');
	Route::get('users/types/{id}/edit', 'UserTypesController@edit');
    Route::post('users/types/{id}/edit', 'UserTypesController@postEdit');
    Route::post('users/types/{id}/delete','UserTypesController@delete');

    //Customers
    Route::get('customers', 'CustomersController@index');
    Route::get('customers/{id}/view', 'CustomersController@view');

    Route::group(['middleware' => 'authorize'],function(){
	    //Settings
	    Route::get('settings', 'SettingsController@index');
	    Route::post('settings', 'SettingsController@postEdit');
	});

    //Sales Report
    Route::get('reports/sales/{date_from?}/{date_to?}', 'ReportsController@sales');

    //Product Demand
    Route::get('reports/demand/{date_from?}/{date_to?}', 'ReportsController@demand');

    //Status of Stocks
    Route::get('reports/inventory/{date_from?}/{date_to?}', 'ReportsController@inventory');
});