<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientLockTemp extends Model {

    protected $table = "ingredients_locked_temp";

	protected $fillable = [];

}
