<?php namespace App\Libraries\Repositories;

use App\Libraries\Helpers;
use Illuminate\Support\Facades\DB;

class BookingsRepository {

    public function getNewReferenceNo(){

    	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 5; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

        return $randomString;
	}

	public function checkIfReferenceNoExists($referenceNo){
		$booking = DB::table('bookings')->where('reference_no', $referenceNo)->first();

		if($booking){
			return true;
		}

		return false;
	}

}