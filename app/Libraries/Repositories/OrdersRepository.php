<?php namespace App\Libraries\Repositories;


use App\Libraries\Helpers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class OrdersRepository {


    public function getOrderDetailsByOrderId($orderId){
		$orderlines = DB::table('orderlines')->where('order_id', $orderId)->get();
		
		$productType = array(1=>"rooms", 2=>"menu", 3=>"extras");
		$orderDetails = [];

		foreach ($orderlines as $orderline)
		{	
			$productTypeId = $orderline->product_type_id;
			$referenceId = $orderline->reference_id;
			
			$productDetail = DB::table("$productType[$productTypeId]")->where('id', $referenceId)->first();
			
			$orderDetails[] = array(
				"id" => $orderline->id,
				"product_type_id" => $productTypeId,
				"reference_id" => $referenceId,
				"name" => $productDetail->name,
				"description" => $productDetail->description,
				"price" => $orderline->unit_price,
				"quantity" => $orderline->quantity
			);
		};

        return $orderDetails;
	}
	
    public function getOrderDetailsByProductTypeIdandReferenceId($productTypeId,$referenceId){
		
		$productType = array(1=>"rooms", 2=>"menu", 3=>"extras");
		
		$productQuery = "SELECT * FROM " . $productType[$productTypeId] . " WHERE id = " . $referenceId;
		
		$productDetails = DB::select( DB::raw("SELECT * FROM ". $productType[$productTypeId] . "WHERE id = :referenceid"), array(
		   'referenceid' => $referenceId,));

        return $productDetails;
    }

} 