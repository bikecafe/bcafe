<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $table = "orders";

	protected $fillable = [];

	public function type(){
		return $this->belongsTo('App\OrderType', 'order_type_id');
	}

    public function customer(){
        return $this->belongsTo('App\Customer', 'customer_id');
    }

	public function status(){
		return $this->belongsTo('App\OrderStatus', 'order_status_id');
	}

	public function orderlines()
    {
        return $this->hasMany('App\OrderLine', 'order_id');
    }

    public function workflow(){
    	return $this->hasMany('App\OrderWorkflow', 'order_id');
    }

    public function getFirstWorkflow(){
    	return $this->workflow()->orderBy('created_at', 'asc')->first();
    }

    public function getLastWorkflow(){
    	return $this->workflow()->orderBy('created_at', 'desc')->first();
    }

    public function acquisition(){
        return $this->belongsTo('App\OrderAcquisition', 'order_acquisition_id');
    }

    public function customer_address(){
        return $this->belongsTo('App\CustomerAddress', 'customer_address_id');
    }

    public function customer_contact(){
        return $this->belongsTo('App\CustomerContact', 'customer_contact_id');
    }
}
