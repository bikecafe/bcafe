<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProductIngredient extends Model {

    protected $table = "order_product_ingredients";

	protected $fillable = [];

	public function order(){
		return $this->belongsTo('App\Order', 'order_id');
	}

	public function product(){
		return $this->belongsTo('App\Product', 'product_id');
	}

	public function ingredient(){
		return $this->belongsTo('App\Ingredient', 'ingredient_id');
	}
}
