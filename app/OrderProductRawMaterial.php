<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProductRawMaterial extends Model {

    protected $table = "order_product_raw_materials";

	protected $fillable = [];

	public function order(){
		return $this->belongsTo('App\Order', 'order_id');
	}

	public function product(){
		return $this->belongsTo('App\Product', 'product_id');
	}

	public function raw_material(){
		return $this->belongsTo('App\RawMaterial', 'raw_material_id');
	}
}
