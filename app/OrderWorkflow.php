<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderWorkflow extends Model {

    protected $table = "order_workflow";

	protected $fillable = [];

	public function order(){
		return $this->belongsTo('App\Order', 'order_id');
	}

	public function status(){
		return $this->belongsTo('App\OrderStatus', 'order_status_id');
	}

	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}

}
