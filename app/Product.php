<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	public function categories(){
        return $this->belongsToMany('App\Category','product_categories' , 'product_id','category_id')->withTimestamps();
    }

    public function ingredients(){
        return $this->belongsToMany('App\Ingredient','product_ingredients' , 'product_id','ingredient_id')->withPivot('quantity')->withTimestamps();
    }

    public function rawmaterials(){
        return $this->belongsToMany('App\RawMaterial','product_raw_materials' , 'product_id','raw_material_id')->withPivot('quantity')->withTimestamps();
    }

    public function stocks(){
    	$ingredients = $this->ingredients;
        $rawmaterials = $this->rawmaterials;

        $ingredient_present = true;
        $rawmaterial_present = true;

    	$ingredient_total = 0;

        //return 0 if no ingredients or raw materials are associated to the product
        if(count($ingredients) == 0 && count($rawmaterials) == 0){
            return 0;
        }

        //consolidate ingredients with same ingredient_id
        if(count($ingredients) > 0){
            $ingredient_stock = [];
            $ingredient_list = [];
            foreach($ingredients as $ingredient){
                $ingredient_stock[$ingredient->id] = floatval($ingredient->quantity); 
                if(isset($ingredient_list[$ingredient->id])){
                    $ingredient_list[$ingredient->id] = floatval($ingredient_list[$ingredient->id] + $ingredient->pivot->quantity);
                } else {
                    $ingredient_list[$ingredient->id] = floatval($ingredient->pivot->quantity);
                }
            }
            //loop through each consolidated ingredient and calculate stocks
            $qty = [];
            foreach($ingredient_list as $id => $quantity){
                $qty[] = intval($ingredient_stock[$id] / $ingredient_list[$id]);
            }
            $ingredient_total = !empty($qty) ? min($qty) : 0;
        } else {
            $ingredient_present = false;
        }
        
        $rawmaterial_total = 0;
        if(count($rawmaterials) > 0){
            //consolidate raw materials with same raw_material_id
            $rawmaterial_stock = [];
            $rawmaterial_list = [];
            foreach($rawmaterials as $rawmaterial){
                $rawmaterial_stock[$rawmaterial->id] = floatval($rawmaterial->quantity); 
                if(isset($rawmaterial_list[$rawmaterial->id])){
                    $rawmaterial_list[$rawmaterial->id] = floatval($rawmaterial_list[$rawmaterial->id] + $rawmaterial->pivot->quantity);
                } else {
                    $rawmaterial_list[$rawmaterial->id] = floatval($rawmaterial->pivot->quantity);
                }
            }

            //loop through each consolidated raw materials and calculate stocks
            $qty = [];
            foreach($rawmaterial_list as $id => $quantity){
                $qty[] = intval($rawmaterial_stock[$id] / $rawmaterial_list[$id]);
            }
            $rawmaterial_total = !empty($qty) ? min($qty) : 0;
        } else {
            $rawmaterial_present = false;
        }
        //return lower amount
    	return ($ingredient_present == true && $rawmaterial_present == true ) ? min($ingredient_total,$rawmaterial_total) : ($ingredient_present == true ? $ingredient_total : $rawmaterial_total);
    }

}
