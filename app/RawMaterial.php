<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RawMaterial extends Model {

    protected $table = "raw_materials";

	protected $fillable = [];

	public function eoq_level(){
		//safe level
		if($this->quantity > $this->max_normal){
			return '<div class="progress" style="margin-bottom:0 !important"><div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%;color:#5bc0de">100</div></div>';
		} 
		//normal
		elseif($this->quantity >= $this->min_normal){
			return '<div class="progress" style="margin-bottom:0 !important"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%;color:#5cb85c">70</div></div>';
		} 
		//critical
		else {
			return '<div class="progress" style="margin-bottom:0 !important"><div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:30%;color:#d9534f">30</div></div>';
		}
	} 

}
