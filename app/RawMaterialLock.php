<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RawMaterialLock extends Model {

    protected $table = "raw_materials_locked";

	protected $fillable = [];

}
