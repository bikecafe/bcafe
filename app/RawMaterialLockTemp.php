<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RawMaterialLockTemp extends Model {

    protected $table = "raw_materials_locked_temp";

	protected $fillable = [];

}
