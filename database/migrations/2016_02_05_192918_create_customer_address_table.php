<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_address', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('customer_id')->index('customeraddress_customer_fk_idx');
			$table->string('house_bldg')->nullable();
			$table->string('street');
			$table->string('village_brgy');
			$table->string('city')->nullable();
			$table->string('landmark')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_address');
	}

}
