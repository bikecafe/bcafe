<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerContactTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_contact', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('customer_id')->nullable()->index('customercontact_customer_fk_idx');
			$table->string('contact_number', 45);
			$table->boolean('is_primary')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_contact');
	}

}
