<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('reference_no', 45)->unique('reference_no_UNIQUE');
			$table->integer('order_status_id')->index('order_orderstatus_fk_idx');
			$table->bigInteger('customer_id')->index('order_customer_fk_idx');
			$table->text('remarks', 65535)->nullable();
			$table->integer('voucher_id')->nullable();
			$table->decimal('total_price', 10)->nullable();
			$table->decimal('change_for', 10)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
