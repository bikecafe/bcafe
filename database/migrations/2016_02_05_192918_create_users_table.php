<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('user_type_id')->index('usertypeusers_fk_idx');
			$table->string('firstname', 100);
			$table->string('lastname', 100);
			$table->string('username', 40)->unique('username_UNIQUE');
			$table->string('email', 100)->unique('email_UNIQUE');
			$table->string('password', 150);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
