<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductIngredientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_ingredients', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('product_id')->index('productingredients_product_fk_idx');
			$table->integer('ingredient_id')->index('productingredients_ingredient_fk_idx');
			$table->decimal('quantity', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_ingredients');
	}

}
