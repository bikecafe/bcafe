<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToProductIngredientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_ingredients', function(Blueprint $table)
		{
			$table->foreign('ingredient_id', 'productingredients_ingredient_fk_idx')->references('id')->on('ingredients')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('product_id', 'productingredients_product_fk_idx')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_ingredients', function(Blueprint $table)
		{
			$table->dropForeign('productingredients_ingredient_fk_idx');
			$table->dropForeign('productingredients_product_fk_idx');
		});
	}

}
