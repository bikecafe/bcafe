<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductRawMaterialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_raw_materials', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('product_id')->index('productrawmaterials_product_fk_idx');
			$table->integer('raw_material_id')->index('productrawmaterials_ingredient_fk_idx');
			$table->decimal('quantity', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_raw_materials');
	}

}
