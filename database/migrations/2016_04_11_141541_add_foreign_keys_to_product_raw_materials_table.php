<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToProductRawMaterialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_raw_materials', function(Blueprint $table)
		{
			$table->foreign('raw_material_id', 'productrawmaterials_rawmaterial_fk_idx')->references('id')->on('raw_materials')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('product_id', 'productrawmaterials_product_fk_idx')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_raw_materials', function(Blueprint $table)
		{
			$table->dropForeign('productrawmaterials_rawmaterial_fk_idx');
			$table->dropForeign('productrawmaterials_product_fk_idx');
		});
	}

}
