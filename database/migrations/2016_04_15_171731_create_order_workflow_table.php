<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderWorkflowTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_workflow', function(Blueprint $table)
		{
			$table->bigInteger('id',true);
			$table->integer('order_id')->index('orderworkflow_orders_fk_idx');
			$table->integer('order_status_id')->index('orderworkflow_orderstatus_fk_idx');
			$table->bigInteger('user_id')->index('orderworkflow_users_fk_idx');
			$table->text('notes', 65535)->nullable();
			$table->timestamps();
		});

		Schema::table('order_workflow', function(Blueprint $table)
		{
			$table->foreign('order_id', 'orderworkflow_order_fk_idx')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('order_status_id', 'orderworkflow_orderstatus_fk_idx')->references('id')->on('order_status')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_id', 'orderworkflow_user_fk_idx')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_workflow');
	}

}
