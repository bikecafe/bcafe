<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsLockedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ingredients_locked', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('reference_no')->index('ingredientslocked_orderreferenceno_idx');
			$table->integer('product_id')->index('ingredientslocked_product_idx');
			$table->integer('ingredient_id')->index('ingredientslocked_ingredient_idx');
			$table->decimal('quantity', 10);
			$table->timestamps();
		});

		Schema::table('ingredients_locked', function(Blueprint $table)
		{
			$table->foreign('product_id', 'ingredientslocked_product_fk_idx')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('ingredient_id', 'ingredientslocked_ingredient_fk_idx')->references('id')->on('ingredients')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ingredients_locked');
	}

}
