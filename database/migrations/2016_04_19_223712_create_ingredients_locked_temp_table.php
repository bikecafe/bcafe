<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsLockedTempTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ingredients_locked_temp', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('reference_no')->index('ingredientslockedtemp_orderreferenceno_idx');
			$table->integer('product_id')->index('ingredientslockedtemp_product_idx');
			$table->integer('ingredient_id')->index('ingredientslockedtemp_ingredient_idx');
			$table->decimal('quantity', 10);
			$table->timestamps();
		});

		Schema::table('ingredients_locked_temp', function(Blueprint $table)
		{
			$table->foreign('product_id', 'ingredientslockedtemp_product_fk_idx')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('ingredient_id', 'ingredientslockedtemp_ingredient_fk_idx')->references('id')->on('ingredients')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ingredients_locked_temp');
	}

}
