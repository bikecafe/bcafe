<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForReleaseFieldToIngredientsLockedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ingredients_locked', function(Blueprint $table)
		{
			$table->boolean('for_release')->default(0)->after('quantity');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ingredients_locked', function(Blueprint $table)
		{
			$table->dropColumn('for_release');
		});
	}

}
