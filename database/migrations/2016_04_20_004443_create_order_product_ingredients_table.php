<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductIngredientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_product_ingredients', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('order_id')->index('orderproductingredients_orderid_idx');
			$table->integer('reference_no')->index('orderproductingredients_orderreferenceno_idx');
			$table->integer('product_id')->index('orderproductingredients_product_idx');
			$table->integer('ingredient_id')->index('orderproductingredients_ingredient_idx');
			$table->decimal('quantity', 10);
			$table->timestamps();
		});

		Schema::table('order_product_ingredients', function(Blueprint $table)
		{
			$table->foreign('order_id', 'orderproductingredients_order_fk_idx')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('product_id', 'orderproductingredients_product_fk_idx')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('ingredient_id', 'orderproductingredients_ingredient_fk_idx')->references('id')->on('ingredients')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_product_ingredients');
	}

}
