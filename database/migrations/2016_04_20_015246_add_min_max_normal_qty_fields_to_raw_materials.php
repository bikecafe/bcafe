<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinMaxNormalQtyFieldsToRawMaterials extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('raw_materials', function(Blueprint $table)
		{
			$table->decimal('min_normal',10,3)->after('unit');
			$table->decimal('max_normal',10,3)->after('min_normal');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('raw_materials', function(Blueprint $table)
		{
			$table->dropColumn('min_normal');
			$table->dropColumn('max_normal');
		});
	}

}
