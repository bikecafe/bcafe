<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPrimaryFieldToCustomerAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customer_address', function(Blueprint $table)
		{
			$table->boolean('is_primary')->default(0)->after('landmark');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customer_address', function(Blueprint $table)
		{
			$table->dropColumn('is_primary');
		});
	}

}
