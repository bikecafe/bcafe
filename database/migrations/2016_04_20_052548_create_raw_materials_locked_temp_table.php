<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawMaterialsLockedTempTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('raw_materials_locked_temp', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('reference_no')->index('rawmaterialslockedtemp_orderreferenceno_idx');
			$table->integer('product_id')->index('rawmaterialslockedtemp_product_idx');
			$table->integer('raw_material_id')->index('rawmaterialslockedtemp_rawmaterial_idx');
			$table->decimal('quantity', 10);
			$table->boolean('for_release')->default(0);
			$table->timestamps();
		});

		Schema::table('raw_materials_locked_temp', function(Blueprint $table)
		{
			$table->foreign('product_id', 'rawmaterialslockedtemp_product_fk_idx')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('raw_material_id', 'rawmaterialslockedtemp_ingredient_fk_idx')->references('id')->on('raw_materials')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('raw_materials_locked_temp');
	}

}
