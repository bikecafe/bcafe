<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateQuantityFieldFromRawMaterialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('raw_materials', function(Blueprint $table)
		{
			$table->decimal('quantity', 10)->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('raw_materials', function(Blueprint $table)
		{
			$table->integer('quantity')->change();
		});
	}

}
