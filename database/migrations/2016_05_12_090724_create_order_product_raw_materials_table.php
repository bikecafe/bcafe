<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductRawMaterialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_product_raw_materials', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('order_id')->index('orderproductrawmaterials_orderid_idx');
			$table->integer('reference_no')->index('orderproductrawmaterials_orderreferenceno_idx');
			$table->integer('product_id')->index('orderproductrawmaterials_product_idx');
			$table->integer('raw_material_id')->index('orderproductrawmaterials_rawmaterial_idx');
			$table->decimal('quantity', 10);
			$table->timestamps();
		});

		Schema::table('order_product_raw_materials', function(Blueprint $table)
		{
			$table->foreign('order_id', 'orderproductrawmaterials_order_fk_idx')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('product_id', 'orderproductrawmaterials_product_fk_idx')->references('id')->on('products')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('raw_material_id', 'orderproductrawmaterials_rawmaterial_fk_idx')->references('id')->on('raw_materials')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_product_raw_materials');
	}

}
