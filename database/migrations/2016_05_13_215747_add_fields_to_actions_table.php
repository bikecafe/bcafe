<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToActionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('actions', function(Blueprint $table)
		{
			$table->integer('reference_id')->after('user_id');
			$table->string('old_value',255)->after('reference_id');
			$table->string('new_value',255)->after('old_value');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('actions', function(Blueprint $table)
		{
			$table->dropColumn('reference_id');
			$table->dropColumn('old_value');
			$table->dropColumn('new_value');
		});
	}

}
