<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCustomerAddressIdFieldOnOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::statement('ALTER TABLE `orders` MODIFY `customer_address_id` INTEGER NULL');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		DB::statement('ALTER TABLE `orders` MODIFY `customer_address_id` INTEGER NOT NULL');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
