<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataForCancelledOrderWorkflows extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('UPDATE `order_workflow` SET `notes` = CONCAT(CONCAT(\'{"stocks":"\',`notes`),\'"}\') WHERE `order_status_id` = 4');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('UPDATE `order_workflow` SET `notes` = SUBSTRING(`notes`,12) WHERE `order_status_id` = 4');
		DB::statement('UPDATE `order_workflow` SET `notes` = SUBSTRING_INDEX(`notes`,"\"",1) WHERE `order_status_id` = 4');
	}

}
