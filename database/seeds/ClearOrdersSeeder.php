<?php

use Illuminate\Exception;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class ClearOrdersSeeder extends Seeder {

	/**
	 * Truncate orders table.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::beginTransaction();

		try{
			DB::statement('SET FOREIGN_KEY_CHECKS=0;');

			//Truncate orders table
			DB::statement('TRUNCATE orders');

			//Truncate order_lines table
			DB::statement('TRUNCATE order_lines');
			
			DB::statement('SET FOREIGN_KEY_CHECKS=1;');
			DB::commit();
		}
		catch(Exception $e){
			DB::rollback();
		}
		
	}

}
