<?php

use Illuminate\Exception;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Customer;
use App\CustomerAddress;
use App\CustomerContact;


class CustomerSeeder extends Seeder {

	/**
	 * Insert default onsite customer.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::beginTransaction();

		try{
			DB::statement('SET FOREIGN_KEY_CHECKS=0;');

			//Truncate customers table
			DB::statement('TRUNCATE customers');

			//Truncate customer_address table
			DB::statement('TRUNCATE customer_address');

			//Truncate customer_contact table
			DB::statement('TRUNCATE customer_contact');

			//Insert into customers table
			Customer::create([
				'id' 			=> 1,
				'firstname'		=> 'Onsite',
				'lastname'		=> 'Customer',
				'username'		=> 'customer',
				'email'			=> 'onsitecustomer@route88bikecafe.ml',
				'password'		=> '5f4dcc3b5aa765d61d8327deb882cf99',
				'is_activated'	=> 1
			]);

			//Insert into customer_address table
			CustomerAddress::create([
				'customer_id'	=> 1,
				'house_bldg'	=> 'n/a',
				'street'		=> 'n/a',
				'village_brgy'	=> 'n/a',
				'city'			=> 'n/a'
			]);

			//Insert into customer_contact table
			CustomerContact::create([
				'customer_id'	=> 1,
				'contact_number'=> '1234567',
				'is_primary'	=> 1
			]);
			
			DB::statement('SET FOREIGN_KEY_CHECKS=1;');
			DB::commit();
		}
		catch(Exception $e){
			DB::rollback();
		}
		
	}

}
