<?php

use Illuminate\Exception;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\OrderStatus;


class OrderStatusSeeder extends Seeder {

	/**
	 * Insert default order statuses
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::beginTransaction();

		try{
			DB::statement('SET FOREIGN_KEY_CHECKS=0;');

			//Truncate order_status table
			DB::statement('TRUNCATE order_status');

			//Insert to order_status table
			OrderStatus::create([
				'id' 			=> 1,
				'name'			=> 'pending'
			]);
			OrderStatus::create([
				'id' 			=> 2,
				'name'			=> 'processing'
			]);
			OrderStatus::create([
				'id' 			=> 3,
				'name'			=> 'delivered'
			]);
			OrderStatus::create([
				'id' 			=> 4,
				'name'			=> 'cancelled'
			]);
			
			DB::statement('SET FOREIGN_KEY_CHECKS=1;');
			DB::commit();
		}
		catch(Exception $e){
			DB::rollback();
		}
		
	}

}
