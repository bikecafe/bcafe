<?php

use Illuminate\Exception;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\OrderType;


class OrderTypeSeeder extends Seeder {

	/**
	 * Insert default order types.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::beginTransaction();

		try{
			DB::statement('SET FOREIGN_KEY_CHECKS=0;');

			//Insert to order_types table
			OrderType::create([
				'id' 			=> 1,
				'name'			=> 'offline'
			]);
			OrderType::create([
				'id' 			=> 2,
				'name'			=> 'online'
			]);
			
			DB::statement('SET FOREIGN_KEY_CHECKS=1;');
			DB::commit();
		}
		catch(Exception $e){
			DB::rollback();
		}
		
	}

}
