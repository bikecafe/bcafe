<?php

use Illuminate\Exception;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Setting;


class SettingSeeder extends Seeder {

	/**
	 * Insert default settings.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::beginTransaction();

		try{
			DB::statement('SET FOREIGN_KEY_CHECKS=0;');

			//Truncate settings table
			DB::statement('TRUNCATE settings');


			//Insert into settings table
			Setting::create([
				'id' 			=> 1,
				'name'			=> 'delivery_fee',
				'value'			=> '50'
			]);

			Setting::create([
				'id' 			=> 2,
				'name'			=> 'contact_email',
				'value'			=> 'r88bikecafe@gmail.com'
			]);

			Setting::create([
				'id' 			=> 3,
				'name'			=> 'delivery_places',
				'value'			=> "Sikatuna Village,Project 2,Project 3"
			]);
			
			DB::statement('SET FOREIGN_KEY_CHECKS=1;');
			DB::commit();
		}
		catch(Exception $e){
			DB::rollback();
		}
		
	}

}
