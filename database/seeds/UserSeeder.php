<?php

use Illuminate\Exception;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\UserType;


class UserSeeder extends Seeder {

	/**
	 * Insert coke glass products.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::beginTransaction();

		try{
			DB::statement('SET FOREIGN_KEY_CHECKS=0;');

			//Insert to user_types table
			UserType::create([
				'id' 			=> 1,
				'name'			=> 'admin',
				'description'	=> 'admin account'
			]);
			UserType::create([
				'id' 			=> 2,
				'name'			=> 'inventory manager',
				'description'	=> 'inventory manager account'
			]);
			UserType::create([
				'id' 			=> 3,
				'name'			=> 'cashier',
				'description'	=> 'cashier account'
			]);

			//Insert into users table
			User::create([
				'id' 			=> 1,
				'user_type_id'	=> 1,
				'firstname'		=> 'admin',
				'lastname'		=> 'account',
				'username'		=> 'admin',
				'email'			=> 'replace@this.com',
				'password'		=> '$2y$10$Y9w2HtLSK7.siOkpYdmg2uKEj68QJ19HILFcbwpxgcgiMLsOW5Kqu'
			]);
			
			DB::statement('SET FOREIGN_KEY_CHECKS=1;');
			DB::commit();
		}
		catch(Exception $e){
			DB::rollback();
		}
		
	}

}
