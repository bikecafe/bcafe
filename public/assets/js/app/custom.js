

$(function(){

	
	/*$('.dropdown-menu').click(function(event){
	  event.stopPropagation();
	});*/

	
	/********************************
	Toggle Aside Menu
	********************************/
	
	$(document).on('click', '.navbar-toggle', function(){
		$('aside.left-panel').toggleClass('collapsed');
	});
	
	
	
	
	
	/********************************
	Aside Navigation Menu
	********************************/

	$("aside.left-panel nav.navigation > ul > li:has(ul) > a").click(function(){
		
		if( $("aside.left-panel").hasClass('collapsed') == false || $(window).width() < 768 ){

		$("aside.left-panel nav.navigation > ul > li > ul").slideUp(300);
		$("aside.left-panel nav.navigation > ul > li").removeClass('active');
		
		if(!$(this).next().is(":visible"))
		{
			
			$(this).next().slideToggle(300,function(){ $("aside.left-panel:not(.collapsed)").getNiceScroll().resize(); });
			$(this).closest('li').addClass('active');
		}
		
		return false;
		
		}
		
	});
	
	
	
	/********************************
	popover
	********************************/
	if( $.isFunction($.fn.popover) ){
	$('.popover-btn').popover();
	}
	
	
	
	/********************************
	tooltip
	********************************/
	if( $.isFunction($.fn.tooltip) ){
	$('.tooltip-btn').tooltip()
	}
	
	
	
	/********************************
	NanoScroll - fancy scroll bar
	********************************/
	if( $.isFunction($.fn.niceScroll) ){
	$(".nicescroll").niceScroll({
	
		cursorcolor: '#9d9ea5',
		cursorborderradius : '0px'		
		
	});
	}
	

	if( $.isFunction($.fn.niceScroll) ){
	$("aside.left-panel:not(.collapsed)").niceScroll({
		cursorcolor: '#8e909a',
		cursorborder: '0px solid #fff',
		cursoropacitymax: '0.5',
		cursorborderradius : '0px'	
	});
	}

	
	
	
	
	/********************************
	Input Mask
	********************************/
	if( $.isFunction($.fn.inputmask) ){
		$(".inputmask").inputmask();
	}
	
	
	

	/********************************
	TagsInput
	********************************/
	if( $.isFunction($.fn.tagsinput) ){
		$('.tagsinput').tagsinput();
	}

	
	/********************************
	wysihtml5
	********************************/
	if( $.isFunction($.fn.wysihtml5) ){
		$('.wysihtml').wysihtml5();
	}
	
	
	
	/********************************
	wysihtml5
	********************************/
	if( $.isFunction($.fn.ckeditor) ){
	CKEDITOR.disableAutoInline = true;
	$('#ckeditor').ckeditor();
	$('.inlineckeditor').ckeditor();
	}

    $(".select2").select2({minimumResultsForSearch: Infinity});
    $(".select2-norm").select2();
    $(".select2-clear").select2({ allowClear: true, placeholder: $(this).data("placeholder")});
    $(".integer").inputmask('integer',{ rightAlign: false});
    $(".decimal").inputmask({'mask':"9{0,7}.9{0,2}", greedy: false});
    // responsive breadcrumbs
    $(window).resize(function() {

        //add after home <div class="btn btn-default">...</div>
        //ellipses = $(".btn-breadcrumb :nth-child(2)")
        //if ($(".btn-breadcrumb a:hidden").length >0) {ellipses.show()} else {ellipses.hide()}

    })

    var zindex = $(".btn-breadcrumb .btn").length + 1;
    $(".btn-breadcrumb .btn").each(function() {
        $( this ).css("zIndex",zindex--);
    });


    // Spinner
    $(".panel-body").on('click','.spinner .btn:first-of-type', function() {
        var input = $(this).closest('.spinner').find('input');
        var inc = parseInt(input.val(), 10);
        var maxval = $(this).closest('.spinner').find('input').attr('data-maxval');
        maxval = (maxval == 'undefined' || maxval == null || maxval == "") ? null : maxval;
        inc = isNaN(inc) ? 0 : inc;
        if(maxval != null && inc == maxval){
            input.val( inc );
        } else {
            input.val( inc + 1);
        }
        
        input.trigger("change");
    });
    $(".panel-body").on('click', '.spinner .btn:last-of-type', function() {
        var input = $(this).closest('.spinner').find('input');
        var dec = parseInt(input.val(), 10);
		var minval = $(this).closest('.spinner').find('input').attr('data-minval');
		minval = (minval == 'undefined' || minval == null || minval == "") ? 0 : minval;
        dec = isNaN(dec) ? 0 : dec;
        if(dec == 0 && dec == minval){
            input.val(dec);
        }
        if(dec > 0 && dec > minval){
            input.val( dec - 1);
        }
        input.trigger("change");
    });
	
	/********************************
	Scroll To Top
	********************************/
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});

});


/********************************
Toggle Full Screen
********************************/

function toggleFullScreen() {
	if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
		if (document.documentElement.requestFullScreen) {
			document.documentElement.requestFullScreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullScreen) {
			document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	} else {
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
	}
}



function swlConfirm(confirmText){
    $(".delete-row").submit(function(e){
        e.preventDefault();
        var form = $(this);
        swal({
                title: "Are you sure?",
                text: confirmText,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,   closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    form.unbind('submit').submit();
                }
            }
        );
    });
}

function resConfirm(confirmText){
    $(".restore-row").submit(function(e){
        e.preventDefault();
        var form = $(this);
        swal({
                title: "Are you sure?",
                text: confirmText,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#70ba63",
                confirmButtonText: "Yes, restore it!",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,   closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    form.unbind('submit').submit();
                }
            }
        );
    });
}

function extractDates(element){
    var dateListArray = [];
    var hiddenInput = $(element);
    var dateValues = hiddenInput.val();
    var dateList = dateValues.split(',');
    var dateListLength = dateList.length;
    for (var i = 0; i < dateListLength; i++) {
        var utcSplit = dateList[i].split('/');
        var dateUtc = new Date(utcSplit[2],parseInt(utcSplit[1])-1,utcSplit[0]);
        dateListArray.push(dateUtc);
    }
    return dateListArray;
}

function cleanPrice(price){
    price = price.split(' ')[0]
    price = price.replace(",",".")
    return parseFloat(price);
}

function timeDiff(start, end) {
    start = moment(start, ["h:mm A"]).format("HH:mm");
    end = moment(end, ["h:mm A"]).format("HH:mm");
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    return (hours < 9 ? "0" : "") + hours + ":" + (minutes < 9 ? "0" : "") + minutes;
}

function loadSelectDataArray(selector,service,params,create){
    create = typeof create !== 'undefined' ? create : false;
    $(selector).each(function(){
        loadSelectData(this,service,params,create);
    });
}

function loadSelectData(selector,service,params,create){
	
    $.get( '/'+service, params).done(function(data){
		create = typeof create !== 'undefined' ? create : false;
        $(selector).find('option').remove().end().append('<option value=""></option>');
        $.each($.parseJSON(data),function(index,item){
            $(selector).append('<option value="' + item.id+ '">'+ item.text +'</option>');
        });
        if(create){
            $(selector).select2({placeholder: $(this).data("placeholder")});
        } else {
            $(selector).select2("val",'');
        }
    });
}

function loadSelectDataFromData(selector,data,create){
    create = typeof create !== 'undefined' ? create : false;
    $(selector).find('option').remove().end().append('<option value=""></option>');
    $.each($.parseJSON(data),function(index,item){
        $(selector).append('<option value="' + item.id+ '">'+ item.text +'</option>');
    });
    if(create){
        $(selector).select2({placeholder: $(this).data("placeholder")});
    } else {
        $(selector).select2("val",'');
    }
}


function loadSelectDataSet(selector,service,params,value){
    $.get( "/admin/services/"+service, params).done(function(data){
        $(selector).find('option').remove().end().append('<option value=""></option>');
        $.each($.parseJSON(data),function(index,item){
            $(selector).append('<option value="' + item.id+ '">'+ item.text +'</option>');
        });
        if(value){
            $(selector).select2("val",value);
        }
    });
}

function loadSelectDataSync(selector,service,params,create){
    create = typeof create !== 'undefined' ? create : false;
    $.ajax({
        type: "GET",
        url: "/admin/services/"+service,
        data: params,
        async: false,
        success: function (result) {
            if(create){
                $(selector).select2();
            }
            $(selector).select2("val",'');
            $(selector).find('option').remove().end().append('<option value=""></option>');
            $.each($.parseJSON(data),function(index,item){
                $(selector).append('<option value="' + item.id+ '">'+ item.text +'</option>');
            });
        }
    });
}

function loadMultipleSelectData(selector,service,params){
	selector.val("");
    selector.select2({
        multiple: true,
        ajax: {
            dataType: "json",
            url: "/admin/services/"+service,
            data: function (term, page) {
                return params;
            },
            results: function (data) {
                var results = [];
                $.each(data, function(index, val){
                    results.push({
                        id: val.id,
                        text: val.text
                    });
                });
                return {
                    results: results
                };
            }
        }
    });
}


function resetSelect(selector){
    $(selector).find('option').remove().end().append('<option value=""></option>');
}

var ProductType = {
	ROOM: {value : 1 , name : "Room"},
	MENU: {value : 2 , name : "Menu"},
	EXTRA: {value : 3 , name : "Extra"},
}

var PaymentMethod = {
    CASH : {value : 1 , name : "Cash"},
    MANUALPOS : {value : 2 , name : "Manual - POS"},
    WEBCC : {value : 3 , name : "Web - CC"},
    WEBPAYPAL : {value : 4 , name : "Web - Paypal"},
    MANUALPAYPAL : {value : 5 , name : "Manual - Paypal"},
    BANKTRANSFER : {value : 6 , name : "Bank Transfer"},
    NA : {value : 7 , name : "n/a"}
}

var PromoType = {
    CODE : {value : 1 , name : "Code"},
    NEWPRICING : {value : 2 ,name :  "New Pricing"},
    FREEADDON : {value : 3 , name : "Free Add On"},
    FIXEDDISCOUNT : {value : 4 ,name :  "Fixed Discount"},
    PERCENTDISCOUNT : {value : 5 ,name :  "% Discount"}
}

function removeArrayElement(element, array){
    array = $.grep(array, function(value) {
        return value != element;
    });
    return array;
}

function cleanArray(array){
    var uniqueArray = [];
    $.each(array, function(i, el){
        if($.inArray(el, uniqueArray) === -1) uniqueArray.push(el);
    });
    return uniqueArray;
}

function removeEmptyArray(actual){
    var newArray = new Array();
    for(var i = 0; i<actual.length; i++){
        if (actual[i]){
            newArray.push(actual[i]);
        }
    }
    return newArray;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

var EcoClipboard;

EcoClipboard = new ((function () {
    function _Class() {
        this.value = "";
        $(document).keydown((function (_this) {
            return function (e) {
                var _ref, _ref1;
                if (!_this.value || !(e.ctrlKey || e.metaKey)) {
                    return;
                }
                if ($(e.target).is("input:visible,textarea:visible")) {
                    return;
                }
                if (typeof window.getSelection === "function" ? (_ref = window.getSelection()) != null ? _ref.toString() : void 0 : void 0) {
                    return;
                }
                if ((_ref1 = document.selection) != null ? _ref1.createRange().text : void 0) {
                    return;
                }
                return $.Deferred(function () {
                    var $clipboardContainer;
                    $clipboardContainer = $("#clipboard-container");
                    $clipboardContainer.empty().show();
                    return $("<textarea id='clipboard'></textarea>").val(_this.value).appendTo($clipboardContainer).focus().select();
                });
            };
        })(this));
        $(document).keyup(function (e) {
            if ($(e.target).is("#clipboard")) {
                return $("#clipboard-container").empty().hide();
            }
        });
    }

    _Class.prototype.set = function (value) {
        this.value = value;
    };

    return _Class;

})());