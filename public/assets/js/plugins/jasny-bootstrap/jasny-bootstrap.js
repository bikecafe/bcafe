/* ===========================================================
 * Bootstrap: fileinput.js v3.1.3
 * http://jasny.github.com/bootstrap/javascript/#fileinput
 * ===========================================================
 * Copyright 2012-2014 Arnold Daniels
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */

+function ($) { "use strict";

  var isIE = window.navigator.appName == 'Microsoft Internet Explorer'

  // FILEUPLOAD PUBLIC CLASS DEFINITION
  // =================================

  var FileinputEco = function (element, options) {
    this.$element = $(element)
    
    this.$input = this.$element.find(':file')
    if (this.$input.length === 0) return

    this.name = this.$input.attr('name') || options.name

    this.$hidden = this.$element.find('input[type=hidden][name="' + this.name + '"]')
    if (this.$hidden.length === 0) {
      this.$hidden = $('<input type="hidden">').insertBefore(this.$input)
    }

    this.$preview = this.$element.find('.fileinput-eco-preview')
    var height = this.$preview.css('height')
    if (this.$preview.css('display') !== 'inline' && height !== '0px' && height !== 'none') {
      this.$preview.css('line-height', height)
    }
        
    this.original = {
      exists: this.$element.hasClass('fileinput-eco-exists'),
      preview: this.$preview.html(),
      hiddenVal: this.$hidden.val()
    }
    
    this.listen()
  }
  
  FileinputEco.prototype.listen = function() {
    this.$input.on('change.bs.fileinputeco', $.proxy(this.change, this))
    $(this.$input[0].form).on('reset.bs.fileinput', $.proxy(this.reset, this))
    
    this.$element.find('[data-trigger="fileinputeco"]').on('click.bs.fileinputeco', $.proxy(this.trigger, this))
    this.$element.find('[data-dismiss="fileinputeco"]').on('click.bs.fileinputeco', $.proxy(this.clear, this))
  },

  FileinputEco.prototype.change = function(e) {
    var files = e.target.files === undefined ? (e.target && e.target.value ? [{ name: e.target.value.replace(/^.+\\/, '')}] : []) : e.target.files
    
    e.stopPropagation()

    if (files.length === 0) {
      this.clear()
      return
    }

    this.$hidden.val('')
    this.$hidden.attr('name', '')
    this.$input.attr('name', this.name)

    var file = files[0]

    if (this.$preview.length > 0 && (typeof file.type !== "undefined" ? file.type.match(/^image\/(gif|png|jpeg)$/) : file.name.match(/\.(gif|png|jpe?g)$/i)) && typeof FileReader !== "undefined") {
      var reader = new FileReader()
      var preview = this.$preview
      var element = this.$element

      reader.onload = function(re) {
        var $img = $('<img>')
        $img[0].src = re.target.result
        files[0].result = re.target.result
        
        element.find('.fileinput-eco-filename').text(file.name)
        
        // if parent has max-height, using `(max-)height: 100%` on child doesn't take padding and border into account
        if (preview.css('max-height') != 'none') $img.css('max-height', parseInt(preview.css('max-height'), 10) - parseInt(preview.css('padding-top'), 10) - parseInt(preview.css('padding-bottom'), 10)  - parseInt(preview.css('border-top'), 10) - parseInt(preview.css('border-bottom'), 10))
        
        preview.html($img)
        element.addClass('fileinput-eco-exists').removeClass('fileinput-eco-new')

        element.trigger('change.bs.fileinputeco', files)
      }

      reader.readAsDataURL(file)
    } else {
      this.$element.find('.fileinput-eco-filename').text(file.name)
      this.$preview.text(file.name)
      
      this.$element.addClass('fileinput-eco-exists').removeClass('fileinput-eco-new')
      
      this.$element.trigger('change.bs.fileinputeco')
    }
  },

  FileinputEco.prototype.clear = function(e) {
    if (e) e.preventDefault()
    
    this.$hidden.val('')
    this.$hidden.attr('name', this.name)
    this.$input.attr('name', '')

    //ie8+ doesn't support changing the value of input with type=file so clone instead
    if (isIE) { 
      var inputClone = this.$input.clone(true);
      this.$input.after(inputClone);
      this.$input.remove();
      this.$input = inputClone;
    } else {
      this.$input.val('')
    }

    this.$preview.html('')
    this.$element.find('.fileinput-eco-filename').text('')
    this.$element.addClass('fileinput-eco-new').removeClass('fileinput-eco-exists')
    
    if (e !== undefined) {
      this.$input.trigger('change')
      this.$element.trigger('clear.bs.fileinputeco')
    }
  },

  FileinputEco.prototype.reset = function() {
    this.clear()

    this.$hidden.val(this.original.hiddenVal)
    this.$preview.html(this.original.preview)
    this.$element.find('.fileinput-eco-filename').text('')

    if (this.original.exists) this.$element.addClass('fileinput-eco-exists').removeClass('fileinput-eco-new')
     else this.$element.addClass('fileinput-eco-new').removeClass('fileinput-eco-exists')
    
    this.$element.trigger('reset.bs.fileinput')
  },

  FileinputEco.prototype.trigger = function(e) {
    this.$input.trigger('click')
    e.preventDefault()
  }

  
  // FILEUPLOAD PLUGIN DEFINITION
  // ===========================

  var old = $.fn.fileinputeco
  
  $.fn.fileinputeco = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('bs.fileinputeco')
      if (!data) $this.data('bs.fileinputeco', (data = new FileinputEco(this, options)))
      if (typeof options == 'string') data[options]()
    })
  }

  $.fn.fileinputeco.Constructor = FileinputEco


  // FILEINPUT NO CONFLICT
  // ====================

  $.fn.fileinputeco.noConflict = function () {
    $.fn.fileinputeco = old
    return this
  }


  // FILEUPLOAD DATA-API
  // ==================

  $(document).on('click.fileinputeco.data-api', '[data-provides="fileinputeco"]', function (e) {
    var $this = $(this)
    if ($this.data('bs.fileinputeco')) return
    $this.fileinputeco($this.data())
      
    var $target = $(e.target).closest('[data-dismiss="fileinputeco"],[data-trigger="fileinputeco"]');
    if ($target.length > 0) {
      e.preventDefault()
      $target.trigger('click.bs.fileinputeco')
    }
  })

}(window.jQuery);