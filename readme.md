## Setup Instructions

**Requirements**

- XAMPP 3.2.1 or later

- Composer

- Git 1.9.5

**Setup**

1. Run XAMPP Control Panel and start Apache and MySQL servers

2. Create a blank database named 'bcafe' in http://localhost/phpmyadmin

3. Download this repository to your machine by going to `C:\xampp\htdocs\` then right-click > `Git Bash Here` and type:
```
git clone https://[username]@bitbucket.org/bikecafe/bcafe.git
```
Note: Change [username] to your Bitbucket username

4. Download the database structure by typing:
```
    composer install
    
    php artisan migrate
```