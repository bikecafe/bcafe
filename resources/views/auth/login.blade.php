<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Route 88 Bike Cafe Admin Panel</title>

	<meta name="description" content="">

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="/assets/css/bootstrap/bootstrap.css" />

    <!-- Fonts  -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>

    <!-- Base Styling  -->
    <link rel="stylesheet" href="/assets/css/app/app.v1.css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        body {
            display: table;
            position: absolute;
            height: 100%;
            width: 100%;
        }

        .container {
            display: table-cell;
            vertical-align: middle;
        }
    </style>

</head>
<body>


    <div class="container login-container">
    	<div class="row login-row">
    	<div class="col-lg-4 login">
        	<h3 class="text-center"><img src="/assets/images/bcafe/logo.png"/ width="200"></h3>
            <hr class="clean">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form role="form" role="form" method="POST" action="/auth/login">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group input-group">
              	<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control"  placeholder="Username" name="username" value="{{ old('email') }}">
              </div>
              <div class="form-group input-group">
              	<span class="input-group-addon"><i class="fa fa-key"></i></span>
                <input type="password" class="form-control"  placeholder="Password" name="password">
              </div>
        	  <button type="submit" class="btn btn-dblue btn-block">Sign in</button>
            </form>
        </div>
        </div>
    </div>



    <!-- JQuery v1.9.1 -->
	<script src="/assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="/assets/js/plugins/underscore/underscore-min.js"></script>
    <!-- Bootstrap -->
    <script src="/assets/js/bootstrap/bootstrap.min.js"></script>

    <!-- Globalize -->
    <script src="/assets/js/globalize/globalize.min.js"></script>

    <!-- NanoScroll -->
    <script src="/assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>

</body>
</html>