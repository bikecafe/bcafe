@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Category <small>{{ ucwords($mode) }}</small></h1></div>
<div class="row">
    <div class="col-md-6">
        @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        @if($errors->any())
        <div class="validation-summary-errors alert alert-danger">
            <ul>
                {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
            </ul>
        </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">{{ ucwords($mode) }} Category</div>
            <div class="panel-body">
                <form enctype="multipart/form-data" action="@if (isset($s)){{ URL::to('/categories/' . $category->id . '/edit') }}@endif" role="form" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="productname">Category Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Category Name" autocomplete="off"  value="{{ Input::old('name', isset($category) ? $category->name : null) }}">
                    </div>
                    <div class="form-group">
                        <label class="cr-styled">
                            <input type="hidden" class="disabled-flag-value" name="disabled_flag_value" value="{{ $enabled ? '0' : '1' }}">
                            <input type="checkbox" class="disabled-flag" name="is_hidden" {{ $enabled ? '' : 'checked' }}>
                            
                            <i class="fa"></i>
                        </label>
                        Hidden in Menu
                    </div>
                    <input type="submit" class="btn btn-dblue" value="Submit">
                    <a href="/categories" class="btn btn-warning">Cancel</a>
                    <?php if($mode == "edit"){ ?>
                    <a href="/categories/{{ $category->id }}/delete" class="btn btn-danger delete-item">Delete</a>
                    <?php }?>
                </form>

            </div>
        </div>
    </div>
</div>

@stop

@section('script')
<script>
     $(document).ready(function(){
        $(".delete-item").click(function(event){
                    event.preventDefault();
                    var link = $(this).attr("href");
                    swal({
                            title: "Are you sure you want to delete this?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#d9534f",
                            confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "Cancel",
                            closeOnConfirm: true,   closeOnCancel: true },
                        function(isConfirm){
                            if (isConfirm) {
                                  window.location.href = link;
                            }
                                      
                        }
                    );
                });
    });
</script>
@stop