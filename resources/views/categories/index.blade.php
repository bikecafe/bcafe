@extends('layouts.master')

@section('content')
<div class="page-header"><h1>{{ ucwords($mode) }} Categories<small> view</small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif
<?php if($mode == 'active'){ ?>
<div class="row">
	<div class="col-md-5">
		<h4><a class="operation-link" href="/categories/add">Add <i class="glyphicon glyphicon-plus-sign operation-icon"></i></a></h4>
	</div>
</div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">{{ ucwords($mode) }} Categories</div>
			<div class="panel-body">
			
				<table class="table table-bordered table-striped" id="basic-datatable">
					<thead>
					<tr>
						<th>ID</th>
						<th>Category Name</th>
						<th>In Menu</th>
						<th>Date Added</th>
						<th>Date Updated</th>
						<?php if($mode == 'active'){ ?>
							<th></th>
							<th></th>
						<?php } ?>
					</tr>
					</thead>
					<tbody>
					@foreach($categories as $category)
					<tr>
						<td>{{ $category->id }}</td>
						<td>{{ $category->name }}</td>
						<td class="text-center">{!! $category->getEnabled() !!}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($category->created_at)) }}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($category->updated_at)) }}</td>
						<?php if($mode == 'active'){ ?>
							<td style="width:50px;">
								<a href="/categories/{{ $category->id }}/products" class="btn btn-green btn-xs">Show Products</a>
							</td>
							<td style="width:30px;">
								<a href="/categories/{{ $category->id }}/edit" class="btn btn-warning btn-xs">Edit</a>
							</td>
						<?php } ?>
						<?php if($mode == 'deleted'){ ?>
							<td style="width:50px;">
								<form action="/categories/{{ $category->id }}/restore" method="post" class="restore-row">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="submit" class="btn btn-dblue btn-xs" value="Restore" />
								</form>
							</td>
						<?php } ?>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<?php if($mode == 'active'){ ?>
			<a href="/categories/deleted" class="btn btn-danger">View Deleted Categories</a>
			<a href="/categories/rank" class="btn btn-green">Manage Category Ranking</a>
		<?php } elseif($mode == 'deleted') {?>
			<a href="/categories" class="btn btn-green">View Active Categories</a>
		<?php }?>
    </div>
</div>

@stop

@section('script')
<script>
    $(document).ready(function(){

        var confirmText = "This category will be deleted";

        swlConfirm(confirmText);

        var confirmText2 = "This category will be restored";

        resConfirm(confirmText2);
    });
</script>
@stop