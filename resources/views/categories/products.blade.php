@extends('layouts.master')

@section('content')
<div class="page-header"><h1>{{$category->name}}<small> products</small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif
<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">Products</div>
			<div class="panel-body">
				Drag the Products and rank how they should appear on the website menu under the {{$category->name}} category.
			</div>
			<div class="panel-body sort-categories">
				@foreach($category_products as $product)
				  <div class="category-div" data-id="{{ $product->id }}"><i class="fa fa-sort category-sort-icon"></i>{{ $product->name }}</div>
				@endforeach
				
			</div>
		</div>
		<form action="@if (isset($user)){{ URL::to('/users/' . $user->id . '/edit') }}@endif" role="form" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" id="ranking" name="ranking" value=""/>
			<button type="submit" class="btn btn-dblue">Save Changes</button>
        	<a href="/categories" class="btn btn-danger">Cancel</a>
        </form>
    </div>
</div>

@stop

@section('script')
<script>
$(document).ready(function(){
	var ranking = [];
	$(".sort-categories").sortable({
		stop: function(event, ui) {
	        var data = [];
	        $(".category-div").each(function(){
				data.push($(this).attr('data-id'));
			});
			$("#ranking").val(data);
	    }
	});
	$(".category-div").each(function(){
		ranking.push($(this).attr('data-id'));
	});
	$("#ranking").val(ranking);
});
</script>
@stop
