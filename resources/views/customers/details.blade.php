@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Customer Account<small>view</small></h1></div>
<div class="row">
    <form enctype="multipart/form-data" action="" role="form" method="POST" id="neworder">
    <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        @if($errors->any())
        <div class="validation-summary-errors alert alert-danger">
            <ul>
                {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
            </ul>
        </div>
        @endif
    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Customer Details</div>
            <div class="panel-body">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">First Name</label>
                        <input type="text" class="form-control" value="{{ $customer->firstname }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">Last Name</label>
                        <input type="text" class="form-control" value="{{ $customer->lastname }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">Email Address</label>
                        <input type="text" class="form-control" value="{{ $customer->email }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">Username</label>
                        <input type="text" class="form-control" value="{{ $customer->username }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">Date Joined</label>
                        <input type="text" class="form-control" value="{{ $customer->created_at }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">Account Status</label>
                        <input type="text" class="form-control" value="{{ $customer->is_activated ? 'Activated' : 'Not Activated' }}" readOnly>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">Delivery Addresses</div>
            <div class="panel-body">
                <table class="table table-bordered table-striped" id="address-datatable">
                    <thead>
                    <tr>
                        <th>Primary</th>
                        <th>House No./Bldg</th>
                        <th>Street Name</th>
                        <th>Village/Brgy</th>
                        <th>City</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($customer->address as $address)
                        <tr>
                            <td style="text-align:center">{!! $address->isPrimary() !!}</td>
                            <td>{{ $address->house_bldg }}</td>
                            <td>{{ $address->street }}</td>
                            <td>{{ $address->village_brgy }}</td>
                            <td>{{ $address->city }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">Contact Numbers</div>
            <div class="panel-body">
                <table class="table table-bordered table-striped" id="contact-datatable">
                    <thead>
                    <tr>
                        <th>Primary</th>
                        <th>Number</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($customer->contact as $contact)
                        <tr>
                            <td style="text-align:center">{!! $contact->isPrimary() !!}</td>
                            <td>{{ $contact->contact_number }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    </form>
    <div class="col-md-12">
        <a href="/customers" class="btn btn-green">View all Customer Accounts</a>
    </div>
</div>

@stop

@section('script')
<script>
    $(document).ready(function(){
        var dt = $("#address-datatable").DataTable({
                    "searching": false,
                    "ordering": false,
                    "paging": false
                });

        var dt2 = $("#contact-datatable").DataTable({
                    "searching": false,
                    "ordering": false,
                    "paging": false
                });
    }
</script>
@stop