@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Customer Accounts<small> view</small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif

<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">Customers</div>
			<div class="panel-body">
			
				<table class="table table-bordered table-striped" id="basic-datatable">
					<thead>
					<tr>
						<th>ID</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Username</th>
						<th>E-mail Address</th>
						<th>Activated</th>
						<th>Date Joined</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					@foreach($customers as $customer)
					<tr>
						<td>{{ $customer->id }}</td>
						<td>{{ $customer->firstname }}</td>
						<td>{{ $customer->lastname }}</td>
						<td>{{ $customer->username }}</td>
						<td>{{ $customer->email }}</td>
						<td class="text-center">{!! $customer->getEnabled() !!}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($customer->created_at)) }}</td>
						<td><a href="/customers/{{ $customer->id }}/view" class="btn btn-warning btn-xs">View</a></td>
						
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		
    </div>
</div>

@stop
