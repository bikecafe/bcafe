@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Ingredients<small> view {{ $mode }} </small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif
<div class="row">
	<div class="col-md-5">
		<h4><a class="operation-link" href="/inventory/ingredients/add">Add <i class="glyphicon glyphicon-plus-sign operation-icon"></i></a></h4>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">Ingredients</div>
			<div class="panel-body">
			
				<table class="table table-bordered table-striped" id="basic-datatable">
					<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Supplier</th>
						<th>Brand</th>
						<th style="width:100px">Stock Level</th>
						<th style="width:100px">Current Quantity</th>
						<th>Unit</th>
						@if($mode == 'active')
							<th></th>
						@endif
					</tr>
					</thead>
					<tbody>
					@foreach($ingredients as $ingredient)
					<tr>
						<td>{{ $ingredient->id }}</td>
						<td>{{ $ingredient->name }}</td>
						<td>{{ $ingredient->supplier }}</td>
						<td>{{ $ingredient->brand }}</td>
						<td>{!! $ingredient->eoq_level() !!}</td>
						<td style="text-align:right">{{ $ingredient->quantity }}</td>
						<td>{{ $ingredient->unit }}</td>
						@if($mode == 'active')
						<td style="width:30px;">
							<a href="/inventory/ingredients/{{ $ingredient->id }}/edit" class="btn btn-warning btn-xs">Edit</a>
						</td>
						@endif
						@if($mode == 'deleted')
						<td style="width:50px;">
							<form action="/inventory/ingredients/{{ $ingredient->id }}/restore" method="post" class="restore-row">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="submit" class="btn btn-dblue btn-xs" value="Restore" />
							</form>
						</td>
						@endif
					</tr>
					@endforeach
					</tbody>
				</table>

			</div>
		</div>
		<?php if($mode == 'active'){ ?>
			<a href="/inventory/ingredients/deleted" class="btn btn-danger">View Deleted Ingredients</a>
		<?php } elseif($mode == 'deleted') {?>
			<a href="/inventory/ingredients" class="btn btn-green">View Active Ingredients</a>
		<?php }?>
    </div>
</div>

@stop

@section('script')
<script>
    $(document).ready(function(){

        var confirmText = "This ingredient will be deleted";

        swlConfirm(confirmText);

        var confirmText2 = "This ingredient will be restored";

        resConfirm(confirmText2);
    });
</script>
@stop