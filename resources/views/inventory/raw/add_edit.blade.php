@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Raw Materials <small>{{ ucwords($mode) }}</small></h1></div>
<div class="row">
    <div class="col-md-6">
        @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        @if($errors->any())
        <div class="validation-summary-errors alert alert-danger">
            <ul>
                {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
            </ul>
        </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">{{ ucwords($mode) }} Raw Material</div>
            <div class="panel-body">
                <form action="@if (isset($raw)){{ URL::to('/inventory/raw/' . $raw->id . '/edit') }}@endif" role="form" method="POST">
                    <div class="col-lg-12"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name" autocomplete="off" value="{{ Input::old('name', isset($raw) ? $raw->name : null) }}">
                        </div>
                        <div class="form-group">
                            <label for="supplier">Supplier</label>
                            <input type="text" class="form-control" name="supplier" id="supplier" placeholder="Supplier" autocomplete="off" value="{{ Input::old('supplier', isset($raw) ? $raw->supplier : null) }}">
                        </div>
                        <div class="form-group">
                            <label for="brand">Brand</label>
                            <input type="text" class="form-control" name="brand" id="brand" placeholder="Brand" autocomplete="off" value="{{ Input::old('brand', isset($raw) ? $raw->brand : null) }}">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="quantity">Current Quantity</label>
                            <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity" autocomplete="off" value="{{ Input::old('quantity', isset($raw) ? $raw->quantity : null) }}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="unit">Unit</label>
                            <select name="unit" class="form-control select2">
                                <option value="g" {{ Input::old('unit', isset($raw) ? $raw->unit : null) == "g" ? 'selected' : '' }}>g</option>
                                <option value="ml" {{ Input::old('unit', isset($raw) ? $raw->unit : null) == "ml" ? 'selected' : '' }}>ml</option>
                                <option value="L" {{ Input::old('unit', isset($raw) ? $raw->unit : null) == "L" ? 'selected' : '' }}>L</option>
                                <option value="pcs" {{ Input::old('unit', isset($raw) ? $raw->unit : null) == "pcs" ? 'selected' : '' }}>pcs</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="unit">Minimum Quantity</label>
                            <input type="text" class="form-control decimal" name="min_normal" placeholder="Min" autocomplete="off" value="{{ Input::old('min_normal', isset($raw) ? $raw->min_normal : null) }}">
                        </div>
                        
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="unit">Maximum Quantity</label>
                            <input type="text" class="form-control decimal" name="max_normal" placeholder="Max" autocomplete="off" value="{{ Input::old('max_normal', isset($raw) ? $raw->max_normal : null) }}">
                        </div>
                        
                    </div>
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-dblue">Submit</button>
                        <a href="/inventory/raw" class="btn btn-warning">Cancel</a>
                        <?php if($mode == "edit"){ ?>
                        <a href="/inventory/raw/{{ $raw->id }}/delete" class="btn btn-danger delete-item">Delete</a>
                        <?php }?>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        
        <div class="panel panel-default">
            <div class="panel-heading">AUDIT TRAIL</div>
            <div class="panel-body">
            
                <table class="table table-bordered table-striped" id="audit-datatable">
                    <thead>
                    <tr>
                        <th>User</th>
                        <th>Quantity</th>
                        <th>Updated Quantity</th>
                        <th>New Quantity</th>
                        <th>Date Updated</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($history as $audit_trail)
                    <tr>
                        <td>{{ $audit_trail->firstname . ' ' .$audit_trail->lastname }}</td>
                        <td>{{ $audit_trail->old_value }}</td>
                        <td>{{ $audit_trail->new_value > $audit_trail->old_value ? '+'.($audit_trail->new_value-$audit_trail->old_value) : ($audit_trail->new_value < $audit_trail->old_value ? '-'.($audit_trail->old_value-$audit_trail->new_value) : '+0') }}</td>
                        <td>{{ $audit_trail->new_value }}</td>
                        <td>{{ date("F j, Y, g:i a",strtotime($audit_trail->created_at)) }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@stop

@section('script')
<script>
     $(document).ready(function(){
        $(".delete-item").click(function(event){
                    event.preventDefault();
                    var link = $(this).attr("href");
                    swal({
                            title: "Are you sure you want to delete this?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#d9534f",
                            confirmButtonText: "Yes, delete it!",
                            cancelButtonText: "Cancel",
                            closeOnConfirm: true,   closeOnCancel: true },
                        function(isConfirm){
                            if (isConfirm) {
                                  window.location.href = link;
                            }
                        }
                    );
                });
        $("#audit-datatable").DataTable( {
        "order": [[ 4, "desc" ]]
    } );
    });
</script>
@stop