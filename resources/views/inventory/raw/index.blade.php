@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Raw Materials<small> view {{ $mode }} </small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif
<div class="row">
	<div class="col-md-5">
		<h4><a class="operation-link" href="/inventory/raw/add">Add <i class="glyphicon glyphicon-plus-sign operation-icon"></i></a></h4>
	</div>
</div>
<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">Raw Materials</div>
			<div class="panel-body">
			
				<table class="table table-bordered table-striped" id="basic-datatable">
					<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Supplier</th>
						<th>Brand</th>
						<th style="width:100px">Stock Level</th>
						<th style="width:100px">Current Quantity</th>
						<th>Unit</th>
						@if($mode == 'active')
							<th></th>
						@endif
					</tr>
					</thead>
					<tbody>
					@foreach($rawMaterials as $raw)
					<tr>
						<td>{{ $raw->id }}</td>
						<td>{{ $raw->name }}</td>
						<td>{{ $raw->supplier }}</td>
						<td>{{ $raw->brand }}</td>
						<td>{!! $raw->eoq_level() !!}</td>
						<td style="text-align:right">{{ $raw->quantity }}</td>
						<td>{{ $raw->unit }}</td>
						@if($mode == 'active')
						<td style="width:30px;">
							<a href="/inventory/raw/{{ $raw->id }}/edit" class="btn btn-warning btn-xs">Edit</a>
						</td>
						@endif
						@if($mode == 'deleted')
						<td style="width:50px;">
							<form action="/inventory/raw/{{ $raw->id }}/restore" method="post" class="restore-row">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="submit" class="btn btn-dblue btn-xs" value="Restore" />
							</form>
						</td>
						@endif
					</tr>
					@endforeach
					</tbody>
				</table>

			</div>
		</div>
		<?php if($mode == 'active'){ ?>
			<a href="/inventory/raw/deleted" class="btn btn-danger">View Deleted Raw Materials</a>
		<?php } elseif($mode == 'deleted') {?>
			<a href="/inventory/raw" class="btn btn-green">View Active Raw Materials</a>
		<?php }?>
    </div>
</div>

@stop

@section('script')
<script>
    $(document).ready(function(){

        var confirmText = "This raw material will be deleted";

        swlConfirm(confirmText);

        var confirmText2 = "This raw material will be restored";

        resConfirm(confirmText2);
    });
</script>
@stop