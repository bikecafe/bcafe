<!DOCTYPE html>
<html lang="en">

<head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @if(Auth::user()->user_type_id == 2)
	<title>Route 88 Bike Cafe | Inventory Manager Panel</title>
    @endif

    @if(Auth::user()->user_type_id == 3)
    <title>Route 88 Bike Cafe | Cashier Panel</title>
    @endif

    @if(Auth::user()->user_type_id == 1)
    <title>Route 88 Bike Cafe | Admin Panel</title>
    @endif

	<meta name="description" content="">
    <link rel="icon" type="image/x-icon" href="/assets/images/logo.ico" />
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="/assets/css/bootstrap/bootstrap.css" />

    <!-- Bootstrap jasny CSS -->
    <link rel="stylesheet" href="/assets/css/plugins/jasny-bootstrap/jasny-bootstrap.css"/>

    <!-- Bootstrap fileinput CSS -->
    <link rel="stylesheet" href="/assets/css/plugins/fileinput/fileinput.css" />

    <!-- Bootstrap multiselect CSS -->
    <link rel="stylesheet" href="/assets/css/plugins/bootstrap-multiselect/bootstrap-multiselect.css" />

    <!-- Bootstrap datepicker CSS -->
    <link rel="stylesheet" href="/assets/css/plugins/bootstrap-datepicker/bootstrap-datepicker.css" />

    <!-- Bootstrap select2 CSS -->
    <link rel="stylesheet" href="/assets/css/plugins/bootstrap-select2/select2.css" />

    <!-- Bootstrap select2-bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/plugins/bootstrap-select2/select2-bootstrap.css" />

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="/assets/css/plugins/sweet-alert/sweetalert2.css" />


    <!-- Calendar Styling  -->
    <link rel="stylesheet" href="/assets/css/plugins/calendar/calendar.css" />

	<!-- Typeahead Styling  -->
    <link rel="stylesheet" href="/assets/css/plugins/typeahead/typeahead.css" />

    <!-- TagsInput Styling  -->
    <link rel="stylesheet" href="/assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" />

    <!-- DateTime Picker  -->
    <link rel="stylesheet" href="/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css" />

    <!-- Switch Buttons  -->
    <link rel="stylesheet" href="/assets/css/switch-buttons/switch-buttons.css" />

	<!-- datatables Styling  -->
    <link rel="stylesheet" href="/assets/css/plugins/datatables/jquery.dataTables.css" />

    <!-- Fonts  -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>

    <!-- Base Styling  -->
    <link rel="stylesheet" href="/assets/css/app/app.v1.css?v=11" />

	<!-- Froala Wysiwyg Editor Styling -->
	<link href="/assets/css/plugins/froala-wysiwyg/froala_editor.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/plugins/froala-wysiwyg/froala_style.min.css" rel="stylesheet" type="text/css">
	
	<!-- Starrr Plugin -->
	<link href="/assets/css/plugins/bootstrap-starrr/starrr.min.css" rel="stylesheet" type="text/css">
	
	<!-- Editable Plugin -->
	<link href="/assets/css/plugins/bootstrap-editable/bootstrap-editable.css" rel="stylesheet" type="text/css">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>


	<aside class="left-panel">

            <div class="user text-center">
                  <h4 class="user-name">Welcome, {{ Auth::user()->firstname }}!</h4>
                  <h5 class="user-name"><b>[{{ strtolower(Auth::user()->type->name) }}]</b></h5>

                <div class="dropdown user-login">
                    <a class="btn btn-xs dropdown-toggle btn-rounded" href="/auth/logout">
                        <i class="fa fa-circle status-icon signout"></i> Signout
                    </a>
                </div>
            </div>
            <!--Inventory Manager-->
            @if(Auth::user()->user_type_id == 2)
            <nav class="navigation">
                <ul class="list-unstyled">
                    <li class="{{ Request::is('products*') ? 'active' : '' }}"><a href="/products"><i class="fa fa-bars"></i> <span class="nav-label">Products</span></a></li>
                    <li class="{{ Request::is('categories*') ? 'active' : '' }}"><a href="/categories"><i class="fa fa-archive"></i> <span class="nav-label">Categories</span></a></li>
                    <li class="has-submenu {{ Request::is('inventory*') ? 'active' : '' }}"><a href="#"><i class="fa fa-database"></i> <span class="nav-label">Inventory</span></a>
                        <ul class="list-unstyled">
                            <li class="{{ Request::is('inventory/raw*') ? 'active' : '' }}"><a href="/inventory/raw">Raw Materials</a></li>
                            <li class="{{ Request::is('inventory/ingredients*') || Request::is('inventory/ingredients/*') ? 'active' : '' }}"><a href="/inventory/ingredients">Ingredients</a></li>
                        </ul>
                    </li>
                    <li class="has-submenu {{ Request::is('reports*') ? 'active' : '' }}"><a href="#"><i class="fa fa-bar-chart"></i> <span class="nav-label">Reports</span></a>
                        <ul class="list-unstyled">
                            <li class="{{ Request::is('reports/inventory') || Request::is('reports/inventory/*') ? 'active' : '' }}"><a href="/reports/inventory">Status of Stocks</a></li>
                            <li class="{{ Request::is('reports/sales') || Request::is('reports/sales/*') ? 'active' : '' }}"><a href="/reports/sales">Sales Report</a></li>
                            <li class="{{ Request::is('reports/demand') || Request::is('reports/demand/*') ? 'active' : '' }}"><a href="/reports/demand">Product Demand</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            @endif

            <!--Cashier-->
            @if(Auth::user()->user_type_id == 3)
            <nav class="navigation">
                <ul class="list-unstyled">
                    <li class="has-submenu {{ Request::is('orders*') ? 'active' : '' }}"><a href="#"><i class="fa fa-book"></i> <span class="nav-label">Orders</span></a>
                        <ul class="list-unstyled">
                            <li class="{{ Request::is('orders') || Request::is('orders/add*') || Request::is('orders/*/manage')  ? 'active' : '' }}"><a href="/orders">Active Orders</a></li>
                            <li class="{{ Request::is('orders/all/onsite*') ? 'active' : '' }}"><a href="/orders/all/onsite">Onsite Orders</a></li>
                            <li class="{{ Request::is('orders/all/online*') ? 'active' : '' }}"><a href="/orders/all/online">Online Orders</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            @endif

            <!--Admin-->
            @if(Auth::user()->user_type_id == 1)
            <nav class="navigation">
            	<ul class="list-unstyled">
                    <li class="{{ Request::is('products*') ? 'active' : '' }}"><a href="/products"><i class="fa fa-bars"></i> <span class="nav-label">Products</span></a></li>
                    <li class="{{ Request::is('categories*') ? 'active' : '' }}"><a href="/categories"><i class="fa fa-archive"></i> <span class="nav-label">Categories</span></a></li>
                    <li class="{{ Request::is('orders*') ? 'active' : '' }}"><a href="/orders"><i class="fa fa-archive"></i> <span class="nav-label">Orders</span></a></li>
                    <!--<li class="has-submenu {{ Request::is('orders*') ? 'active' : '' }}"><a href="#"><i class="fa fa-book"></i> <span class="nav-label">Orders</span></a>
                        <ul class="list-unstyled">
                            <li class="{{ Request::is('orders') || Request::is('orders/add*') || Request::is('orders/*/manage') || Request::is('orders/all*') ? 'active' : '' }}"><a href="/orders">Orders</a></li>
                            <li class="{{ Request::is('orders/status') || Request::is('orders/status/*') ? 'active' : '' }}"><a href="/orders/status">Manage Order Statuses</a></li>
                        </ul>
                    </li>-->
                    <li class="has-submenu {{ Request::is('inventory*') ? 'active' : '' }}"><a href="#"><i class="fa fa-database"></i> <span class="nav-label">Inventory</span></a>
                        <ul class="list-unstyled">
                            <li class="{{ Request::is('inventory/raw*') ? 'active' : '' }}"><a href="/inventory/raw">Raw Materials</a></li>
                            <li class="{{ Request::is('inventory/ingredients*') || Request::is('inventory/ingredients/*') ? 'active' : '' }}"><a href="/inventory/ingredients">Ingredients</a></li>
                        </ul>
                    </li>
					<li class="has-submenu {{ Request::is('reports*') ? 'active' : '' }}"><a href="#"><i class="fa fa-bar-chart"></i> <span class="nav-label">Reports</span></a>
                        <ul class="list-unstyled">
                            <li class="{{ Request::is('reports/inventory') || Request::is('reports/inventory/*') ? 'active' : '' }}"><a href="/reports/inventory">Status of Stocks</a></li>
                            <li class="{{ Request::is('reports/sales') || Request::is('reports/sales/*') ? 'active' : '' }}"><a href="/reports/sales">Sales Report</a></li>
                            <li class="{{ Request::is('reports/demand') || Request::is('reports/demand/*') ? 'active' : '' }}"><a href="/reports/demand">Product Demand</a></li>
                        </ul>
                    </li>
					<li class="has-submenu {{ Request::is('users*') || Request::is('customers*') || Request::is('settings*')? 'active' : '' }}"><a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Other Settings</span></a>
                    	<ul class="list-unstyled">
                        	<li class="{{ Request::is('users') || Request::is('users/*/edit') || Request::is('users/add')? 'active' : '' }}"><a href="/users">Users</a></li>
                            <!--<li class="{{ Request::is('users/types') ? 'active' : '' }}"><a href="/users/types">User Types</a></li>-->
                            <li class="{{ Request::is('customers*') ? 'active' : '' }}"><a href="/customers">Customer Accounts</a></li>
                            <li class="{{ Request::is('settings*') ? 'active' : '' }}"><a href="/settings">General Settings</a></li>
					   </ul>
                    </li>

                </ul>
            </nav>
            @endif
    </aside>
    <!-- Aside Ends-->

    <section class="content">

        <header class="top-head container-fluid">
            <button type="button" class="navbar-toggle pull-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </header>
        <!-- Header Ends -->


        <div class="warper container-fluid">
			@yield('content')
        </div>
        <!-- Warper Ends Here (working area) -->


    </section>
    <!-- Content Block Ends Here (right box)-->


    <!-- JQuery v1.11.2 -->
	<script src="/assets/js/jquery/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery/jquery-ui.js" type="text/javascript"></script>
    <script src="/assets/js/plugins/underscore/underscore-min.js"></script>
    <!-- Bootstrap -->
    <script src="/assets/js/bootstrap/bootstrap.min.js"></script>

    <!-- Bootstrap jasny -->
    <script src="/assets/js/plugins/jasny-bootstrap/jasny-bootstrap.js"></script>

    <!-- Bootstrap fileinput -->
    <script src="/assets/js/plugins/fileinput/fileinput.min.js"></script>

    <!-- Bootstrap multiselect -->
    <script src="/assets/js/plugins/bootstrap-multiselect/bootstrap-multiselect.js"></script>

    <!-- Bootstrap datepicker -->
    <script src="/assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>

    <!-- Bootstrap select2 -->
    <script src="/assets/js/plugins/bootstrap-select2/select2.min.js"></script>

    <!-- Sweet Alert -->
    <script src="/assets/js/plugins/sweet-alert/sweetalert2.min.js"></script>
    
    <!-- Globalize -->
    <script src="/assets/js/globalize/globalize.min.js"></script>
    
    <!-- NanoScroll -->
    <script src="/assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
    
    <!-- Chart JS -->
    <script src="/assets/js/plugins/DevExpressChartJS/dx.chartjs.js"></script>
    <script src="/assets/js/plugins/DevExpressChartJS/world.js"></script>
   	<!-- For Demo Charts -->
    <script src="/assets/js/plugins/DevExpressChartJS/demo-charts.js"></script>
    <script src="/assets/js/plugins/DevExpressChartJS/demo-vectorMap.js"></script>
    
    <!-- Sparkline JS -->
    <script src="/assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- For Demo Sparkline -->
    <script src="/assets/js/plugins/sparkline/jquery.sparkline.demo.js"></script>
    
    <!-- Angular JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.14/angular.min.js"></script>
    <!-- ToDo List Plugin -->
    
    
    <!-- Calendar JS -->
    <script src="/assets/js/plugins/calendar/calendar.js"></script>
    <!-- Calendar Conf -->
	
    <!-- NiceScroll -->
    <script src="/assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
    
	<!-- TypeaHead -->
    <script src="/assets/js/plugins/typehead/typeahead.bundle.js"></script>
    <script src="/assets/js/plugins/typehead/typeahead.bundle-conf.js"></script>
    
    <!-- InputMask -->
    <script src="/assets/js/plugins/inputmask/jquery.inputmask.bundle.js"></script>
    
    <!-- TagsInput -->
    <script src="/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
    
    <!-- moment -->
    <script src="/assets/js/moment/moment.js"></script>
    
    <!-- DateTime Picker -->
    <script src="/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>

    <!-- HandleBarsJs -->
    <script src="/assets/js/plugins/handlebars/handlebars-v3.0.0.js"></script>
    
    <!-- Custom JQuery -->
	<script src="/assets/js/app/custom.js?v=6" type="text/javascript"></script>
    
	<!-- Sparkline JS -->
    <script src="/assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

	<!-- Data Table -->
    <script src="/assets/js/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/assets/js/plugins/datatables/DT_bootstrap.js"></script>
    <script src="/assets/js/plugins/datatables/jquery.dataTables-conf.js"></script>

	<!-- Froala Wysiwyg -->
	<script src="/assets/js/plugins/froala-wysiwyg/froala_editor.min.js"></script>
	<!--[if lt IE 9]>
	<script src="/assets/js/plugins/froala-wysiwyg/froala_editor_ie8.min.js"></script>
	<![endif]-->
	
	<!-- Froala Wysiwyg Plugins -->
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/block_styles.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/colors.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/char_counter.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/file_upload.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/font_family.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/font_size.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/fullscreen.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/lists.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/inline_styles.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/media_manager.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/tables.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/urls.min.js"></script>
	<script src="/assets/js/plugins/froala-wysiwyg/plugins/video.min.js"></script>
	
	<!-- Starrr Plugin -->
	<script src="/assets/js/plugins/bootstrap-starrr/starrr.min.js"></script>
	
	<!-- Editable Plugin -->
	<script src="/assets/js/plugins/bootstrap-editable/bootstrap-editable.js"></script>

    <!-- JQuery Sortable -->
    <script src="/assets/js/jquery/jquery-ui-sortable.min.js"></script>
    

	<!--script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-56821827-1', 'auto');
        ga('send', 'pageview');
    
    </script-->

    @yield('script','')

</body>
</html>
