@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Onsite Order<small>add</small></h1></div>
<div class="row">
    <form enctype="multipart/form-data" action="" role="form" method="POST" id="neworder">
    <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        @if($errors->any())
        <div class="validation-summary-errors alert alert-danger">
            <ul>
                {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
            </ul>
        </div>
        @endif
        <div class="panel panel-default product-panel">
            <div class="panel-heading">Add Product to Order</div>
            <div class="panel-body">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="sproduct">Product</label>
                        <select class='form-control productSearch' type='text' name="productSearch" placeholder='Select Product'>
                            <option></option>
                            @if (isset($products))
                                @foreach($products as $product)
                                <option value="{{ $product->id }}" data-price="{{ $product->price }}" data-name="{{ $product->name }}">{{ $product->name }} (Php {{ $product->price }})</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="sproduct">Stocks</label>
                        <input type="text" autocomplete="off" id="stocks" name="stocks" class="form-control integer" readOnly>
                        
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="sproduct">Order Quantity</label>
                        <div class="input-group spinner">
                            <input type="text" autocomplete="off" id="qty" name="qty" class="form-control integer" value="1" data-minval="0" data-maxval="2" readOnly>
                            <div class="input-group-btn-vertical">
                                <a class="btn btn-default"><i class="fa fa-caret-up"></i></a>
                                <a class="btn btn-default"><i class="fa fa-caret-down"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <label for="add" style="height:15px"></label>
                    <div class="form-group">
                        <button type="button" id="addproduct" class="btn btn-green">ADD</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Order Items</div>
            <div class="panel-body">
                <table class="table table-bordered table-striped" id="orders-datatable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Product Name</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Order Information</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="date">Date Ordered</label>
                    <input type="text" id="date-ordered" class="form-control" value="" readOnly>
                </div>
                <div class="form-group">
                    <label for="order-status">Status</label>
                    <select class="form-control select2" id="order-status" name="order-status">
                        @foreach ($orderStatuses as $orderStatus)
                            <option value="{{$orderStatus->id}}" >{{$orderStatus->name}}</option>
                        @endforeach
                    </select>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Details</div>
            <div class="panel-body">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="grandtotal">Grand Total</label>
                        <input type="hidden" name="grand-total">
                        <input type="text" id="grand-total" class="form-control" value="" readOnly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="grandtotal">Amount Tendered</label>
                        <input type="text" id="tendered" name="amount-tendered" class="form-control decimal" value="">
                        <span class="error-msg" style="color:#FF0000;display:none">Invalid amount</span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="grandtotal">Change</label>
                        <input type="text" id="change" class="form-control" value="" readOnly>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <input type="submit" id="add-order" class="btn btn-dblue" value="Submit">
        <a href="/orders" class="btn btn-danger">Cancel</a>
    </div>
    <input type="hidden" name="reference_no" id="reference-no" value="{{ time() }}">
    <input type="hidden" name="order-items" id="order-items" value="">
    </form>
</div>

@stop

@section('script')
<script>
    $(document).ready(function(){
        var catchExit = true;

        $(".productSearch").select2();

        $(".product-panel").on("change",".productSearch",function(){
            var product_id = $(this).find('option:selected').val();
            $.get( "/products/"+product_id+"/stocks/").done(function(data){
                $("#stocks").val(data);
                $("#qty").attr("data-maxval", data);
                if(data == 0){
                    $("#qty").val(0);
                    $("#qty").attr("data-minval", data);
                } else {
                    $("#qty").val(1);
                    $("#qty").attr("data-minval", "1");
                }
            });
            
        });

        function refreshStocks(){
            var product_id = $(".productSearch").find("option:selected").val();
            $.get( "/products/"+product_id+"/stocks/").done(function(data){
                console.log("data:"+data);
                $("#stocks").val(data);
                $("#qty").attr("data-maxval", data);
                if(data == 0){
                    $("#qty").val(0);
                    $("#qty").attr("data-minval", data);
                } else {
                    $("#qty").val(1);
                    $("#qty").attr("data-minval", "1");
                }
            });
        }

        var dt = $("#orders-datatable").DataTable({
                    "searching": false,
                    "ordering": false,
                    "paging": false
                });

        $('#addproduct').on( 'click', function () {
            var id = $('.productSearch').find('option:selected').val();
            var name = $('.productSearch').find('option:selected').attr("data-name");
            var price = $('.productSearch').find('option:selected').attr("data-price");
            var qty = $('#qty').val();
            var subtotal = $('#qty').val() * $('.productSearch').find('option:selected').attr("data-price");
            if(name != "" && name != undefined && price != "" && price != undefined && qty != 0){
                //lock product ingredients
                var reference_no = $("#reference-no").val();
                var token = $("[name=_token]").val();
                $.ajax({
                  method: "POST",
                  url: "/orders/ingredient/lock",
                  data: { _token: token, product_id: id, product_qty: qty, reference_no: reference_no, for_release : 1 },
                  error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.responseText);
                    console.log(thrownError);
                  }
                });

                //display product to table
                dt.row.add( [
                    id,
                    name,
                    price,
                    qty,
                    parseFloat(subtotal).toFixed(2),
                    '<a href="#" class="btn btn-danger btn-xs delete-product">X</a>'
                ] ).draw( false );
                $('.productSearch').select2("val","");
                $("#stocks").val("");
                $("#qty").val(1);
                updateGrandtotal();
                $("#tendered").val("");
                calculate_change();
            }
           
        });

        $(document).on('click', 'a.delete-product', function(event) {
            event.preventDefault();

            //unlock product ingredients
            var id = $(this).parent().parent('tr').find('td:nth-child(1)').html();
            var price = $(this).parent().parent('tr').find('td:nth-child(3)').html();
            var qty = $(this).parent().parent('tr').find('td:nth-child(4)').html();

            var reference_no = $("#reference-no").val();
            var token = $("[name=_token]").val();
            $.ajax({
              method: "POST",
              url: "/orders/ingredient/unlock",
              data: { _token: token, product_id: id, product_qty: qty, reference_no: reference_no },
              error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                console.log(thrownError);
              }
            }).done(function( data ){
                refreshStocks();
            });
            
            $(this).parent().parent('tr').addClass("delete-this");
            dt.row(".delete-this").remove().draw( false );
            updateGrandtotal();
            
        });

        function updateGrandtotal(){
            var grand_total = 0;
            $('#orders-datatable tbody tr td:nth-child(5)').each( function(){
                grand_total += parseFloat($(this).html());
            });
            grand_total = parseFloat(grand_total).toFixed(2);
            $("#grand-total").val("Php " + grand_total);
            $("input[name=grand-total]").val(grand_total);
            return grand_total;
        }

        function getDateTime(){
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if(h<10)
            {
                    h = "0"+h;
            }
            m = date.getMinutes();
            if(m<10)
            {
                    m = "0"+m;
            }
            s = date.getSeconds();
            if(s<10)
            {
                    s = "0"+s;
            }
            result = ''+days[day]+' - '+months[month]+' '+d+', '+year+' '+h+':'+m+':'+s;
            return result;

        }
        window.setInterval(function(){
          var datetimenow = getDateTime();
          $("#date-ordered").val(datetimenow);
        }, 1000);

        $("#add-order").click(function(event){
            event.preventDefault();
            catchExit = false;
            if(dt.data().length > 0){
                var grandtotal = updateGrandtotal();
                var amounttendered = $("#tendered").val();
                if(isNaN(parseFloat(amounttendered)) || parseFloat(amounttendered) == 0.00 || parseFloat(amounttendered) < parseFloat(grandtotal)){
                    $("#tendered").css("border","1px solid #FF0000");
                    $(".error-msg").show();
                } else {
                    var order_items = [];
                    $('#orders-datatable tbody tr').each( function(){
                        var id = $(this).find('td:nth-child(1)').html();
                        var price = $(this).find('td:nth-child(3)').html();
                        var qty = $(this).find('td:nth-child(4)').html();
                        var item = {};
                        item.product_id = id;
                        item.price = price;
                        item.quantity = qty;
                        order_items.push(item);
                    });
                    $("#order-items").val(JSON.stringify(order_items));
                    $("#neworder").submit();
                }

            }
        });

        $("#tendered").keyup(function(){
            $("#tendered").css("border","1px solid #ccc");
            $(".error-msg").hide();

            calculate_change();
        });

        function calculate_change(){
            var change = parseFloat($("#tendered").val() - $("input[name=grand-total]").val()).toFixed(2);
            change = change == 'NaN' ? '0.00' : change;
            $("#change").val(change);
        }
        calculate_change();

        //user is about to leave the page without saving changes
        window.onbeforeunload = confirmExit;
        function confirmExit(){ 
            console.log(catchExit);
            if(catchExit){
            //release locked ingredients that were not part of original order
            var reference_no = $("#reference-no").val();
            var token = $("[name=_token]").val();
            $.ajax({
              method: "POST",
              url: "/orders/ingredient/release",
              async : false,
              data: { _token: token, reference_no: reference_no },
              error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                console.log(thrownError);
              },
              success: function(data){
                console.log(data);
              }
            });
            
            } 
            // return "Hello";
        }
    });
</script>
@stop