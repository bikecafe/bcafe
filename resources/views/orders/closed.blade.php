@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Order<small>view</small></h1></div>
<div class="row">
    <form enctype="multipart/form-data" action="" role="form" method="POST" id="neworder">
    <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        @if($errors->any())
        <div class="validation-summary-errors alert alert-danger">
            <ul>
                {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
            </ul>
        </div>
        @endif
    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Order Items</div>
            <div class="panel-body">
                <table class="table table-bordered table-striped" id="orders-datatable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Product Name</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($order->orderlines as $orderline)
                        <tr>
                            <td>{{ $orderline->product->id }}</td>
                            <td>{{ $orderline->product->name }}</td>
                            <td>{{ $orderline->price }}</td>
                            <td>{{ $orderline->quantity }}</td>
                            <td>{{ number_format(floatval($orderline->price * $orderline->quantity),2) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if(isset($ordertype) && $ordertype == "online")
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Customer Information</div>
            <div class="panel-body">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">First Name</label>
                        <input type="text" class="form-control" value="{{ $order->customer->firstname }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">Last Name</label>
                        <input type="text" class="form-control" value="{{ $order->customer->lastname }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">Email Address</label>
                        <input type="text" class="form-control" value="{{ $order->customer->email }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="date">Address</label>
                        <input type="text" class="form-control" value="{{ $order->customer_address ? $order->customer_address->house_bldg . " " . $order->customer_address->street . " " . $order->customer_address->village_brgy . " " . $order->customer_address->city : 'N/A'}}" readOnly>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="date">Contact Number</label>
                        <input type="text" class="form-control" value="{{ $order->customer_contact->contact_number }}" readOnly>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Order Information</div>
            <div class="panel-body">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="date">Order Type</label>
                        <input type="text" class="form-control" value="{{ $order->type->name }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        @if(isset($ordertype) && $ordertype == "online")
                        <label for="date">Acquisition Type</label>
                        <input type="text" class="form-control" value="{{ ucwords($order->acquisition->name) }}" readOnly>
                        @endif
                        @if(isset($ordertype) && $ordertype == "onsite")
                        <label for="date">Tendered By</label>
                        <input type="text" class="form-control" value="{{ count($order->getFirstWorkflow()) > 0 ? $order->getFirstWorkflow()->user->firstname . ' ' . $order->getFirstWorkflow()->user->lastname : '' }}" readOnly>
                        @endif
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="date">Date Ordered</label>
                        <input type="text" class="form-control" value="{{ date( 'l - F d, Y H:i:s', strtotime($order->created_at) ) }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="date">Date Updated</label>
                        <input type="text" class="form-control" value="{{ date( 'l - F d, Y H:i:s', strtotime($order->updated_at) ) }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="order-status">Status</label>
                        <input type="text" id="status" class="form-control" value="{{ $order->getLastWorkflow()->status->name }}" readOnly>
                    </div>
                </div>
                @if(!empty($order->getLastWorkflow()) && $order->getLastWorkflow()->status->name == "cancelled")
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="order-status">Inventory Stocks</label>
                        <?php if(!empty($order->getLastWorkflow()->notes)){
                            $notes = json_decode($order->getLastWorkflow()->notes);
                        } ?>
                        <input type="text" id="stocks" class="form-control" value="{{ isset($notes->stocks) ? $notes->stocks : ''}}" readOnly>
                    </div>
                    <div class="form-group">
                        <label for="order-status">Remarks</label>
                        <input type="text" id="remarks" class="form-control" value="{{ isset($notes->remarks) ? $notes->remarks : '' }}" readOnly>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Details</div>
            <div class="panel-body">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="grandtotal">Reference No.</label>
                        <input type="text" id="reference-no" class="form-control" value="{{ $order->reference_no }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="grandtotal">Grand Total</label>
                        <input type="hidden" name="grand-total" value="{{ $order->total_price }}">
                        <input type="text" id="grand-total" class="form-control" value="Php {{ $order->total_price }}" readOnly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="grandtotal">Amount Tendered</label>
                        <input type="text" id="tendered" name="amount-tendered" class="form-control decimal" value="{{ $order->change_for }}" readOnly>
                        <span class="error-msg" style="color:#FF0000;display:none">Invalid amount</span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="grandtotal">Change</label>
                        <input type="text" id="change" class="form-control" value="" readOnly>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <input type="hidden" name="order-items" id="order-items" value="">
    </form>
    <div class="col-md-12">
        <a href="/orders/all/{{ $ordertype }}" class="btn btn-green">All {{ ucwords($ordertype) }} Orders</a>
    </div>
</div>

@stop

@section('script')
<script>
    $(document).ready(function(){
        $(".productSearch").select2();

        $(".product-panel").on("change",".productSearch",function(){
            var product_id = $(this).find('option:selected').val();
            $.get( "/products/"+product_id+"/stocks/").done(function(data){
                $("#stocks").val(data);
                $("#qty").attr("data-maxval", data);
                if(data == 0){
                    $("#qty").val(0);
                    $("#qty").attr("data-minval", data);
                } else {
                    $("#qty").val(1);
                    $("#qty").attr("data-minval", "1");
                }
            });
            
        });

        var dt = $("#orders-datatable").DataTable({
                    "searching": false,
                    "ordering": false,
                    "paging": false
                });

        $('#addproduct').on( 'click', function () {
            var id = $('.productSearch').find('option:selected').val();
            var name = $('.productSearch').find('option:selected').attr("data-name");
            var price = $('.productSearch').find('option:selected').attr("data-price");
            var qty = $('#qty').val();
            var subtotal = $('#qty').val() * $('.productSearch').find('option:selected').attr("data-price");
            if(name != "" && name != undefined && price != "" && price != undefined && qty != 0){
                 dt.row.add( [
                    id,
                    name,
                    price,
                    qty,
                    parseFloat(subtotal).toFixed(2),
                    '<a href="#" class="btn btn-danger btn-xs delete-product">X</a>'
                ] ).draw( false );
                $('.productSearch').select2("val","");
                $("#stocks").val("");
                $("#qty").val(1);
                updateGrandtotal();
                $("#tendered").val("");
                calculate_change();
            }
           
        });

        $(document).on('click', 'a.delete-product', function(event) {
            event.preventDefault();
            $(this).parent().parent('tr').addClass("delete-this");
            dt.row(".delete-this").remove().draw( false );
            updateGrandtotal();
        });

        function updateGrandtotal(){
            var grand_total = 0;
            $('#orders-datatable tbody tr td:nth-child(5)').each( function(){
                grand_total += parseFloat($(this).html());
            });
            grand_total = parseFloat(grand_total).toFixed(2);
            $("#grand-total").val("Php " + grand_total);
            $("input[name=grand-total]").val(grand_total);
            return grand_total;
        }

        function getDateTime(){
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if(h<10)
            {
                    h = "0"+h;
            }
            m = date.getMinutes();
            if(m<10)
            {
                    m = "0"+m;
            }
            s = date.getSeconds();
            if(s<10)
            {
                    s = "0"+s;
            }
            result = ''+days[day]+' - '+months[month]+' '+d+', '+year+' '+h+':'+m+':'+s;
            return result;

        }
        window.setInterval(function(){
          var datetimenow = getDateTime();
          $("#date-ordered").val(datetimenow);
        }, 1000);

        $("#add-order").click(function(event){
            event.preventDefault();
            if($('#orders-datatable tbody tr').length > 0){
                var grandtotal = updateGrandtotal();
                var amounttendered = $("#tendered").val();
                if(isNaN(parseFloat(amounttendered)) || parseFloat(amounttendered) == 0.00 || parseFloat(amounttendered) < parseFloat(grandtotal)){
                    $("#tendered").css("border","1px solid #FF0000");
                    $(".error-msg").show();
                } else {
                    var order_items = [];
                    $('#orders-datatable tbody tr').each( function(){
                        var id = $(this).find('td:nth-child(1)').html();
                        var price = $(this).find('td:nth-child(3)').html();
                        var qty = $(this).find('td:nth-child(4)').html();
                        var item = {};
                        item.product_id = id;
                        item.price = price;
                        item.quantity = qty;
                        order_items.push(item);
                    });
                    $("#order-items").val(JSON.stringify(order_items));
                    $("#neworder").submit();
                }

            }
        });

        $("#tendered").keyup(function(){
            $("#tendered").css("border","1px solid #ccc");
            $(".error-msg").hide();

            calculate_change();
        });

        function calculate_change(){
            var change = parseFloat($("#tendered").val() - $("input[name=grand-total]").val()).toFixed(2);
            change = change == 'NaN' ? '0.00' : change;
            $("#change").val(change);
        }
        calculate_change();
    });
</script>
@stop