@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Orders<small> view</small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif

<div class="row">
	<div class="col-md-5">
		<h4><a class="operation-link" href="/orders/add">Add Onsite Order <i class="glyphicon glyphicon-plus-sign operation-icon"></i></a></h4>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">ACTIVE ONSITE ORDERS</div>
			<div class="panel-body">
			
				<table class="table table-bordered table-striped" id="basic-datatable">
					<thead>
					<tr>
						<th>ID</th>
						<th>Reference No.</th>
						<th>Tendered By</th>
						<th>Date Added</th>
						<th>Date Updated</th>
						<th>Status</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					@foreach($onsite_processing_orders as $order)
					<tr>
						<td>{{ $order->id }}</td>
						<td>{{ $order->reference_no }}</td>
						<td>
							<?php 
								$workflow = $order->getLastWorkflow();
								if(count($workflow) > 0){
									echo ucwords($workflow->user->firstname . " " .$workflow->user->lastname);
								}
							?>
						</td>
						<td>{{ date("F j, Y, g:i a",strtotime($order->created_at)) }}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($order->updated_at)) }}</td>
						<td>{{ $order->status->name }}</td>
						<td style="width:30px;">
							<a href="/orders/{{ $order->id }}/manage" class="btn btn-warning btn-xs">Manage</a>
						</td>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<a href="/orders/all/onsite" class="btn btn-green">View All Onsite Orders</a>
		<br/><br/>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">ACTIVE ONLINE ORDERS</div>
			<div class="panel-body">
			
				<table class="table table-bordered table-striped" id="online-orders">
					<thead>
					<tr>
						<th>ID</th>
						<th>Reference No.</th>
						<th>Customer</th>
						<th>Date Added</th>
						<th>Date Updated</th>
						<th>Status</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					@foreach($online_active_orders as $order)
					<tr>
						<td>{{ $order->id }}</td>
						<td>{{ $order->reference_no }}</td>
						<td>{{ $order->customer->firstname . " " . $order->customer->lastname}}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($order->created_at)) }}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($order->updated_at)) }}</td>
						<td>{{ $order->status->name }}</td>
						<td style="width:30px;">
							<a href="/orders/{{$order->id}}/manage" class="btn btn-warning btn-xs">Manage</a>
						</td>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<a href="/orders/all/online" class="btn btn-green">View All Online Orders</a>
		<br/><br/>
    </div>
</div>

@stop

@section('script')
	<script>
		$("#online-orders").DataTable();
	</script>
@stop
