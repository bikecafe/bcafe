@extends('layouts.master')

@section('content')
<div class="page-header"><h1>{{$order_type}} Orders<small> view</small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif

<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">ALL {{strtoupper($order_type)}} ORDERS</div>
			<div class="panel-body">
			
				<table class="table table-bordered table-striped" id="basic-datatable">
					<thead>
					<tr>
						<th>ID</th>
						<th>Reference No.</th>
						@if(isset($order_type) && strToLower($order_type) == "onsite")
						<th>Tendered By</th>
						@endif
						@if(isset($order_type) && strToLower($order_type) == "online")
						<th>Customer</th>
						@endif
						<th>Date Added</th>
						<th>Date Updated</th>
						<th>Status</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					@foreach($orders as $order)
					<tr>
						<td>{{ $order->id }}</td>
						<td>{{ $order->reference_no }}</td>
						@if(isset($order_type) && strToLower($order_type) == "onsite")
						<td>
							<?php 
								$workflow = $order->getLastWorkflow();
								if(count($workflow) > 0){
									echo ucwords($workflow->user->firstname . " " .$workflow->user->lastname);
								}
							?>
						</td>
						@endif
						@if(isset($order_type) && strToLower($order_type) == "online")
						<td>{{ $order->customer->firstname . " " . $order->customer->lastname}}</td>
						@endif
						<td>{{ date("F j, Y, g:i a",strtotime($order->created_at)) }}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($order->updated_at)) }}</td>
						<td>{{ $order->status->name }}</td>
						<td style="width:30px;">
							<a href="/orders/{{ $order->id }}/manage" class="btn btn-warning btn-xs">Manage</a>
						</td>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<a href="/orders/" class="btn btn-green">Filter Active Orders</a>
		<br/><br/>
    </div>
</div>

@stop
