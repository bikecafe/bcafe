@extends('layouts.master')

@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">{{ Session::get('success') }}</div>
@endif
@if(Session::has('error'))
    <div class="alert alert-danger">{{ Session::get('error') }}</div>
@endif
@if($errors->any())
<div class="validation-summary-errors alert alert-danger">
    <ul>
        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
    </ul>
</div>
@endif

<div class="page-header"><h1>Product <small>{{ ucwords($mode) }}</small></h1></div>
<div class="row">
    <form enctype="multipart/form-data" action="@if (isset($product)){{ URL::to('/products/' . $product->id . '/edit') }}@endif" role="form" method="POST">
        <div class="col-md-6">
            <!-- START: Product Information -->
            <div class="panel panel-default">
                <div class="panel-heading">Product Information</div>
                <div class="panel-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="productname">Product Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" autocomplete="off"  value="{{ Input::old('name', isset($product) ? $product->name : null) }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" name="description" id="description">{{ Input::old('description', isset($product) ? $product->description : null) }}</textarea>
                        </div>
                        <?php if($mode == "edit"){ ?>
                            <div class="form-group">

                                <label for="productname">Current Image</label><br/>
                                <img src="/uploads/{{ $product->image }}" height="100"/>
                            </div>
                            
                        <?php } ?>
                        <div class="form-group">
                            <?php if($mode == "add"){ ?>
                            <label for="productname">Image</label>
                            <?php } ?>
                            <?php if($mode == "edit"){ ?>
                            <label for="productname">New Image</label>
                            <?php } ?>
                            <input type="file" class="form-control" name="image" id="image" placeholder="Product Image" autocomplete="off"  value="{{ Input::old('image', isset($product) ? $product->image : null) }}">
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" name="price" id="price" placeholder="Price" autocomplete="off" value="{{ Input::old('price', isset($product) ? $product->price : null) }}" onkeypress="return isNumberKey(this,event);" onpaste="return false;">
                        </div>
                        <div class="form-group">
                            <label for="categories">Add to Categories</label>
                            <select class="form-control select2" id="category-select" name="categories[]" multiple="multiple">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ (isset($product_categories) && in_array($category->id,$product_categories)) ? 'selected' : ( session()->has('data') && in_array($category->id,session('data')) ? 'selected' : '' )}} >
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                </div>
            </div>
            <!-- END: Product Information -->
        </div>

        <div class="col-md-6">

            <!-- START: Product Ingredients -->
            <div class="panel panel-default ingredient-panel">
                <div class="panel-heading">Product Ingredients per Order
                    <div class="pull-right delete">
                        <input type="button" class="btn btn-success btn-xs ingredient-add-row" value="Add +">
                    </div>
                </div>
                <div class="panel-body product-ingredient">
                    <div class="form-group" id="prod-ingredients">
                        @if (isset($product_ingredients))
                            @foreach($product_ingredients as $ing)
                            <div class="row ingredient-row">
                                <div class="col-lg-6">
                                    <label for="ingredients">Ingredient</label>
                                    <select class='form-control ingredientSearch' type='text' name="ingredientname[]" placeholder='Select Ingredient' data-val='{{ $ing["id"] }}'>
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label for="ingredients">Quantity</label>
                                    <input type="text" autocomplete="off" id="qty" name="ingredientqty[]" class="form-control decimal" value="{{ $ing["qty"] }}">
                                </div>
                                <div class="col-lg-2 i-unit">
                                    <label for="ingredients">Unit</label>
                                    <input type="text" autocomplete="off" id="unit" name="unit[]" class="form-control" value="" readonly>
                                </div>
                                <div class="pull-right delete">
                                    <input type="button" class="btn btn-danger btn-xs ingredient-delete" value="X">
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <!-- END: Product Ingredients -->

            <!-- START: Product Raw Materials -->
            <div class="panel panel-default rawmaterials-panel">
                <div class="panel-heading">Product Raw Materials per Order
                    <div class="pull-right delete">
                        <input type="button" class="btn btn-success btn-xs rawmaterial-add-row" value="Add +">
                    </div>
                </div>
                <div class="panel-body product-rawmaterial">
                    <div class="form-group" id="prod-rawmaterials">
                        @if (isset($product_rawmaterials))
                            @foreach($product_rawmaterials as $ing)
                            <div class="row rawmaterial-row">
                                <div class="col-lg-6">
                                    <label for="ingredients">Ingredient</label>
                                    <select class='form-control ingredientSearch' type='text' name="rawmaterialname[]" placeholder='Select Ingredient' data-val='{{ $ing["id"] }}'>
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label for="ingredients">Quantity</label>
                                    <input type="text" autocomplete="off" id="qty" name="rawmaterialqty[]" class="form-control decimal" value="{{ $ing["qty"] }}">
                                </div>
                                <div class="col-lg-2 i-unit">
                                    <label for="ingredients">Unit</label>
                                    <input type="text" autocomplete="off" id="unit" name="rawunit[]" class="form-control" value="" readonly>
                                </div>
                                <div class="pull-right delete">
                                    <input type="button" class="btn btn-danger btn-xs ingredient-delete" value="X">
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <!-- END: Product Raw Materials -->
            
        </div>

        <div class="col-md-12">
            <input type="submit" class="btn btn-dblue" value="Submit">
            <a href="/products" class="btn btn-warning">Cancel</a>
            <?php if($mode == "edit"){ ?>
            <a href="/products/{{ $product->id }}/delete" class="btn btn-danger delete-product">Delete</a>
            <?php }?>
            </form>
        </div>
        <input id="ingredients-str" type="hidden" value="{{ isset($ingredients) ? json_encode($ingredients) : ''}}"/>
        <input id="rawmaterials-str" type="hidden" value="{{ isset($rawmaterials) ? json_encode($rawmaterials) : ''}}"/>
    </form>
</div>

@stop

@section('script')
<script>
     function isNumberKey(txt, evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            //Check if the text already contains the . character
            if (txt.value.indexOf('.') === -1) {
                return true;
            } else {
                return false;
            }
        } else {
            if (charCode > 31
                 && (charCode < 48 || charCode > 57))
                return false;
        }
        return true;
    }

    $(document).ready(function(){

        $(".delete-product").click(function(event){
            event.preventDefault();
            var link = $(this).attr("href");
            swal({
                    title: "Are you sure you want to delete this?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#d9534f",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,   closeOnCancel: true },
                function(isConfirm){
                    if (isConfirm) {
                          window.location.href = link;
                    }
                }
            );
        });
        var ingredients = [];
        var add_prod_ing_html = '<div class="row ingredient-row">';
            add_prod_ing_html += '<div class="col-lg-6">';
            add_prod_ing_html += '<label for="ingredients">Ingredient</label>';
            add_prod_ing_html += '<select class="form-control ingredientSearch" type="text" name="ingredientname[]" placeholder="Select Ingredient">';
            add_prod_ing_html += '<option></option>';
            add_prod_ing_html += '</select>';
            add_prod_ing_html += '</div>';
            add_prod_ing_html += '<div class="col-lg-3">';
            add_prod_ing_html += '<label for="ingredients">Quantity</label>';
            add_prod_ing_html += '<input type="text" autocomplete="off" id="qty" name="ingredientqty[]" class="form-control decimal" value="">';
            add_prod_ing_html += '</div>';
            add_prod_ing_html += '<div class="col-lg-2 i-unit">';
            add_prod_ing_html += '<label for="ingredients">Unit</label>';
            add_prod_ing_html += '<input type="text" autocomplete="off" id="unit" name="unit[]" class="form-control" value="" readonly>';
            add_prod_ing_html += '</div>';
            add_prod_ing_html += '<div class="pull-right delete">';
            add_prod_ing_html += '<input type="button" class="btn btn-danger btn-xs ingredient-delete" value="X">';
            add_prod_ing_html += '</div>';
            add_prod_ing_html += '</div>';

        var rawmaterials = [];
        var add_prod_raw_html = '<div class="row rawmaterial-row">';
            add_prod_raw_html += '<div class="col-lg-6">';
            add_prod_raw_html += '<label for="ingredients">Ingredient</label>';
            add_prod_raw_html += '<select class="form-control ingredientSearch" type="text" name="rawmaterialname[]" placeholder="Select Ingredient">';
            add_prod_raw_html += '<option></option>';
            add_prod_raw_html += '</select>';
            add_prod_raw_html += '</div>';
            add_prod_raw_html += '<div class="col-lg-3">';
            add_prod_raw_html += '<label for="ingredients">Quantity</label>';
            add_prod_raw_html += '<input type="text" autocomplete="off" id="qty" name="rawmaterialqty[]" class="form-control decimal" value="">';
            add_prod_raw_html += '</div>';
            add_prod_raw_html += '<div class="col-lg-2 i-unit">';
            add_prod_raw_html += '<label for="ingredients">Unit</label>';
            add_prod_raw_html += '<input type="text" autocomplete="off" id="unit" name="rawunit[]" class="form-control" value="" readonly>';
            add_prod_raw_html += '</div>';
            add_prod_raw_html += '<div class="pull-right delete">';
            add_prod_raw_html += '<input type="button" class="btn btn-danger btn-xs ingredient-delete" value="X">';
            add_prod_raw_html += '</div>';
            add_prod_raw_html += '</div>';

        $('#category-select').hide();
        hideShowPanels();

        function hideShowPanels(){
            var ingredientCount = $(".ingredient-row").length;

            if(ingredientCount>0){
                $(".ingredient-panel .panel-body").show("fast");
            } else {
                $(".ingredient-panel .panel-body").hide("slow");
            }

            var rawmaterialCount = $(".rawmaterial-row").length;
            console.log(rawmaterialCount);
            if(rawmaterialCount>0){
                $(".rawmaterials-panel .panel-body").show("fast");
            } else {
                $(".rawmaterials-panel .panel-body").hide("slow");
            }

        }
        loadInitialIngredients();
        function loadInitialIngredients(){
            var ingredient_str = $('#ingredients-str').val();
            var ingredient_choices = JSON.parse(ingredient_str);
            
            $("#prod-ingredients .ingredientSearch").each(function(idx,elem){
                var parent = $(this);
                selected = parent.attr("data-val");
                parent.find('option').remove();
                $.each(ingredient_choices,function(index,item){
                    if(parseInt(selected) == parseInt(item.id)){
                        parent.append('<option value="' + item.id+ '" unit="' + item.unit + '" selected>'+ item.name +'</option>');
                        parent.parent().siblings('.i-unit').find("input[name^=unit]").val(item.unit);
                    } else {
                        parent.append('<option value="' + item.id+ '" unit="' + item.unit + '">'+ item.name +'</option>');
                    }
                });
                parent.select2();
            });

            var rawmaterial_str = $('#rawmaterials-str').val();
            var rawmaterial_choices = JSON.parse(rawmaterial_str);
            
            $("#prod-rawmaterials .ingredientSearch").each(function(idx,elem){
                var parent = $(this);
                selected = parent.attr("data-val");
                parent.find('option').remove();
                $.each(rawmaterial_choices,function(index,item){
                    if(parseInt(selected) == parseInt(item.id)){
                        parent.append('<option value="' + item.id+ '" unit="' + item.unit + '" selected>'+ item.name +'</option>');
                        parent.parent().siblings('.i-unit').find("input[name^=rawunit]").val(item.unit);
                    } else {
                        parent.append('<option value="' + item.id+ '" unit="' + item.unit + '">'+ item.name +'</option>');
                    }
                });
                parent.select2();
            });
        }

        function addIngredientOptions(selector){
            var ingredient_str = $('#ingredients-str').val();
            var ingredient_choices = JSON.parse(ingredient_str);
            $(selector).find('#prod-ingredients .ingredientSearch').find('option').remove().end().append('<option value=""></option>');
            $.each(ingredient_choices,function(index,item){
                $(selector).find('.ingredientSearch').append('<option value="' + item.id+ '" unit="' + item.unit + '">'+ item.name +'</option>');
            });
            $(selector).find('.ingredientSearch').select2();
        }

        function addRawMaterialOptions(selector){
            var rawmaterial_str = $('#rawmaterials-str').val();
            var rawmaterial_choices = JSON.parse(rawmaterial_str);
            $(selector).find('#prod-rawmaterials .ingredientSearch').find('option').remove().end().append('<option value=""></option>');
            $.each(rawmaterial_choices,function(index,item){
                $(selector).find('.ingredientSearch').append('<option value="' + item.id+ '" unit="' + item.unit + '">'+ item.name +'</option>');
            });
            $(selector).find('.ingredientSearch').select2();
        }

        function addRawMaterialOptions(selector){
            var rawmaterial_str = $('#rawmaterials-str').val();
            var rawmaterial_choices = JSON.parse(rawmaterial_str);
            $(selector).find('#prod-rawmaterials .ingredientSearch').find('option').remove().end().append('<option value=""></option>');
            $.each(rawmaterial_choices,function(index,item){
                $(selector).find('.ingredientSearch').append('<option value="' + item.id+ '" unit="' + item.unit + '">'+ item.name +'</option>');
            });
            $(selector).find('.ingredientSearch').select2();
        }

        $(".ingredient-panel").on("change",".ingredientSearch",function(){
            var unit = $(this).find('option:selected').attr('unit');
            $(this).parent().siblings('.i-unit').find("input[name^=unit]").val(unit);
        });

        $(".ingredient-add-row").click(function(){
            new_row = $(add_prod_ing_html).hide();
            new_row.appendTo("#prod-ingredients").show("normal");
            addIngredientOptions(new_row);
            $(".decimal").inputmask({'mask':"9{0,5}.9{0,3}", greedy: false});
            // $("#prod-ingredients").append(add_prod_ing_html);
            hideShowPanels();
        });

        $(".ingredient-panel").on("click", ".ingredient-delete", function(){
            this_row = $(this).parent().parent('.row');
            this_row.hide('normal', function(){ 
                this_row.remove(); 
                hideShowPanels();
            });
            
        });

        $(".rawmaterials-panel").on("change",".ingredientSearch",function(){
            var unit = $(this).find('option:selected').attr('unit');
            $(this).parent().siblings('.i-unit').find("input[name^=rawunit]").val(unit);
        });

        $(".rawmaterial-add-row").click(function(){
            new_row = $(add_prod_raw_html).hide();
            new_row.appendTo("#prod-rawmaterials").show("normal");
            addRawMaterialOptions(new_row);
            $(".decimal").inputmask({'mask':"9{0,5}.9{0,3}", greedy: false});
            // $("#prod-ingredients").append(add_prod_ing_html);
            hideShowPanels();
        });

        $(".rawmaterials-panel").on("click", ".ingredient-delete", function(){
            this_row = $(this).parent().parent('.row');
            this_row.hide('normal', function(){ 
                this_row.remove(); 
                hideShowPanels();
            });
        });
    });
</script>
@stop