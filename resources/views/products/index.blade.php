@extends('layouts.master')

@section('content')
<div class="page-header"><h1>{{ ucwords($mode) }} Products<small> view</small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif
<?php if($mode == 'active'){ ?>
<div class="row">
	<div class="col-md-5">
		<h4><a class="operation-link" href="/products/add">Add <i class="glyphicon glyphicon-plus-sign operation-icon"></i></a></h4>
	</div>
</div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">{{ ucwords($mode) }} Products</div>
			<div class="panel-body">
			
				<table class="table table-bordered table-striped" id="basic-datatable">
					<thead>
					<tr>
						<th>ID</th>
						<th style="word-wrap: break-word; max-width:150px">Product Name</th>
						<th>Price</th>
						<th>Servings</th>
						<th>Date Added</th>
						<th>Date Updated</th>
						<?php if($mode == 'active'){ ?>
							<th></th>
						<?php } ?>
					</tr>
					</thead>
					<tbody>
					@foreach($products as $product)
					<tr>
						<td>{{ $product->id }}</td>
						<td>{{ $product->name }}</td>
						<td>{{ $product->price }}</td>
						<td>{{ $product->stocks() }}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($product->created_at)) }}</td>
						<td>{{ date("F j, Y, g:i a",strtotime($product->updated_at)) }}</td>
						<?php if($mode == 'active'){ ?>
							<td style="width:30px;">
								<a href="/products/{{ $product->id }}/edit" class="btn btn-warning btn-xs">Edit</a>
							</td>
						<?php } ?>
						<?php if($mode == 'deleted'){ ?>
							<td style="width:50px;">
								<form action="/products/{{ $product->id }}/restore" method="post" class="restore-row">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="submit" class="btn btn-dblue btn-xs" value="Restore" />
								</form>
							</td>
						<?php } ?>
					</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<?php if($mode == 'active'){ ?>
			<a href="/products/deleted" class="btn btn-danger">View Deleted Products</a>
		<?php } elseif($mode == 'deleted') {?>
			<a href="/products" class="btn btn-green">View Active Products</a>
		<?php }?>
    </div>
</div>

@stop

@section('script')
<script>
    $(document).ready(function(){

        var confirmText = "This product will be deleted";

        swlConfirm(confirmText);

        var confirmText2 = "This product will be restored";

        resConfirm(confirmText2);
    });
</script>
@stop