@extends('layouts.master')

@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">{{ Session::get('success') }}</div>
@endif
@if(Session::has('error'))
    <div class="alert alert-danger">{{ Session::get('error') }}</div>
@endif
@if($errors->any())
<div class="validation-summary-errors alert alert-danger">
    <ul>
        {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
    </ul>
</div>
@endif

<div class="page-header"><h1>Product <small>{{ ucwords($mode) }}</small></h1></div>
<div class="row">
    <form enctype="multipart/form-data" action="@if (isset($product)){{ URL::to('/products/' . $product->id . '/edit') }}@endif" role="form" method="POST">
        <div class="col-md-6">
            <!-- START: Product Information -->
            <div class="panel panel-default">
                <div class="panel-heading">Product Information</div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" action="@if (isset($product)){{ URL::to('/products/' . $product->id . '/edit') }}@endif" role="form" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="productname">Product Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" autocomplete="off"  value="{{ Input::old('name', isset($product) ? $product->name : null) }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description" id="description" placeholder="Description" autocomplete="off" value="{{ Input::old('description', isset($product) ? $product->description : null) }}">
                        </div>
                        <?php if($mode == "edit"){ ?>
                            <div class="form-group">

                                <label for="productname">Current Image</label><br/>
                                <img src="/uploads/{{ $product->image }}" height="100"/>
                            </div>
                            
                        <?php } ?>
                        <div class="form-group">
                            <?php if($mode == "add"){ ?>
                            <label for="productname">Image</label>
                            <?php } ?>
                            <?php if($mode == "edit"){ ?>
                            <label for="productname">New Image</label>
                            <?php } ?>
                            <input type="file" class="form-control" name="image" id="image" placeholder="Product Image" autocomplete="off"  value="{{ Input::old('image', isset($product) ? $product->image : null) }}">
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" name="price" id="price" placeholder="Price" autocomplete="off" value="{{ Input::old('price', isset($product) ? $product->price : null) }}">
                        </div>
                        <div class="form-group">
                            <label for="categories">Add to Categories</label>
                            <select class="form-control select2" id="category-select" name="categories[]" multiple="multiple">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ (isset($product_categories) && in_array($category->id,$product_categories)) ? 'selected' : ( session()->has('data') && in_array($category->id,session('data')) ? 'selected' : '' )}} >
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" class="btn btn-dblue" value="Submit">
                        <a href="/products" class="btn btn-danger">Cancel</a>
                    </form>

                </div>
            </div>
            <!-- END: Product Information -->
        </div>

        <div class="col-md-6">
            <!-- START: Stocks -->
            <div class="panel panel-default">
                <div class="panel-heading">Available Servings</div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <input type="text" autocomplete="off" id="qty" name="quantity" class="form-control integer" value="{{ Input::old('quantity', isset($product) ? $product->getStocks() : null) }}" readOnly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Stocks -->

            <!-- START: Product Ingredients -->
            <div class="panel panel-default ingredient-panel">
                <div class="panel-heading">Product Ingredients per Order
                    <div class="pull-right delete">
                        <input type="button" class="btn btn-success btn-xs ingredient-add-row" value="Add +">
                    </div>
                </div>
                <div class="panel-body product-ingredient">
                    <div class="form-group">
                        <div class="row ingredient-row">
                            <div class="col-lg-6">
                                <label for="ingredients">Ingredient</label>
                                <select class='form-control ingredientSearch' type='text' name="ingredientname[]" placeholder='Select Ingredient'>
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label for="ingredients">Quantity</label>
                                <input type="text" autocomplete="off" id="qty" name="ingredientqty[]" class="form-control decimal" value="{{ Input::old('quantity', isset($product) ? $product->ingredients : null) }}">
                            </div>
                            <div class="col-lg-2 i-unit">
                                <label for="ingredients">Unit</label>
                                <input type="text" autocomplete="off" id="unit" name="unit[]" class="form-control" value="{{ Input::old('quantity', isset($product) ? $product->getStocks() : null) }}" readonly>
                            </div>
                            <div class="pull-right delete">
                                <input type="button" class="btn btn-danger btn-xs passenger-delete" value="X">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Product Ingredients -->
            
        </div>
        <input id="ingredients-str" type="hidden" value="{{ isset($ingredients) ? json_encode($ingredients) : ''}}"/>
    </form>
</div>

@stop

@section('script')
<script>
    $(document).ready(function(){
        var ingredients = [];

        $('#category-select').hide();
        hideShowPanels();

        function hideShowPanels(){
            var ingredientCount = $(".ingredient-row").length;

            if(ingredientCount>0){
                $(".ingredient-panel .panel-body").show("slow");
            } else {
                $(".ingredient-panel .panel-body").hide("slow");
            }

        }
        loadInitialIngredients();
        function loadInitialIngredients(){
            var ingredient_str = $('#ingredients-str').val();
            var ingredient_choices = JSON.parse(ingredient_str);
            //console.log(ingredient_choices);
            $('.ingredientSearch').find('option').remove().end().append('<option value=""></option>');
            $.each(ingredient_choices,function(index,item){
                $('.ingredientSearch').append('<option value="' + item.id+ '" unit="' + item.unit + '">'+ item.name +'</option>');
            });
            $(".ingredientSearch").select2();
        }

        $(".ingredient-panel").on("change",".ingredientSearch",function(){
            var unit = $(this).find('option:selected').attr('unit');
            $(this).parent().siblings('.i-unit').find("input[name^=unit]").val(unit);
        });
    });
</script>
@stop