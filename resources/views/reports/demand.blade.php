@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Product Demand Report<small> view</small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif
<div class="alert alert-danger invalid_alert hidden">Invalid date range!</div>
<div class="alert alert-danger empty_alert hidden">Please fill in the date fields</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">SETTINGS</div>
			<div class="panel-body">
				<div class='col-sm-4'>
		            <div class="form-group">
		            	<label>Date From</label>
		                <div class='input-group date' id='report-date-from'>
		                    <input type='text' class="form-control" value="{{ isset($date_from) ? $date_from : '' }}"/>
		                    <input type='hidden' id="report-date-from-value" class="form-control" value="{{ isset($date_from) ? $date_from : '' }}"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
		            </div>
		        </div>
		        <div class='col-sm-4'>
		            <div class="form-group">
		            	<label>Date To</label>
		                <div class='input-group date' id='report-date-to'>
		                    <input type='text' class="form-control" value="{{ isset($date_to) ? $date_to : '' }}"/>
		                    <input type='hidden' id="report-date-to-value" class="form-control" value="{{ isset($date_to) ? $date_to : '' }}"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
		            </div>
		        </div>
		        <div class="col-lg-2">
                    <label for="add" style="height:15px"></label>
                    <div class="form-group">
                        <button type="button" id="generate" class="btn btn-green">GENERATE</button>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">
				TOP PRODUCTS
				<div class="pull-right delete">
                    <input type="button" class="btn btn-success btn-xs top-product-print" value="Print">
                </div>
			</div>
			<div class="panel-body" id="top_product">
				<div class="col-md-8">
					<div id="barchart" style="width:100%;height:400px"></div>
				</div>
				<div class="col-md-4">
				@if(isset($productGross) && !empty($productGross))
				<table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Rank</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    	<?php $x = 1; ?> 
                    	@foreach($productGross as $prod)
                    	<tr>
                    		<td>{{$x}}</td>
                    		<td>{{$prod['name']}}</td>
                    		<td>{{$prod['quantity']}}</td>
                    		<td>{{$prod['amount']}}</td>
                    	</tr>
                    	<?php $x++; ?>
                    	@endforeach
                    </tbody>
                </table>
				@endif
				</div>
			</div>
		</div>
    </div>
</div>

<?php 
	if(isset($topProducts) && !empty($topProducts)){
		$category_data = json_encode($topProducts);
	} else {
		$category_data = json_encode([]);
	}
?>

@stop

@section('script')
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script>
		google.charts.load("current", {packages:['corechart']});
	    google.charts.setOnLoadCallback(drawChart);
	    function drawChart() {
	      var data = google.visualization.arrayToDataTable(<?php echo $category_data ?>);

	      var view = new google.visualization.DataView(data);
	      view.setColumns([0, 1,
	                       { calc: "stringify",
	                         sourceColumn: 1,
	                         type: "string",
	                         role: "annotation" },
	                       2]);

	      var options = {
	        width: 600,
	        height: 400,
	        bar: {groupWidth: "95%"},
	        legend: { position: "none" },
	      };
	      var chart = new google.visualization.ColumnChart(document.getElementById("barchart"));
	      chart.draw(view, options);
		  }
	      
		$(document).ready(function(){
			 $('.top-product-print').click(function(){
			 		var data = $("#top_product").html();
			 		var rdate_from = $("#report-date-from-value").val();
			 		var rdate_to = $("#report-date-to-value").val();
			 		var report_date_from = $.datepicker.formatDate("MM d, yy", new Date(rdate_from));
			 		var report_date_to = $.datepicker.formatDate("MM d, yy", new Date(rdate_to));
			        var mywindow = window.open('', 'Product Demand', 'height=800,width=1000');
			        mywindow.document.write('<html><head><title>Product Demand</title>');
			        mywindow.document.write('<link rel="stylesheet" href="/assets/css/bootstrap/bootstrap.css" type="text/css" />');
			        mywindow.document.write('<link rel="stylesheet" href="/assets/css/app/app.v1.css?v=11" type="text/css" />');
			        mywindow.document.write('<link rel="stylesheet" href="/assets/css/app/reports.css?v=11" type="text/css" />');
			        mywindow.document.write('<style type="text/css" media="screen"> @page{ size: landscape;}</style>');
			        mywindow.document.write('</head><body style="background-color:#FFFFFF">');
			        mywindow.document.write('<h3 style="text-align:center">Route 88: Bike Café</h3>');
			        mywindow.document.write('<h5 style="text-align:center">#88-A Anonas Street, Barangay East Kamias, 1102 Quezon City, Philippines</h5>');
					mywindow.document.write('<h3 style="text-align:center">Top Product Demand Report</h3>');
					mywindow.document.write('<h4 style="text-align:center">'+report_date_from+' to '+report_date_to+'</h4>');
			        mywindow.document.write(data);
			        mywindow.document.write('<h4>Prepared By:</h4>');
			        mywindow.document.write('<h4>'+'<?php echo ucwords(Auth::user()->firstname) . " " . ucwords(Auth::user()->lastname) ?>'+'</h4>');
			        mywindow.document.write('<h4>'+$.datepicker.formatDate("MM d, yy ", new Date())+' <?php echo date("g:i a")?></h4>');
			        mywindow.document.write('</body></html>');

			        mywindow.document.close(); // necessary for IE >= 10
			        myDelay = setInterval(checkReadyState, 10);

				    function checkReadyState() {
				        if (mywindow.document.readyState == "complete") {
				            clearInterval(myDelay);
				            mywindow.focus(); // necessary for IE >= 10

				            mywindow.print();
				            mywindow.close();
				        }
				    }

			        return true;
			    });

            $('#report-date-from').datetimepicker({
            	format: 'YYYY-MM-DD',
            	maxDate: new Date()
            });

            $('#report-date-to').datetimepicker({
            	format: 'YYYY-MM-DD',
            	maxDate: new Date()
            });

            $("#generate").click(function(){
            	if($('#report-date-from > input').val() !== '' && $('#report-date-to > input').val() !== ''){
            		var date_from = $('#report-date-from > input').val();
            		var date_to = $('#report-date-to > input').val();
            		var d1 = new Date(date_from);
            		var d2 = new Date(date_to);
            		if(d2.getTime() >= d1.getTime()){
            			$(".empty_alert").addClass("hidden");
            			$(".invalid_alert").addClass("hidden");
            			window.location = '/reports/demand/'+date_from+'/'+date_to;
            		} else {
            			$(".empty_alert").addClass("hidden");
            			$(".invalid_alert").removeClass("hidden");
                        window.scrollTo(0, 0);
            		}
            		
            	} else {
            		$(".invalid_alert").addClass("hidden");
            		$(".empty_alert").removeClass("hidden");
                    window.scrollTo(0, 0);
            	}
            });
		            
		})
	</script>
@stop
