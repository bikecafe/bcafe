@extends('layouts.master')

@section('content')
<div class="page-header"><h1>General Settings<small></small></h1></div>
@if(Session::has('success'))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('success') !!}
</div>
@endif
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! Session::get('error') !!}
</div>
@endif

<div class="row">
    <div class="col-md-6">
		
		<div class="panel panel-default">
			<div class="panel-heading">Settings</div>
			<form enctype="multipart/form-data" role="form" method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="panel-body">
					<div class="col-lg-12">
						<!--<div class="form-group">
							<label for="delivery_fee">Delivery Fee</label>
							<input class="form-control" type="text" name="delivery_fee" value="{{ Input::old('delivery_fee', Helper::getSettingValue('delivery_fee')) }}" placeholder="Delivery Fee" autocomplete="off"/>
						</div>-->
						<div class="form-group">
							<label for="contact_email">Contact Email</label>
							<input class="form-control" type="text" name="contact_email" value="{{ Input::old('contact_email', Helper::getSettingValue('contact_email')) }}" placeholder="Contact Email" autocomplete="off"/>
						</div>
						<div class="form-group">
							<label for="delivery_places">Delivery Places</label>
							<input data-role="tagsinput" class="form-control" type="text" name="delivery_places" value="{{ Input::old('delivery_places', Helper::getSettingValue('delivery_places')) }}" autocomplete="off"/>
						</div>
					</div>
					<div class="col-lg-12">
						<button type="submit" class="btn btn-dblue">Submit</button>
	                	<a href="/users" class="btn btn-danger">Cancel</a>
	                </div>
				</div>
				
			</form>
		</div>
		
    </div>

</div>

@stop
