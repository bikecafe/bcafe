@extends('layouts.master')

@section('content')
<div class="page-header"><h1>Users <small>{{ ucwords($mode) }}</small></h1></div>
<div class="row">
    <div class="col-md-6">
        @if(Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        @if($errors->any())
        <div class="validation-summary-errors alert alert-danger">
            <ul>
                {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
            </ul>
        </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">{{ ucwords($mode) }} User</div>
            <div class="panel-body">
                <form action="@if (isset($user)){{ URL::to('/users/' . $user->id . '/edit') }}@endif" role="form" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" @if ($mode == 'edit'){{ 'disabled' }} @endif value="{{ Input::old('username', isset($user) ? $user->username : null) }}">
                    </div>
                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="text" class="form-control" name="first_name" id="firstname" placeholder="First Name" autocomplete="off" value="{{ Input::old('first_name', isset($user) ? $user->firstname : null) }}">
                    </div>
                    <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input type="text" class="form-control" name="last_name" id="lastname" placeholder="Last Name" autocomplete="off" value="{{ Input::old('last_name', isset($user) ? $user->lastname : null) }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" value="{{ Input::old('email', isset($user) ? $user->email : null) }}">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        <p class="help-block">@if (isset($user)){{ "Leave this blank if you don't want to change the password" }} @endif</p>
                        <p class="help-block">A password should be 8 characters long or more, has a number, a letter and a symbol </p>
                    </div>
                    <div class="form-group {{ (isset($user) && $user->type->id == 1) ? 'hidden' : ''}}" >
                        <label for="user-type">User Type</label>
                        <select class="form-control select2" id="user-type" name="user_type_id">
                            @foreach ($userTypes as $userType)
                                @if($mode == 'add')
                                    <option value="{{$userType->id}}" {{ Input::old('user_type_id') == $userType->id ? 'selected' : '' }}>{{$userType->name}}</option>
                                @else
                                    <option value="{{$userType->id}}" {{ Input::old('user_type_id',$user->user_type_id) === $userType->id ? 'selected' : '' }}>{{$userType->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-dblue">Submit</button>
                    <a href="/users" class="btn btn-danger">Cancel</a>
                </form>

            </div>
        </div>
    </div>
</div>

@stop